#Super User Dashboard. 

##Widgets
### Students branch wise. 
 - percentage of students of all branches in Donut Graph.
 - Total Students in the all branches. 
 
### Students summary gender wise
 - percentage of Males of all **students** of all branches
 - Percentage of Females of all **students** of all branches

### Students Detail
 - Branch students categories in Males and females . 
 - Student in each class in each branch.
 

 | ID | Name | Males | Females |
 |:---|:----:|:-----:|--------:|
 |1| Branch A | 2 | 1|
 |2| Class 1 | 1 | 0|
 |3| Class 2 | 1 | 1|
  