#Student Module

##Database Table

 - student_pi // Student Personal Information 
 - student_gi // Student Guardian Information
 - student_ai // Student Academic Information
 - student_ei // Student enrollment Information

##Global Variable

We have some variable in all the tables. so, we have those variable in student module also. 

###Deletable

 - 0 : Data has deleted from front end. 
 - 1 : it means Data exists.
  
###Editable
 - 0 : Data is not editable and deletable. 
 - 1 : Data is editable and deletable if deletable='1'.

#Delete Student 

currently, we add the feature. What data you delete. It remain there with unique id. Every new student get the new unique id.
Once student id will be created with its unique id.
 

##On Delete 

We update the value of deletable column to 0. So no data with deletable="0" cannot be accesible to front end.
   student_pi.deletable="0"
   student_gi.deletable="0"
   student_ei.deletable="0"
   student_ai.deletable="0"