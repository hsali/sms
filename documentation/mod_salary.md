#Student Module
##Type of Users

user_type field is responsible for user type. 1 is for super, 2 is for admin and so on as following. 

 1. Super
 2. Admin
 3. Teacher
 4. Parent
 5. Student
 
##Database Table

- D means default. 
- AI mean auto increment
- PK mean primary key
 
###Important variables

####arrears_and_advance
 this will maintain  arrears or advanced payment. 
 
     arrears_and_advance= paid-payable;
  - -ve value: left sum or arrears 
  - +ve value: advance payment
  
####User Type
 
   1. Super
   2. Admin
   3. Teacher
   4. Parent
   5. Student
  
###Database Table : users 
   - salary_id (PK,AI, int, 11)
   - arrears_and_advance
   - allowances // total allowance paid. 
   - deductions // total deductions 
   - payable
   - paid
   - total_presents(int)
   - total_absents(int)
   - total_leaves(int)
   - total_late_presents(int)
   - month(varchar)
   - year(varchar)
   - staff_id(int)
   - branch_id(int, 10)
   - editable(smallint, 5,D(1))
   - deletable(smallint, 5,D(1))
   - created_date_time(DATETIME)
   - last_updated_date_time(TIMESTAMP)

##Global Variable

We have some variable in all the tables. so, we have those variable in student module also. 

###Deletable
  - 0 = Data has deleted from front end. 
  - 1 = it means Data exists.
  
###Editable
 - 0 : Data is not editable and deletable. 
 - 1 : Data is editable and deletable if deletable='1'.
 