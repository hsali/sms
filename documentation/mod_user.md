#Student Module
##Type of Users

user_type field is responsible for user type. 1 is for super, 2 is for admin and so on as following. 

 1. Super
 2. Admin
 3. Teacher
 4. Parent
 5. Student
 
##Database Table

 - D means default. 
 - AI mean auto increment
 - PK mean primary key
 
###Important variables

####status 
 This variable maintain the status of user. Either user is active or suspended or something else which is defined.
 
  - 0: Inactive
  - 1: Active . 
  
####User Type
 
   1. Super
   2. Admin
   3. Teacher
   4. Parent
   5. Student
  
###Database Table : users 
   - user_id (PK,AI, int, 11)
   - user_name(varchar, 20)
   - first_name(varchar, 20)
   - last_name(varchar, 20)
   - password(varchar, 20)
   - email(varchar, 100)
   - about_me(LONGTEXT)
   - user_type(smallint, 5, D(2))
   - status(smallint, 5,D(2))
   - branch_id(int, 10)
   - editable(smallint, 5,D(1))
   - deletable(smallint, 5,D(1))
   - created_date_time(DATETIME)
   - last_updated_date_time(TIMESTAMP)

##Global Variable

We have some variable in all the tables. so, we have those variable in student module also. 

###Deletable
  - 0 = Data has deleted from front end. 
  - 1 = it means Data exists.
  
###Editable
 - 0 : Data is not editable and deletable. 
 - 1 : Data is editable and deletable if deletable='1'.
 