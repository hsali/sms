# School Management System 
 School Management System

## About SMS 
### Words 1
Lets start to manage your school with simple school management system.
### Word 2 : Features
SMS is simply called the School Management System. It help the school to manage their students, staffs, fees, examination and many more.
### Word 3 : Design of SMS
SMS is simple to learn and use. Its user interface is user-friendly. It is responsive design. Use this design from any latest browser on any device.

## Features
 * Multi Branches. You can manage different your school by seprating their data in single installation
 * Multi Users. Super, admin, teacher, parent, student
 * Picture capturing and uploading for their profile in user,staff and student module. 
 
 
### Modules in this structure
 * Branches
 * [Users] (../blob/master/documentation/mod_user.md)
 * Classes
 * Sections
 * [Students](../blob/master/documentation/mod-student.md) 
 * Staff
 * Attendance
 * Fee
 * [Salary](../blob/master/documentation/mod_salary.md)
 * Examination

## Specification 
### Login Page
  Logo Size : 149 x 29 px
#Release 

### v0.1.3
 * Staff Add, Edit, Delete, View, search
 * improve profile pic. profile picture support jpeg, jpg, gif, png format
 * 
 
### v0.1.2
### v0.1.1
### v0.1.0
### v0.0.1
 * Manage users
    * add, delete,edit, view, search users
 * User Access
 
    we have 5 types of users here. 
    
     * Super
    
    The user of type super access can access all the branches' data. It has full access 
    
     * Admin
    
    Admin User can access to the specific branch..
    
     * Admin
    
    Admin User can access to the specific branch..
    
     * Admin
 
     * Admin
    
 * Manage Branches
    * Add, edit, view, delete, search branches
    *
    
   