-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ems
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admission_fee`
--

DROP TABLE IF EXISTS `admission_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admission_fee` (
  `af_id` int(10) NOT NULL AUTO_INCREMENT,
  `process_fee` varchar(10) DEFAULT '0',
  `medical_fee` varchar(10) DEFAULT '0',
  `library_fund` varchar(10) DEFAULT '0',
  `computer_lab_fund` varchar(10) DEFAULT '0',
  `form_charges` varchar(10) DEFAULT '0',
  `stationary_charges` varchar(10) DEFAULT '0',
  `tuition_fee` varchar(10) DEFAULT '0',
  `other_fee` varchar(10) DEFAULT '0',
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` varchar(45) NOT NULL,
  `student_id` bigint(20) NOT NULL,
  PRIMARY KEY (`af_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admission_fee`
--

LOCK TABLES `admission_fee` WRITE;
/*!40000 ALTER TABLE `admission_fee` DISABLE KEYS */;
/*!40000 ALTER TABLE `admission_fee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `branch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(500) NOT NULL,
  `branch_phone` varchar(20) DEFAULT NULL,
  `branch_cell` varchar(20) DEFAULT NULL,
  `branch_fax` varchar(20) DEFAULT NULL,
  `branch_address` varchar(500) DEFAULT NULL,
  `branch_description` text,
  `branch_email` varchar(100) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` int(2) NOT NULL DEFAULT '1',
  `editable` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'Branch A','123456','123156465','15641321','fatima, shujabad','female campus...','fatima@gmail.com','2015-03-03 17:33:47','2014-12-25 13:19:43',1,1),(2,'Branch B','1234568','16549465','1354654','main road.','boys branch','branch@gmail.com','2014-12-27 05:27:03','2014-12-27 00:27:03',1,1),(3,'Branch C','0614396589','03007181089','','Lal bagh shujabad','girl campus','kanwar.altaf@citi.edu.pk','2014-12-31 04:05:37','2014-12-27 10:01:01',1,1);
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('22f5da94df1049d70d4b9b663808fa2b','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',1440356918,'a:9:{s:9:\"user_data\";s:0:\"\";s:7:\"pre_url\";s:42:\"http://localhost/ems/index.php/super/users\";s:9:\"user_name\";s:5:\"super\";s:7:\"user_id\";s:1:\"1\";s:10:\"first_name\";s:2:\"Mr\";s:9:\"last_name\";s:5:\"Super\";s:9:\"branch_id\";s:2:\"-1\";s:9:\"user_type\";s:1:\"1\";s:5:\"login\";b:1;}'),('b642bae35bf6df0f09cdcb358f5912e7','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',1440421359,''),('f75967c37083f923a3dc3f5c5a7d64f2','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',1440421279,'a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"pre_url\";s:30:\"http://localhost/ems/index.php\";}');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classes` (
  `class_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(50) NOT NULL COMMENT 'class name',
  `created_date_time` datetime NOT NULL COMMENT 'created_date_time',
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` int(2) NOT NULL DEFAULT '1',
  `editable` int(2) NOT NULL DEFAULT '1',
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES (1,'PG','2015-01-23 13:12:05','2015-01-23 07:12:05',1,0,1),(2,'KG1','2015-01-23 13:12:15','2015-01-23 07:12:15',1,1,1),(3,'KG2','2015-01-23 13:12:22','2015-01-23 07:12:22',1,1,1),(4,'J1','2015-01-23 13:12:34','2015-01-23 07:12:34',1,1,1),(6,'J2','2015-01-23 13:13:01','2015-01-23 07:13:01',1,1,1),(7,'9th','2015-03-02 15:19:13','2015-03-02 05:19:13',1,1,1),(8,'10th','2015-03-02 15:19:29','2015-03-02 05:19:29',1,1,1);
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_receipts`
--

DROP TABLE IF EXISTS `fee_receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fee_receipts` (
  `receipt_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `received_payment` int(11) NOT NULL,
  `received_payment_date` date NOT NULL,
  `received_fee_ids` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `student_id` bigint(20) NOT NULL,
  `deletable` int(5) NOT NULL DEFAULT '1',
  `editable` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`receipt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_receipts`
--

LOCK TABLES `fee_receipts` WRITE;
/*!40000 ALTER TABLE `fee_receipts` DISABLE KEYS */;
INSERT INTO `fee_receipts` VALUES (1,4000,'2015-07-15','1,2,3','2015-07-15 14:17:21','2015-07-15 09:17:21',8,1,1),(2,2200,'2015-07-15','4,5,6','2015-07-15 18:52:15','2015-07-15 13:52:15',8,1,1),(3,700,'2015-07-15','7,8','2015-07-15 21:27:26','2015-07-15 16:27:26',8,1,1),(4,3400,'2015-07-15','9,10,11,12','2015-07-15 21:28:40','2015-07-15 16:28:40',8,1,1),(5,3900,'2015-07-15','13,14,15,16','2015-07-15 21:30:08','2015-07-15 16:30:08',8,1,1),(6,4700,'2015-07-15','17,18,19','2015-07-15 21:31:43','2015-07-15 16:31:43',8,1,1),(7,3200,'2015-07-15','20,21,22','2015-07-15 21:32:38','2015-07-15 16:32:38',8,1,1);
/*!40000 ALTER TABLE `fee_receipts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_status`
--

DROP TABLE IF EXISTS `fee_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fee_status` (
  `fs_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'fee status id',
  `fc_id` int(11) DEFAULT NULL COMMENT 'fee category id',
  `fs_fee` int(11) DEFAULT NULL COMMENT 'fee',
  `f_status` smallint(6) DEFAULT NULL COMMENT 'fee status: paid, un-paid,etc',
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `student_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`fs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_status`
--

LOCK TABLES `fee_status` WRITE;
/*!40000 ALTER TABLE `fee_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `received_fee`
--

DROP TABLE IF EXISTS `received_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `received_fee` (
  `rf_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fee` int(11) NOT NULL,
  `period` int(11) DEFAULT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fc_id` int(11) NOT NULL,
  `student_id` bigint(20) NOT NULL,
  `deletable` int(5) NOT NULL DEFAULT '1',
  `editable` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `received_fee`
--

LOCK TABLES `received_fee` WRITE;
/*!40000 ALTER TABLE `received_fee` DISABLE KEYS */;
INSERT INTO `received_fee` VALUES (1,2000,12,'2015-07-15 14:17:21','2015-07-15 09:17:21',8,8,1,1),(2,500,12,'2015-07-15 14:17:21','2015-07-15 09:17:21',9,8,1,1),(3,1500,12,'2015-07-15 14:17:21','2015-07-15 09:17:21',10,8,1,1),(4,500,12,'2015-07-15 18:52:14','2015-07-15 13:52:14',9,8,1,1),(5,1500,12,'2015-07-15 18:52:15','2015-07-15 13:52:15',10,8,1,1),(6,200,1,'2015-07-15 18:52:15','2015-07-15 13:52:15',13,8,1,1),(7,500,12,'2015-07-15 21:27:25','2015-07-15 16:27:26',9,8,1,1),(8,200,1,'2015-07-15 21:27:26','2015-07-15 16:27:26',13,8,1,1),(9,500,12,'2015-07-15 21:28:40','2015-07-15 16:28:40',9,8,1,1),(10,1500,12,'2015-07-15 21:28:40','2015-07-15 16:28:40',10,8,1,1),(11,200,1,'2015-07-15 21:28:40','2015-07-15 16:28:40',13,8,1,1),(12,1200,1,'2015-07-15 21:28:40','2015-07-15 16:28:40',11,8,1,1),(13,500,12,'2015-07-15 21:30:07','2015-07-15 16:30:07',9,8,1,1),(14,2000,12,'2015-07-15 21:30:07','2015-07-15 16:30:07',12,8,1,1),(15,200,1,'2015-07-15 21:30:08','2015-07-15 16:30:08',13,8,1,1),(16,1200,1,'2015-07-15 21:30:08','2015-07-15 16:30:08',11,8,1,1),(17,1200,1,'2015-07-15 21:31:43','2015-07-15 16:31:43',11,8,1,1),(18,1500,12,'2015-07-15 21:31:43','2015-07-15 16:31:43',10,8,1,1),(19,2000,12,'2015-07-15 21:31:43','2015-07-15 16:31:43',12,8,1,1),(20,1500,12,'2015-07-15 21:32:38','2015-07-15 16:32:38',10,8,1,1),(21,1200,1,'2015-07-15 21:32:38','2015-07-15 16:32:38',11,8,1,1),(22,500,12,'2015-07-15 21:32:38','2015-07-15 16:32:38',9,8,1,1);
/*!40000 ALTER TABLE `received_fee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `result_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `result_cat_id` int(11) NOT NULL,
  `exam_date` date NOT NULL,
  `obtained_marks` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `total_marks` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  `student_id` bigint(20) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` int(11) NOT NULL DEFAULT '1',
  `editable` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`result_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (1,31,'2015-07-16','89','100',27,8,'2015-07-16 23:28:49','2015-07-16 18:28:49',1,1),(2,31,'2015-07-16','50','100',29,8,'2015-07-16 23:28:49','2015-07-16 18:28:49',1,1);
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salary`
--

DROP TABLE IF EXISTS `salary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salary` (
  `salary_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `allowances` int(11) NOT NULL,
  `deductions` int(11) NOT NULL,
  `payable` int(11) NOT NULL,
  `staff_id` bigint(20) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` int(11) NOT NULL DEFAULT '1',
  `editable` int(11) NOT NULL DEFAULT '1',
  `paid` int(5) NOT NULL,
  `total_presents` int(5) NOT NULL,
  `total_leaves` int(5) NOT NULL,
  `total_absents` int(5) NOT NULL,
  `total_late_presents` int(5) DEFAULT NULL,
  `month` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `year` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `branch_id` int(1) NOT NULL,
  `arrears_and_advance` int(10) NOT NULL,
  PRIMARY KEY (`salary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salary`
--

LOCK TABLES `salary` WRITE;
/*!40000 ALTER TABLE `salary` DISABLE KEYS */;
INSERT INTO `salary` VALUES (1,500,500,18000,1,'2015-08-05 22:48:07','2015-08-05 17:48:07',1,1,18000,2,1,0,0,'07','2015',1,0),(2,500,500,17419,1,'2015-08-06 12:53:18','2015-08-06 07:53:19',1,1,17419,2,1,0,0,'07','2015',1,0);
/*!40000 ALTER TABLE `salary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salary_setting`
--

DROP TABLE IF EXISTS `salary_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salary_setting` (
  `ss_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `single_group` int(11) NOT NULL,
  `staff_and_designations` bigint(20) NOT NULL,
  `salary_type` int(11) NOT NULL,
  `salary_type_value` int(11) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `editable` int(11) NOT NULL DEFAULT '1',
  `deletable` int(11) NOT NULL DEFAULT '1',
  `branch_id` int(10) NOT NULL,
  PRIMARY KEY (`ss_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salary_setting`
--

LOCK TABLES `salary_setting` WRITE;
/*!40000 ALTER TABLE `salary_setting` DISABLE KEYS */;
INSERT INTO `salary_setting` VALUES (1,1,1,36,-500,'2015-07-27 12:52:01','2015-07-27 07:52:01',0,1,1),(4,1,1,34,500,'2015-07-27 13:04:10','2015-07-27 08:04:10',1,1,1),(6,2,23,34,600,'2015-07-27 16:24:24','2015-07-27 11:24:25',1,1,1),(8,2,22,36,-400,'2015-07-27 16:24:25','2015-07-27 11:24:25',1,1,1);
/*!40000 ALTER TABLE `salary_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section_attendance`
--

DROP TABLE IF EXISTS `section_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section_attendance` (
  `sa_id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'section attendance id',
  `section_id` int(10) NOT NULL,
  `total_present` int(5) NOT NULL,
  `total_absent` int(5) NOT NULL,
  `total_leave` int(5) NOT NULL,
  `total_late` int(5) NOT NULL,
  `attendance_date` date DEFAULT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subject_id` int(10) NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '10',
  `class_id` int(10) NOT NULL,
  `deletable` smallint(5) NOT NULL DEFAULT '1',
  `editable` smallint(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='section attendance ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section_attendance`
--

LOCK TABLES `section_attendance` WRITE;
/*!40000 ALTER TABLE `section_attendance` DISABLE KEYS */;
INSERT INTO `section_attendance` VALUES (1,1,20,8,3,0,'2015-07-09','2015-06-30 01:37:05','2015-06-29 20:37:05',30,1,0,1,1),(4,1,20,8,3,0,'2015-07-16','2015-06-30 01:41:17','2015-06-29 20:41:17',30,1,0,1,1),(5,3,25,7,2,1,'2015-07-20','2015-06-30 01:48:39','2015-06-29 20:48:39',30,1,2,1,1),(6,3,25,7,2,1,'2015-06-30','2015-06-30 02:00:17','2015-06-29 21:00:17',30,1,2,1,1),(7,3,25,7,2,1,'2015-06-30','2015-06-30 02:00:52','2015-06-29 21:00:52',30,1,6,1,1),(8,3,25,7,2,1,'2015-06-30','2015-06-30 02:02:41','2015-06-29 21:02:41',30,1,1,1,1),(9,3,19,4,5,3,'2015-07-08','2015-07-07 12:05:58','2015-07-07 07:05:58',28,1,7,1,1);
/*!40000 ALTER TABLE `section_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `section_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `section_name` varchar(50) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `class_id` int(10) NOT NULL,
  `deletable` int(11) NOT NULL DEFAULT '1',
  `editable` int(11) NOT NULL DEFAULT '1',
  `branch_id` int(10) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,'Golden','2015-01-23 16:15:41','2015-01-23 10:15:41',1,1,1,1),(2,'Blue','2015-01-23 16:37:54','2015-01-23 10:37:54',2,1,1,1),(3,'Sky Blue','2015-01-23 16:38:07','2015-01-23 10:38:07',2,1,1,1),(4,'Orange','2015-03-03 00:31:09','2015-03-02 14:31:09',7,1,1,1),(5,'Lemon','2015-03-03 00:34:37','2015-03-02 14:34:37',7,1,1,1),(6,'Mango','2015-03-03 00:36:54','2015-03-02 14:36:54',8,1,1,1),(7,'Lemone','2015-03-03 00:39:20','2015-03-02 14:39:20',8,1,1,1),(8,'Apple','2015-03-03 18:08:56','2015-03-03 08:08:56',8,1,1,1);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `set_prog_fee`
--

DROP TABLE IF EXISTS `set_prog_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_prog_fee` (
  `pf_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'program fee id',
  `fee` int(10) DEFAULT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fc_id` int(10) NOT NULL COMMENT 'fee category id',
  `class_id` int(10) NOT NULL,
  `branch_id` int(10) NOT NULL,
  `period` smallint(6) NOT NULL,
  `editable` int(5) NOT NULL DEFAULT '1',
  `deletable` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='setting/configration for program fee for each class. ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `set_prog_fee`
--

LOCK TABLES `set_prog_fee` WRITE;
/*!40000 ALTER TABLE `set_prog_fee` DISABLE KEYS */;
INSERT INTO `set_prog_fee` VALUES (1,2000,'2015-03-16 16:59:17','2015-03-16 06:59:17',8,7,1,5,1,1),(2,2000,'2015-06-12 21:30:51','2015-06-12 16:30:52',8,7,1,12,1,1),(3,500,'2015-06-12 21:31:37','2015-06-12 16:31:37',9,7,1,12,1,1),(4,1500,'2015-06-12 21:32:15','2015-06-12 16:32:15',10,7,1,12,1,1),(5,1200,'2015-06-12 21:32:57','2015-06-12 16:32:57',11,7,1,1,1,1),(6,2000,'2015-06-12 21:33:29','2015-06-12 16:33:29',12,7,1,12,1,1),(7,200,'2015-06-12 21:34:11','2015-06-12 16:34:11',13,7,1,1,1,1),(8,500,'2015-06-12 21:34:32','2015-06-12 16:34:32',14,7,1,12,1,1);
/*!40000 ALTER TABLE `set_prog_fee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_ai`
--

DROP TABLE IF EXISTS `staff_ai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_ai` (
  `staff_aid` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'staff academic id',
  `a_degree_type` smallint(6) DEFAULT NULL,
  `a_class_name` varchar(100) DEFAULT NULL,
  `a_institute` varchar(200) DEFAULT NULL,
  `a_obtained_marks` int(11) DEFAULT NULL,
  `a_total_marks` int(11) DEFAULT NULL,
  `a_starting_date` date DEFAULT NULL,
  `a_ending_date` date DEFAULT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` smallint(6) DEFAULT '1',
  `staff_id` bigint(20) NOT NULL,
  `editable` int(11) NOT NULL DEFAULT '1',
  `branch_id` int(10) NOT NULL,
  PRIMARY KEY (`staff_aid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_ai`
--

LOCK TABLES `staff_ai` WRITE;
/*!40000 ALTER TABLE `staff_ai` DISABLE KEYS */;
INSERT INTO `staff_ai` VALUES (1,6,'BA Urdu','IUB',3,4,'2015-04-14','2015-04-21','2015-04-04 23:26:11','2015-04-04 18:26:11',1,1,1,1),(2,6,'BA Urdu','IUB',3,4,'2015-04-14','2015-04-21','2015-04-04 23:30:39','2015-04-04 18:30:39',1,2,1,1);
/*!40000 ALTER TABLE `staff_ai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_attendance`
--

DROP TABLE IF EXISTS `staff_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_attendance` (
  `a_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attendance_status` smallint(5) NOT NULL,
  `attendance_time` datetime DEFAULT NULL,
  `staff_id` bigint(20) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletable` smallint(6) DEFAULT '1',
  `editable` int(11) NOT NULL DEFAULT '1',
  `branch_id` int(10) NOT NULL,
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_attendance`
--

LOCK TABLES `staff_attendance` WRITE;
/*!40000 ALTER TABLE `staff_attendance` DISABLE KEYS */;
INSERT INTO `staff_attendance` VALUES (1,1,'2015-07-08 12:36:20',1,'2015-07-08 12:36:27','2015-07-08 07:36:27',1,1,1),(2,1,'2015-07-07 12:36:20',1,'2015-07-08 12:36:55','2015-07-08 07:36:55',1,1,1),(3,1,'2015-07-08 16:02:55',2,'2015-07-08 16:02:58','2015-07-09 07:13:20',1,1,1),(4,1,'2015-07-06 16:02:55',2,'2015-07-08 16:09:05','2015-07-08 11:09:05',1,1,1),(5,3,'2015-07-04 16:02:55',2,'2015-07-08 16:36:42','2015-07-08 11:36:42',1,1,1),(6,4,'2015-08-01 16:48:07',2,'2015-07-08 16:48:30','2015-08-05 06:15:57',1,1,1),(7,3,'2015-07-08 16:51:51',1,'2015-07-08 16:53:01','2015-07-08 11:53:01',1,1,1),(8,3,'2015-08-05 12:40:16',1,'2015-08-05 12:40:53','2015-08-05 07:40:53',1,1,1);
/*!40000 ALTER TABLE `staff_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_exi`
--

DROP TABLE IF EXISTS `staff_exi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_exi` (
  `experience_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `experience_type` smallint(6) DEFAULT NULL,
  `designation_name` varchar(100) DEFAULT NULL,
  `organization_name` varchar(500) DEFAULT NULL,
  `salary` int(11) DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `ending_date` date DEFAULT NULL,
  `staff_id` bigint(20) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` smallint(6) DEFAULT '1',
  `editable` int(11) NOT NULL DEFAULT '1',
  `branch_id` int(10) NOT NULL,
  PRIMARY KEY (`experience_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_exi`
--

LOCK TABLES `staff_exi` WRITE;
/*!40000 ALTER TABLE `staff_exi` DISABLE KEYS */;
INSERT INTO `staff_exi` VALUES (1,21,'IUB','IQRA',15000,'2015-04-23','2015-04-23',1,'2015-04-04 23:26:11','2015-04-04 18:26:11',1,1,1),(2,21,'IUB','IQRA',15000,'2015-04-23','2015-04-23',2,'2015-04-04 23:30:39','2015-04-04 18:30:39',1,1,1);
/*!40000 ALTER TABLE `staff_exi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_ji`
--

DROP TABLE IF EXISTS `staff_ji`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_ji` (
  `job_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `staff_designation` smallint(5) DEFAULT NULL,
  `doa` date NOT NULL COMMENT 'date of appointment',
  `job_type` smallint(5) DEFAULT NULL,
  `basic_salary` int(11) DEFAULT NULL,
  `comments` text,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `editable` smallint(6) DEFAULT '1',
  `staff_id` bigint(20) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `b_staff_id` int(11) NOT NULL,
  `deletable` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_ji`
--

LOCK TABLES `staff_ji` WRITE;
/*!40000 ALTER TABLE `staff_ji` DISABLE KEYS */;
INSERT INTO `staff_ji` VALUES (1,22,'2015-04-15',24,18000,'good teacher.. honest','2015-04-04 23:26:11','2015-04-04 18:26:11',1,1,1,1,1),(2,22,'2015-04-15',24,18000,'good teacher..','2015-04-04 23:30:39','2015-04-04 18:30:39',1,2,1,2,1);
/*!40000 ALTER TABLE `staff_ji` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_pi`
--

DROP TABLE IF EXISTS `staff_pi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_pi` (
  `staff_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `spouse_name` varchar(30) DEFAULT NULL,
  `gender` smallint(6) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `phone_no` varchar(15) DEFAULT NULL,
  `cell_no` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cnic` varchar(15) DEFAULT NULL,
  `marital_status` smallint(6) DEFAULT NULL,
  `religion` smallint(6) DEFAULT NULL,
  `domicile` smallint(5) DEFAULT NULL,
  `country` smallint(6) DEFAULT NULL,
  `city` smallint(6) DEFAULT NULL,
  `other_city` varchar(100) DEFAULT NULL,
  `town` varchar(100) DEFAULT NULL,
  `permanent_address` varchar(500) DEFAULT NULL,
  `present_address` varchar(500) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` smallint(6) DEFAULT '1',
  `spouse_type` int(11) DEFAULT NULL,
  `editable` int(11) NOT NULL DEFAULT '1',
  `branch_id` int(10) NOT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_pi`
--

LOCK TABLES `staff_pi` WRITE;
/*!40000 ALTER TABLE `staff_pi` DISABLE KEYS */;
INSERT INTO `staff_pi` VALUES (1,'Asif','Khan','Hassan Khan',1,'2015-04-01','15315','123456789','hassan@gmail.com','12345678',1,1,4,162,1,'Mahajor town','Mahajor Colony','House 7, st 4, same','House 7, st 4','2015-04-07 22:06:25','2015-04-04 18:26:11',1,17,1,1),(2,'Ali','Khan','Atif  Khan',1,'2015-04-01','2151321','123456789','hassan@gmail.com','15354654',1,1,4,162,1,'Mahajor town','Mahajor Colony','House 7, st 4','House 7, st 4','2015-04-04 23:30:39','2015-04-04 18:30:39',1,17,1,1);
/*!40000 ALTER TABLE `staff_pi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `std_attendance`
--

DROP TABLE IF EXISTS `std_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `std_attendance` (
  `a_id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'section attendance id',
  `ad` datetime NOT NULL COMMENT 'attendance datetime',
  `subject_id` int(20) NOT NULL,
  `attendance_status` int(5) NOT NULL COMMENT '1-present, absent, leave, late',
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` smallint(5) NOT NULL DEFAULT '1',
  `editable` smallint(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='student attendance ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `std_attendance`
--

LOCK TABLES `std_attendance` WRITE;
/*!40000 ALTER TABLE `std_attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `std_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_ai`
--

DROP TABLE IF EXISTS `student_ai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_ai` (
  `academic_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `student_id` bigint(20) DEFAULT NULL,
  `a_degree_type` smallint(6) DEFAULT NULL,
  `a_class_name` varchar(100) DEFAULT NULL,
  `a_institute` varchar(200) DEFAULT NULL,
  `a_obtained_marks` decimal(8,3) DEFAULT NULL,
  `a_total_marks` decimal(8,3) DEFAULT NULL,
  `a_starting_date` date DEFAULT NULL,
  `a_ending_date` date DEFAULT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date_time` datetime DEFAULT NULL,
  `editable` smallint(5) NOT NULL DEFAULT '1',
  `deletable` smallint(5) NOT NULL DEFAULT '1',
  `branch_id` int(10) NOT NULL,
  PRIMARY KEY (`academic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='Student can have multiple academic records. ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_ai`
--

LOCK TABLES `student_ai` WRITE;
/*!40000 ALTER TABLE `student_ai` DISABLE KEYS */;
INSERT INTO `student_ai` VALUES (6,7,1,'4th','Iqra School',450.000,500.000,'2015-03-19','2015-03-31','2015-03-05 12:06:02','2015-03-05 22:06:02',1,1,1),(7,8,1,'4th','Iqra School system',450.000,500.000,'2015-07-07','2015-07-08','2015-03-05 12:09:05','2015-03-05 22:09:05',1,1,1),(8,9,1,'4th','Iqra School',450.000,500.000,'2015-03-19','2015-03-31','2015-03-05 12:09:45','2015-03-05 22:09:45',1,1,2);
/*!40000 ALTER TABLE `student_ai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_ei`
--

DROP TABLE IF EXISTS `student_ei`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_ei` (
  `enrollment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dor` date DEFAULT NULL,
  `class_id` smallint(5) DEFAULT NULL,
  `section_id` smallint(5) DEFAULT NULL,
  `fee` decimal(10,2) DEFAULT NULL,
  `enrollment_status` smallint(5) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `student_id` bigint(20) DEFAULT NULL,
  `branch_id` int(10) NOT NULL,
  `deletable` smallint(1) NOT NULL DEFAULT '1',
  `editable` smallint(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`enrollment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='student enrollment status';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_ei`
--

LOCK TABLES `student_ei` WRITE;
/*!40000 ALTER TABLE `student_ei` DISABLE KEYS */;
INSERT INTO `student_ei` VALUES (6,'2015-03-19',7,3,0.00,1,'2015-03-27 21:44:37','2015-03-05 12:06:02',7,1,1,1),(7,'2015-07-07',7,3,0.00,1,'2015-08-21 11:58:40','2015-03-05 12:09:05',8,1,1,1),(8,'2015-03-19',7,3,0.00,1,'2015-04-02 16:50:41','2015-03-05 12:09:45',9,2,1,1);
/*!40000 ALTER TABLE `student_ei` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_gi`
--

DROP TABLE IF EXISTS `student_gi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_gi` (
  `guardian_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `guardian_type` smallint(6) DEFAULT NULL,
  `guardian_name` varchar(100) DEFAULT NULL,
  `guardian_cnic` varchar(15) DEFAULT NULL,
  `guardian_status` smallint(6) DEFAULT NULL,
  `guardian_occupation` varchar(50) DEFAULT NULL,
  `guardian_education` smallint(6) DEFAULT NULL,
  `guardian_phone` varchar(15) DEFAULT NULL,
  `guardian_email` varchar(100) DEFAULT NULL,
  `guardian_cellno1` varchar(15) DEFAULT NULL,
  `guardian_cellno2` varchar(15) DEFAULT NULL,
  `student_id` bigint(20) DEFAULT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date_time` datetime DEFAULT NULL,
  `editable` smallint(5) NOT NULL DEFAULT '1',
  `deletable` smallint(5) NOT NULL DEFAULT '1',
  `branch_id` int(10) NOT NULL,
  PRIMARY KEY (`guardian_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='Guardian information. One student can have multiple guardian.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_gi`
--

LOCK TABLES `student_gi` WRITE;
/*!40000 ALTER TABLE `student_gi` DISABLE KEYS */;
INSERT INTO `student_gi` VALUES (6,1,'Nasir','23823487',2,'Land Lord',4,'234747','nasir@gmail.com','123456789','',7,'2015-03-05 12:06:02','2015-03-05 22:06:02',1,1,1),(7,1,'Nasir','23823487',2,'Land Lord',4,'234747','nasir@gmail.com','123456789','45151',8,'2015-03-05 12:09:05','2015-03-05 22:09:05',1,1,1),(8,1,'Nasir','23823487',2,'Land Lord',4,'234747','nasir@gmail.com','123456789','',9,'2015-03-05 12:09:45','2015-03-05 22:09:45',1,1,2);
/*!40000 ALTER TABLE `student_gi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_pi`
--

DROP TABLE IF EXISTS `student_pi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_pi` (
  `student_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `roll_no` bigint(20) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `gender` smallint(5) DEFAULT NULL,
  `caste` varchar(50) DEFAULT NULL,
  `cnic` varchar(15) DEFAULT NULL,
  `tnbs` smallint(5) DEFAULT NULL COMMENT 'total number of brothers and sisters',
  `pbse` smallint(5) DEFAULT NULL COMMENT 'position in brothers and sister from eldest',
  `dob` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cell_no` varchar(15) DEFAULT NULL,
  `domicile` smallint(5) DEFAULT NULL,
  `religion` smallint(5) DEFAULT NULL,
  `town` varchar(50) DEFAULT NULL,
  `city` int(5) DEFAULT NULL,
  `country` int(5) DEFAULT NULL,
  `permanent_address` text,
  `present_address` text,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date_time` datetime NOT NULL,
  `deletable` smallint(6) DEFAULT NULL,
  `other_city` varchar(100) DEFAULT NULL,
  `editable` smallint(5) NOT NULL DEFAULT '1',
  `branch_id` int(10) NOT NULL,
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='personal information.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_pi`
--

LOCK TABLES `student_pi` WRITE;
/*!40000 ALTER TABLE `student_pi` DISABLE KEYS */;
INSERT INTO `student_pi` VALUES (7,1,'Ahsan','Raza',1,'Raza','1234565',4,2,'2015-03-12','asl@gamil.com','1234654',4,1,'Chakallaa',47,162,'house 7','house 7','2015-03-05 12:06:02','2015-03-05 22:06:01',1,'Chaklalal',1,1),(8,2,'Ahmad','Ali',1,'Raza','12345654',4,2,'2015-08-21','asl@gamil.com','1234654',4,1,'Chakallaa',1,162,'house 7','house 7','2015-03-05 12:09:05','2015-03-05 22:09:05',1,'Karachi',1,1),(9,1,'Rasheed','Ali',1,'Raza','12345654',4,2,'2015-03-12','asl@gamil.com','1234654',4,1,'Chakallaa',47,162,'house 7','house 7','2015-03-05 12:09:45','2015-03-05 22:09:45',1,'Chaklalal',1,2);
/*!40000 ALTER TABLE `student_pi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) DEFAULT NULL,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `about_me` text,
  `status` smallint(6) DEFAULT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_type` smallint(6) DEFAULT NULL,
  `branch_id` int(10) NOT NULL,
  `deletable` int(11) NOT NULL DEFAULT '1',
  `editable` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'super','Mr','Super','super','admin@gmiail.com','I am super',1,'2015-02-27 19:43:22','2015-02-27 09:43:22',1,-1,0,0),(2,'admin','Mr','admin','admin','admin@gmail.com','I am admin',1,'2015-02-27 19:29:34','2015-02-27 09:14:27',2,1,1,1),(3,'admin2','Kanwar','Altaf','admin2','kunwar.altaf@gmail.com','It is me . I am principal.',1,'2015-03-14 21:56:11','2015-03-14 11:56:11',2,2,1,1),(4,'kanwar','Kanwar','Altaf','kanwar','kanwar.altaf@citi.edu.pk','I am principle of CITI ...',0,'2015-08-07 10:56:39','2015-08-07 05:56:41',1,-1,1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `var_name`
--

DROP TABLE IF EXISTS `var_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `var_name` (
  `var_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `var_name` varchar(100) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` int(2) DEFAULT '1',
  `editable` int(2) DEFAULT '1',
  `var_description` varchar(45) DEFAULT NULL,
  `var_title` varchar(100) NOT NULL,
  PRIMARY KEY (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `var_name`
--

LOCK TABLES `var_name` WRITE;
/*!40000 ALTER TABLE `var_name` DISABLE KEYS */;
INSERT INTO `var_name` VALUES (1,'gender','2015-01-24 23:27:37','2015-01-24 13:27:53',1,0,'Gender','Gender'),(3,'subject_group','2015-01-25 15:38:54','2015-01-25 05:39:12',1,0,'Subject Groups','Subject Group'),(4,'fee_category','2015-03-16 15:14:07','2015-03-16 05:14:07',1,0,'Fee Category','Fee Category'),(5,'a_spouse_type','2015-04-04 14:46:40','2015-04-04 09:46:40',1,0,'These values will be used for staff informati','Spouse type'),(6,'a_experience_type','2015-04-04 16:46:05','2015-04-04 11:46:05',1,0,'it contain the list of type of experience.','Experience Type'),(7,'a_staff_designation','2015-04-04 19:46:19','2015-04-04 14:46:19',1,0,'Type of staff in your organization.','Staff Designation'),(8,'a_job_type','2015-04-04 20:32:34','2015-04-04 15:32:34',1,0,'Job types in your organization. Contract, per','Job Types'),(9,'a_subjects','2015-06-29 23:58:33','2015-06-29 18:58:33',1,0,'subject for the classes.','Subjects'),(10,'result_category','2015-07-16 00:28:45','2015-07-15 19:28:45',1,0,'Add the result category like : 1st term, 2nd ','Result Category'),(11,'staff_allowances_type','2015-07-22 16:25:32','2015-07-22 11:25:33',1,0,'Type of allowance for staff salary.','Staff Allowances'),(12,'staff_deduction_type','2015-07-22 16:26:59','2015-07-22 11:26:59',1,0,'Type of deductions for staff salary.','Staff Deductions');
/*!40000 ALTER TABLE `var_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `var_value`
--

DROP TABLE IF EXISTS `var_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `var_value` (
  `var_vid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'var value id',
  `var_value` varchar(500) NOT NULL,
  `var_id` bigint(20) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `last_updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletable` int(11) DEFAULT '1',
  `editable` int(11) DEFAULT '1',
  `variable_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`var_vid`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `var_value`
--

LOCK TABLES `var_value` WRITE;
/*!40000 ALTER TABLE `var_value` DISABLE KEYS */;
INSERT INTO `var_value` VALUES (3,'General Science',3,'2015-03-16 14:55:10','2015-01-25 05:39:52',1,1,0),(5,'Male',1,'2015-03-16 13:28:04','2015-03-16 03:28:04',1,1,0),(6,'Female',1,'2015-03-16 13:28:14','2015-03-16 03:28:14',0,1,0),(8,'Process Fee',4,'2015-03-16 15:32:42','2015-03-16 05:32:42',1,1,0),(9,'Library Fund',4,'2015-03-16 15:32:52','2015-03-16 05:32:52',1,1,0),(10,'Computer Fund',4,'2015-03-16 18:02:56','2015-03-16 08:02:56',1,1,0),(11,'Monthly Fee',4,'2015-03-16 19:38:22','2015-03-16 09:38:22',1,1,0),(12,'Stationary Fee',4,'2015-03-16 19:38:30','2015-03-16 09:38:30',1,1,0),(13,'Form Charges',4,'2015-03-16 19:38:45','2015-03-16 09:38:45',1,1,0),(14,'Medical Fee',4,'2015-03-16 19:38:54','2015-03-16 09:38:54',1,1,0),(15,'Other Fee',4,'2015-03-16 19:40:03','2015-03-16 09:40:03',1,1,0),(16,'Other',1,'2015-03-16 22:52:26','2015-03-16 12:52:26',1,1,0),(17,'Father',5,'2015-04-04 14:48:39','2015-04-04 09:48:39',1,1,0),(18,'Husband',5,'2015-04-04 14:48:55','2015-04-04 09:48:55',1,1,0),(19,'Free Internship',6,'2015-04-04 17:03:26','2015-04-04 12:03:26',1,1,0),(20,'Paid Internship',6,'2015-04-04 17:03:31','2015-04-04 12:03:31',1,1,0),(21,'Job',6,'2015-04-04 17:03:52','2015-04-04 12:03:52',1,1,0),(22,'Teacher',7,'2015-04-04 19:46:37','2015-04-04 14:46:37',1,1,0),(23,'Principal',7,'2015-04-04 19:47:17','2015-04-04 14:47:17',1,1,0),(24,'Contract',8,'2015-04-04 20:32:55','2015-04-04 15:32:55',1,1,0),(25,'Permanent',8,'2015-04-04 20:33:03','2015-04-04 15:33:03',1,1,0),(26,'Mother',5,'2015-06-11 13:06:48','2015-06-11 08:06:48',1,1,0),(27,'Math',9,'2015-06-29 23:59:00','2015-06-29 18:59:00',1,1,0),(28,'English',9,'2015-06-29 23:59:08','2015-06-29 18:59:08',1,1,0),(29,'Urdu',9,'2015-06-29 23:59:17','2015-06-29 18:59:17',1,1,0),(30,'All',9,'2015-06-30 00:44:32','2015-06-29 19:44:32',1,0,0),(31,'1st Term',10,'2015-07-16 00:31:20','2015-07-15 19:31:21',1,1,0),(32,'2st Term',10,'2015-07-16 00:31:30','2015-07-15 19:31:30',1,1,0),(33,'Annual Term',10,'2015-07-16 00:31:37','2015-07-15 19:31:37',1,1,0),(34,'Transportation',11,'2015-07-23 01:45:27','2015-07-22 20:45:27',1,1,600),(35,'Medical',11,'2015-07-23 02:01:48','2015-07-22 21:01:48',1,1,700),(36,'Absent Fine',12,'2015-07-23 02:03:33','2015-07-22 21:03:33',1,1,100);
/*!40000 ALTER TABLE `var_value` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-24 19:20:09
