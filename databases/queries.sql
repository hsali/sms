SELECT  cbs.`branch_id`, b.`branch_name`,  c.`class_name`  FROM
`branches` AS b INNER JOIN `comb_branch_class` AS cbs ON  b.`branch_id`= cbs.`branch_id`  AND cbs.`class_id`=5
INNER JOIN `classes` AS c ON cbs.`class_id`=c.`class_id` ;

SELECT  s.`section_id`, c.`class_name` , s.`section_name`  FROM
`classes` AS c INNER JOIN `sections` AS s ON s.`class_id`=c.`class_id`;

SELECT sp.`student_id`, b.`branch_name`,
 sp.`first_name`,
 sp.`last_name`, sg.`guardian_name`,
 sp.`roll_no`, c.`class_name`, sp.`dob` 
 FROM `student_pi` AS spstudent_pi
 INNER JOIN `student_gi` AS sg ON sg.`student_id`=sp.`student_id` 
 INNER JOIN `studen_ei` AS se ON se.`student_id`=sp.`student_id`  
 INNER JOIN `branches` AS b ON b.`branch_id`= se.`branch_id` 
 INNER JOIN `classes` AS c ON c.`class_id`=se.`class_id`;
 
 select  sp.student_id, sp.roll_no, sp.first_name, sp.last_name,
 b.branch_name, c.class_name, s.section_name
 from student_pi as sp
 inner join student_ei as se on se.student_id=sp.student_id 
 inner join branches as b on b.branch_id= se.branch_id
 inner join classes as c on c.class_id=se.class_id
 inner join sections as s on s.section_id=se.section_id;
 
 select pi.student_id, pi.roll_no, pi.first_name, pi.last_name, c.class_name, s.section_name from
 student_pi as pi 
 inner join student_ei as ei on ei.student_id=pi.student_id
 inner join classes as c on c.class_id=ei.class_id
 inner join sections as s on s.section_id=ei.section_id
 inner join branches as b on b.branch_id=ei.branch_id AND b.branch_id=1;

-- view students 
SELECT sp.`student_id`, b.`branch_name`, sp.`first_name`, sp.`last_name`, sg.`guardian_name`, sp.`roll_no`, c.`class_name`, sp.`dob`  FROM `st_pi` AS sp INNER JOIN `st_gi` AS sg ON sg.`student_id`=sp.`student_id` INNER JOIN `st_ei` AS se ON se.`student_id`=sp.`student_id`  INNER JOIN `branches` AS b ON b.`branch_id`= se.`branch_id` INNER JOIN `classes` AS c ON c.`class_id`=se.`now_class`;