<?php


class M_attendance extends CI_Model {
    public function __construct(){

    }
public function view_section_attendance(){
    $branch_id=$this->session->userdata("branch_id");
    $sql = "SELECT sa.sa_id,c.class_name, s.section_name, vv.var_value as subject_name, sa.total_present, sa.total_absent, sa.total_leave, sa.total_late, sa.attendance_date,sa.last_updated_date_time from section_attendance as sa join sections as s ON sa.section_id=s.section_id
JOIN var_value as vv on vv.var_vid=sa.subject_id
JOIN classes as c on c.class_id=sa.class_id
join var_name as vn on vn.var_id=vv.var_id && sa.deletable='1' && sa.branch_id=" . $branch_id . ";";
    $result=$this->db->query($sql);
    if($result->num_rows>0){
        return $result->result_array();
    }
    else
        return 0;

}
    public function view_section_attendance_by_id($id){
        $branch_id=$this->session->userdata("branch_id");
        $sql="
SELECT sa.sa_id,c.class_name, s.section_name, vv.var_value as subject_name, sa.total_present, sa.total_absent, sa.total_leave, sa.total_late, sa.attendance_date,sa.last_updated_date_time from section_attendance as sa join sections as s ON sa.section_id=s.section_id
  JOIN var_value as vv on vv.var_vid=sa.subject_id
  JOIN classes as c on c.class_id=sa.class_id
  join var_name as vn on vn.var_id=vv.var_id && sa.deletable='1'  && sa.branch_id=" . $branch_id . " && sa.sa_id=" . $id . ";";
        $result=$this->db->query($sql);
        if($result->num_rows>0){
            return $result->result_array();
        }
        else
            return 0;

    }

    public function view_staff_attendance(){
//        $branch_id=$this->session->userdata("branch_id");

$branch_id=1;
        $query="SELECT st.a_id,concat(sp.first_name,' ',sp.last_name) as staff_name, st.attendance_status, st.staff_id,st.attendance_time FROM staff_attendance as st
JOIN staff_pi as sp on sp.staff_id=st.staff_id && st.deletable='1' && st.branch_id='".$branch_id."' ORDER BY st.attendance_time DESC";
   $result=$this->db->query($query);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else {
            return FALSE;
        }
    }
    public function attendance_id_into_badge($id){
        $color="";
    }

}