<?php

class M_student extends CI_Model {

    public function __construct() {
        
    }

    /**
     * get roll no
     * @param $branch_id
     * @return mixed
     */
    public function get_roll_no($branch_id) {
        $this->db->where('branch_id', $branch_id);
        $this->db->from('student_ei');
        $rn = $this->db->count_all_results();
        return $rn + 1;
    }

    /**
     * insert record and return id
     * @param $table_name
     * @param $data
     */
    public function ins_rec_ri($table_name, $data) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function view_students() {
        $branch_id = $this->session->userdata("branch_id");

            $sql = "SELECT sp.student_id, sp.roll_no, sp.first_name, sp.last_name, sg.guardian_name,sg.guardian_cellno1, se.class_id, se.section_id, sp.editable, sp.deletable from student_pi as sp
JOIN student_gi as sg on sg.student_id=sp.student_id && sg.deletable='1' && sg.branch_id='".$branch_id."'
JOIN  student_ei as se on se.student_id=sp.student_id && se.deletable='1' && se.branch_id='".$branch_id."'
WHERE sp.deletable='1' && sp.branch_id='".$branch_id."';";
            $query = $this->db->query($sql);

            return $query->result_array();
        }

}
