<?php

/**
 *
 * Class M_functions
 */
class M_functions extends CI_Model
{
    public function __construct()
    {

    }

    public function user_status($id)
    {

        switch ($id) {
            case "0":
                return "Inactive";
                break;
            case "1":
                return "Active";
                break;
            default:
                return 0;
                break;
        }
    }

    public function user_type($id)
    {

        switch ($id) {
            case "1":
                return "Super";
                break;
            case "2":
                return "Admin";
                break;
            case "3":
                return "Teacher";
                break;
            case "4":
                return "Parent";
                break;
            case "5":
                return "Student";
                break;
            default:
                return 0;
                break;
        }
    }

    public function initiate_sidebar_variables()
    {
        $total_branches = $this->m_common->sel_spe_array_counts("branches", array("deletable" => 1, "editable" => 1), "branch_id,branch_name");
        $total_classes = $this->m_common->sel_spe_array_counts("classes", array("deletable" => 1, "editable" => 1), "class_id,class_name");
        $total_sections = $this->m_common->sel_spe_array_counts("sections", array("deletable" => 1, "editable" => 1), "section_id,section_name");
        $total_users = $this->m_common->sel_spe_array_counts("users", array("deletable" => 1, "editable" => 1), "user_id,user_name");
        $total_students = $this->m_common->sel_spe_array_counts("student_ei", array(), "student_id");
        $this->config->set_item('total_branches', $total_branches);
        $this->config->set_item('total_classes', $total_classes);
        $this->config->set_item('total_sections', $total_sections);
        $this->config->set_item('total_users', $total_users);
        $user_type = $this->session->userdata("user_type");
        $branch_id = $this->session->userdata("branch_id");
        if ($user_type == "1") {
            $total_students = $this->m_common->sel_spe_array_counts("student_ei", array(), "student_id");
            $total_staffs = $this->m_common->sel_spe_array_counts("staff_ji", array(), "staff_id");
        } elseif ($user_type == "2") {
            $total_students = $this->m_common->sel_spe_array_counts("student_ei", array("branch_id" => $branch_id), "student_id");
            $total_staffs = $this->m_common->sel_spe_array_counts("staff_ji", array("branch_id" => $branch_id, "deletable" => "1"), "staff_id");

        }
        empty($total_students)? "":$this->config->set_item("total_students", $total_students);
        empty($total_staffs)? "":$this->config->set_item("total_staffs", $total_staffs);

    }

    public function login_control()
    {
        $this->initiate_sidebar_variables();

        $this->session->set_userdata("pre_url", current_url());
        $se_login = $this->session->userdata("login");
        $se_user_id = $this->session->userdata("user_id");
        $se_branch_id = $this->session->userdata("branch_id");

        $se_user_name = $this->session->userdata("user_name");
        if ($se_login === FALSE
            && $se_user_id === FALSE
            && $se_user_name == FALSE
        ) {
            redirect(site_url("login"));
        } else {

        }

    }

    /**
     * user access function it allow the specific user to
     * @param type $id
     */
    public function user_access($id)
    {

        if ($id == '1') {
            return 'super';
        } elseif ($id == '2') {
            return 'admin';
        } elseif ($id == '3') {
            return 'staff';
        } elseif ($id == '4') {
            return 'student';
        } elseif ($id == '5') {
            return 'parent';
        }
    }

    /**
     * 100 => Error
     * 200 => Success
     * 300 => Warning
     *
     * @param $code
     * @param $message
     * @return array
     */
    public function code_sending($code, $message)
    {

        switch ($code) {
            case 100:
                $class = "danger";
                $header = "Error!";
                $msg_icon = "times";
                break;
            case 200:
                $class = "success";
                $header = "Success";
                $msg_icon = "check";
                break;
            case 300:
                $class = "warning";
                $header = "Warning!";
                $msg_icon = "warning";
                break;
            case 400:
                $class = "info";
                $header = "Info";
                $msg_icon = "info";
                break;
            default;
                $class = "";
                $header = "";

        }

        return array(
            "code" => $code,
            "msg_header" => $header,
            "msg_class" => $class,
            "msg_body" => $message,
            "msg_icon" => $msg_icon
        );
    }

    /**
     * Convert integer into month names
     * @param $id
     * @return String
     */
    public function num_into_month_name($id)
    {
        $month = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        return $month[$id - 1];

    }

    /**
     * Pure PHP function
     * @param string $cdata
     * @return array
     */

    /*   public function upload($cdata=""){


           $conf["field_name"]=empty(! $cdata["field_name"])? $cdata["field_name"]: "uploadPic";
           $conf["file_size"]="500000";
           $conf["target_dir"]=empty(! $cdata["target_dic"])? $cdata["target_dic"]: "pdb/";
           $conf["target_file"] = $conf["target_dir"] . basename($_FILES[$conf["field_name"]]["name"]);
           $conf["ext"] = pathinfo($conf["target_file"],PATHINFO_EXTENSION);
           $conf["file_name"] = pathinfo($conf["target_file"],PATHINFO_FILENAME);
           $conf["new_name"]="pdb/".$conf["target_dir"].$cdata["new_name"].".".$conf["ext"]  ;
           $uploadOk = 1;
           $message="";
   //
   //var_dump($conf);exit;
   // Check if image file is a actual image or fake image

               $check = getimagesize($_FILES[$conf["field_name"]]["tmp_name"]);
               if($check !== false) {
                   // echo "File is an image - " . $check["mime"] . ".";
                   $uploadOk = 1;
               } else {
                  $message.="File is not Image |  ";
               }
   // Check if file already exists
           if (file_exists($conf["new_name"])) {
   //            $uploadOk = 0;
   //            $message.= "File is already exists | ";
               unlink($conf["new_name"]);
           }
   // Check file size
           if ($_FILES[$conf["field_name"]]["size"] > $conf["file_size"]) {
               $uploadOk = 0;
               $message.= "Your File is too large | ";
           }
   // Allow certain file formats
           if($conf["ext"] != "jpg" && $conf["ext"] != "png" && $conf["ext"] != "jpeg"
               && $conf["ext"] != "gif" ) {
               $uploadOk = 0;
               $message.= "Only JPG, JPEG, PNG, GIF files are allowed | ";
           }
   //        var_dump($cdata); echo "</br>";
   //        var_dump($conf); echo "</br>";
   //        var_dump($uploadOk); echo "</br>";
   //        exit;

   // Check if $uploadOk is set to 0 by an error
           if ($uploadOk == 0) {
               $message.= "Your file was not uploaded ";
               return array("code"=>"2", "msg"=>$message);
   // if everything is ok, try to upload file
           } else {
               if (move_uploaded_file($_FILES[$conf["field_name"]]["tmp_name"], $conf["new_name"])) {
   //                echo "The file ". basename( $_FILES[$conf["field_name"]]["name"]). " has been uploaded.";
                   $message="File Uploaded";
                   return array("code"=>"1", "msg"=>$message);
               } else {

                   $message.= "There was an error uploading your file.";
                   return array("code"=>"2", "msg"=>$message);
               }
           }

       }*/

    /**
     * @param $data is associative array
     *
     */


    public function upload($data)
    {

        $config['upload_path'] = './pdb/' . $data["path_to_file"];
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['overwrite'] = TRUE;
        $data["upload_path"] = $config["upload_path"];
        $config['max_filename'] = "0";
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = FALSE;

        $config['file_name'] = $data["file_name"];

        if (file_exists($data["upload_path"] . "/" . $data["file_name"] . ".jpg")) {
            unlink($data["upload_path"] . "/" . $data["file_name"] . '.jpg');
        }
        if (file_exists($data["upload_path"] . "/" . $data["file_name"] . ".jpeg")) {
            unlink($data["upload_path"] . "/" . $data["file_name"] . '.jpeg');
        }
        if (file_exists($data["upload_path"] . "/" . $data["file_name"] . ".png")) {
            unlink($data["upload_path"] . "/" . $data["file_name"] . '.png');
        }
        if (file_exists($data["upload_path"] . "/" . $data["file_name"] . ".gif")) {
            unlink($data["upload_path"] . "/" . $data["file_name"] . '.gif');
        }

        $this->load->library('upload', $config);


        if (!$this->upload->do_upload($data["field_name"])) {
            $error = $this->upload->display_errors();

            return array(
                "msg" => $error,
                "condition" => "1"
            );

        } else {
            $data = $this->upload->data();

            return array(
                "msg" => $data,
                "condition" => "2"
            );
        }

    }

    public function check_file($upload_path, $file_name)
    {
        $data["upload_path"] = $upload_path;
        $data["file_name"] = $file_name;
        if (file_exists($data["upload_path"] . "/" . $data["file_name"] . ".jpg")) {
            unlink($data["upload_path"] . "/" . $data["file_name"] . '.jpg');
        }
        if (file_exists($data["upload_path"] . "/" . $data["file_name"] . ".jpeg")) {
            unlink($data["upload_path"] . "/" . $data["file_name"] . '.jpeg');
        }
        if (file_exists($data["upload_path"] . "/" . $data["file_name"] . ".png")) {
            unlink($data["upload_path"] . "/" . $data["file_name"] . '.png');
        }
        if (file_exists($data["upload_path"] . "/" . $data["file_name"] . ".gif")) {
            unlink($data["upload_path"] . "/" . $data["file_name"] . '.gif');
        }
    }
}