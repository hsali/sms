<?php

class M_result extends CI_Model {

    public function __construct() {

    }

    /**
     * get fees with specific fee ids
     * @param $ids
     * @return bool or array of received fees
     */
    public function get_received_fee_ids($ids){
        $sql="SELECT rf.rf_id,vv.var_value as fee_category ,rf.fee,rf.period FROM received_fee as rf
  JOIN var_value as vv on vv.var_vid=rf.fc_id
where rf.rf_id IN (".$ids.") && rf.deletable='1';";
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }
    public function view_all_results(){
        $branch_id=$this->session->userdata("branch_id");
        $sql="SELECT r.result_id,r.student_id,sp.first_name, sp.last_name,c.class_name,r.result_cat_id  as result_category_id, vv.var_value as subject_name, r.exam_date, r.obtained_marks, r.total_marks, round(r.obtained_marks/r.total_marks *100,2) as percentage
FROM result  as r
JOIN student_pi as sp on sp.student_id=r.student_id && sp.deletable='1'
JOIN student_ei as se on se.student_id=r.student_id && se.branch_id ='".$branch_id."' && se.deletable='1'
JOIN classes as c on c.class_id=se.class_id && c.deletable='1'
JOIN  var_value as vv on vv.var_vid=r.subject_id
WHERE r.deletable='1';";
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }

public function specific_result($student_id="",$result_category="",$subject_id="",$class_id="", $from_date="", $to_date=""){
    $sql="SELECT r.result_id,r.student_id,sp.first_name, sp.last_name,c.class_name,r.result_cat_id  as result_category_id, vv.var_value as subject_name, r.exam_date, r.obtained_marks, r.total_marks, round(r.obtained_marks/r.total_marks *100,2) as percentage
FROM result  as r
  JOIN student_pi as sp on sp.student_id=r.student_id && sp.deletable='1'
  JOIN student_ei as se on se.student_id=r.student_id && se.branch_id ='1' && se.deletable='1'
  JOIN classes as c on c.class_id=se.class_id && c.deletable='1' && c.class_id".$class_id."
  JOIN  var_value as vv on vv.var_vid=r.subject_id
WHERE r.deletable='1' && r.exam_date BETWEEN '".$from_date."' AND '".$to_date."' && r.student_id".$student_id." && r.subject_id ".$subject_id." && r.result_cat_id".$result_category." ;";
    $query=$this->db->query($sql);
    if($query->num_rows()>0){
        return $query->result_array();
    }
    else
        return FALSE;
}

}
