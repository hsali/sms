<?php

/**
 *
 * Class M_functions
 */
class Home extends CI_Model
{
    public function __construct()
    {

    }

    public function student_summary()
    {
        $total_branches = $this->m_common->sel_spe_array_counts("branches", array("deletable" => 1, "editable" => 1), "branch_id,branch_name");
        $branches = $this->m_common->sel_spe_array_fields("branches", array("deletable" => 1), "branch_id, branch_name");
        $student_summary = array();
        $total_students = $this->m_common->sel_spe_array_counts("student_ei", array("deletable" => "1"), "student_id");
        foreach ($branches as $branch) {
            $branch_students = $this->m_common->sel_spe_array_counts("student_ei", array("branch_id" => $branch["branch_id"], "deletable" => "1"), "student_id");
            $branch_students = $branch_students / $total_students * 100; // percentage
            $branch_students = round($branch_students, 2);
            array_push($student_summary, array("title" => $branch["branch_name"], "value" => $branch_students));

        }
        return $student_summary;
    }

    public function summary()
    {
        $total_students = $this->m_common->sel_spe_array_counts("student_pi", array("deletable" => "1"), "student_id");
        $total_student_males = $this->m_common->sel_spe_array_counts("student_pi", array("deletable" => "1", "gender" => "1"), "student_id");
        $total_student_females = $this->m_common->sel_spe_array_counts("student_pi", array("deletable" => "1", "gender" => "2"), "student_id");
        $total_staff_males = $this->m_common->sel_spe_array_counts("staff_pi", array("deletable" => "1", "gender" => "1"), "staff_id");
        $total_staff_females = $this->m_common->sel_spe_array_counts("staff_pi", array("deletable" => "1", "gender" => "2"), "staff_id");
        $total_staffs = $this->m_common->sel_spe_array_counts("staff_ji", array("deletable" => "1"), "staff_id");
        $total_fee = $this->m_common->get_sum_where("fee_receipts","received_payment",array("deletable"=>"1"));
        $total_salary = $this->m_common->get_sum_where("salary","paid",array("deletable"=>"1"));

        $summary = array(
            "total_students" => $total_students,
            "total_staffs" => $total_staffs,
            "total_student_males" => $total_student_males,
            "total_student_females" => $total_student_females,
            "total_staff_males" => $total_staff_males,
            "total_staff_females" => $total_staff_females,
            "total_fee"=>$total_fee,
            "total_salary"=>$total_salary

        );

        return $summary;
    }

    public function student_detail()
    {
        $branches = $this->m_common->sel_spe_array_fields("branches", array("deletable" => "1"), "branch_id, branch_name");
        $classes = $this->m_common->sel_spe_array_fields("classes", array("deletable" => "1"), "class_id, class_name");
        $total_branches = $this->m_common->sel_spe_array_counts("branches", array("deletable" => "1"), "branch_id");
        $total_classes = $this->m_common->sel_spe_array_counts("classes", array("deletable" => "1"), "class_id");
        $student_detail_mf = array();

        foreach ($branches as $branch) {
            $branch_student_males = $this->m_common->sel_spe_array_counts("student_pi", array("branch_id" => $branch["branch_id"], "deletable" => "1", "gender" => "1"), "student_id");
            $branch_student_females = $this->m_common->sel_spe_array_counts("student_pi", array("branch_id" => $branch["branch_id"], "deletable" => "1", "gender" => "2"), "student_id");
            array_push($student_detail_mf, array( "name" => $branch["branch_name"], "males" => $branch_student_males, "females" => $branch_student_females));

            foreach ($classes as $class) {
                $class_student_males = $this->total_class_student( $branch["branch_id"],$class["class_id"],1);
                $class_student_females =$this->total_class_student( $branch["branch_id"],$class["class_id"],2);
                array_push($student_detail_mf, array("name" => $class["class_name"], "males" => $class_student_males, "females" => $class_student_females));

            }
        }
        return $student_detail_mf;
    }


    public function staff_detail(){

        $branches = $this->m_common->sel_spe_array_fields("branches", array("deletable" => "1"), "branch_id, branch_name");
        $staff_detail_mf = array();

        foreach ($branches as $branch) {
            $branch_staff_males = $this->m_common->sel_spe_array_counts("staff_pi", array("branch_id" => $branch["branch_id"], "deletable" => "1", "gender" => "1"), "staff_id");
            $branch_staff_females = $this->m_common->sel_spe_array_counts("staff_pi", array("branch_id" => $branch["branch_id"], "deletable" => "1", "gender" => "2"), "staff_id");
            array_push($staff_detail_mf, array( "name" => $branch["branch_name"], "males" => $branch_staff_males, "females" => $branch_staff_females));
        }
        return $staff_detail_mf;
    }
    public function total_class_student($branch_id, $class_id, $gender)
    {
        $sql = "Select count(*) as total from student_pi as sp
JOIN student_ei as se on se.student_id=sp.student_id && se.class_id='" . $class_id . "' && se.branch_id='" . $branch_id . "' WHERE sp.gender='".$gender."'";
        if ($this->db->query($sql)) {
            $query = $this->db->query($sql);
            $result=$query->row_array();
            return $result["total"];
        } else
            return FALSE;
    }

    public function staff_summary()
    {
        $total_branches = $this->m_common->sel_spe_array_counts("branches", array("deletable" => 1, "editable" => 1), "branch_id,branch_name");
        $branches = $this->m_common->sel_spe_array_fields("branches", array("deletable" => 1), "branch_id, branch_name");
        $staff_summary = array();
        $total_staffs = $this->m_common->sel_spe_array_counts("staff_ji", array(), "staff_id");
        foreach ($branches as $branch) {
            $branch_staff = $this->m_common->sel_spe_array_counts("staff_ji", array("branch_id" => $branch["branch_id"]), "staff_id");
            $branch_staff = $branch_staff / $total_staffs * 100; // percentage
            $branch_staff = round($branch_staff, 2);
            array_push($staff_summary, array("title" => $branch["branch_name"], "value" => $branch_staff));

        }
        return $staff_summary;
    }


    /**
     * Gender summary
     */
    public function staff_gender_summary()
    {
        $total_branches = $this->m_common->sel_spe_array_counts("branches", array("deletable" => 1, "editable" => 1), "branch_id,branch_name");
        $branches = $this->m_common->sel_spe_array_fields("branches", array("deletable" => 1), "branch_id, branch_name");
        $gender_staff_summary = array();
        $total_staffs = $this->m_common->sel_spe_array_counts("staff_ji", array(), "staff_id");
        foreach ($branches as $branch) {
            $branch_staff = $this->m_common->sel_spe_array_counts("staff_ji", array("branch_id" => $branch["branch_id"],), "staff_id");
            $branch_staff = $branch_staff / $total_staffs * 100; // percentage
            $branch_staff = round($branch_staff, 2);
            array_push($staff_summary, array("title" => $branch["branch_name"], "value" => $branch_staff));

        }
        return $gender_staff_summary;
    }


}