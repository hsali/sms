<?php

class M_staff extends CI_Model {

    public function __construct() {

    }

    /**
     * get roll no
     * @param $branch_id
     * @return mixed
     */
    public function get_staff_id($branch_id) {
        $this->db->where('branch_id', $branch_id);
        $this->db->from('staff_ji');
        $rn = $this->db->count_all_results();
        return $rn + 1;
    }

    /**
     * insert record and return id
     * @param $table_name
     * @param $data
     */
    public function ins_rec_ri($table_name, $data) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function view_staff() {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT pi.staff_id, ji.b_staff_id, pi.first_name, pi.last_name, pi.cell_no, pi.email,pi.domicile, ji.staff_designation from
  staff_pi as pi
  JOIN staff_ji as ji on ji.staff_id=pi.staff_id && ji.branch_id='" . $branch_id . "' && ji.deletable='1'
WHERE pi.branch_id='" . $branch_id . "' && pi.deletable='1';
";
            $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return FALSE;
    }

}
