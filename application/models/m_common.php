<?php


class M_common extends CI_Model {
    public function __construct(){

    }

    public function get_sum_where($table_name,$field,$where){
        $this->db->select_sum($field);
        $this->db->where($where);
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result[$field];
        }
        else
            return FALSE;
    }
    /**
     * To get the sum of single field.
     * @param $tables_name
     * @param $field
     * @return bool or sum of single field
     */

    public function get_sum($table_name, $field){
        $this->db->select_sum($field);
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result[$field];
        }
        else
            return FALSE;

    }
    /**
     * get single field based upon the conditions
     * @param $table_name
     * @param $where
     * @param $field
     * @return single field return or FALSE
     */

    public function select_signle_field_and_row($table_name, $where, $field)
    {
        $this->db->select($field);
        $this->db->where($where);
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result[$field];
        } else
            return FALSE;
    }

    /**
     * used to select student of specific branch
     * @param $branch_id
     * @return bool or array of staff
     */
    public function select_students($branch_id=""){
        $branch_id=$this->session->userdata("branch_id");
        $sql="SELECT sp.student_id,sp.first_name, sp.last_name
FROM student_pi as sp
JOIN student_ei as se on se.student_id=sp.student_id && se.deletable='1' && se.branch_id='".$branch_id."';";
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }
    /**
     * used to select staff of specific branch
     * @param $branch_id
     * @return bool or array of staff
     */

    public function select_staffs()
    {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT sp.staff_id, sj.b_staff_id, sp.first_name, sp.last_name
FROM staff_pi as sp
JOIN staff_ji as sj on sj.staff_id=sp.staff_id && sj.branch_id='".$branch_id."';";
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }
    /**
     * @param $name
     * @return array or False
     */
    public function select_variable($name)
    {
        $sql = "SELECT vv.var_vid, vv.var_value from var_name as vn
JOIN var_value as vv ON vv.var_id=vn.var_id && vn.var_name='" . $name . "';";
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            return $query->result_array();
        } else
            return FALSE;
    }

    public function select_value_of_variable($var_vid){
        $sql="SELECT var_value
FROM var_value WHERE var_vid='".$var_vid."';";
        $query=$this->db->query($sql);

        if($query->num_rows()>0){
            $result= $query->row_array();
            return $result["var_value"];
        }
        else
            return FALSE;
    }
    public  function is_changeable($table_name, $id, $value){
        $this->db->where($id,$value);
        $this->
        $this->db->select($table_name);
    }
    public function sel_spe_rec_objects($table_name,$id,$value){
        $this->db->where($id, $value);
        $query=$this->db->get($table_name);
        if($query->num_rows>0){
            return $query->result();
        }
        else
            return FALSE;
    }

    /**
     * get object with specific conditions
     * @param $table
     * @param $where
     */
    public function sel_spe_rec_fields_objects($table, $where)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        if ($query->num_rows > 0) {
            return $query->result();
        } else
            return FALSE;
    }
    public function sel_all_rec_objects($table_name){
        $query=$this->db->get($table_name);
        return $query->result();
    }

    /**
     * select all records with specific fields
     * @param $table_name
     * @param $fields
     * @return bool
     */
    public  function sel_all_rec_fields($table_name,$fields){
        $this->db->select($fields);
        $query=$this->db->get($table_name);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }

    /**
     *
     * @return total counts of specific fields for specific values
     */
    public function sel_spe_array_counts($table_name,$where, $fields){
        $this->db->select($fields);
        $this->db->where($where);
        $this->db->from($table_name);
        $query=$this->db->get();
        $total_counts=$query->num_rows();
        return $total_counts;
    }
    /**
     * get the array of data
     * @param $table_name
     * @param $where
     * @param $fields
     * @return bool
     */

    public function sel_spe_array_fields($table_name,$where, $fields){
        $this->db->select($fields);
        $this->db->where($where);
        $this->db->from($table_name);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }

    /**
     * @param $table_name
     * @param $where
     * @param $fields
     * @param $order
     * @return bool or array
     */
    public function sel_spe_array_fields_order($table_name,$where, $fields,$order){
        $this->db->select($fields);
        $this->db->where($where);
        $this->db->from($table_name);
        $this->db->order_by($order,"desc");
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }

    /**
     * fetch single row of table based upon the condition
     * @param $table_name
     * @param $where
     * @return single row or false
     */
    public function sel_spe_row($table_name, $where)
    {
        $this->db->where($where);
        $this->db->from($table_name);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else
            return FALSE;
    }

    /**
     * select specific row with specific fields where condition
     *
     */
    public function sel_spe_row_fields($table_name,$where, $fields){
        $this->db->select($fields);
        $this->db->where($where);
        $this->db->from($table_name);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }
        else
            return FALSE;
    }
    /**
     * select specific row
     * @param $table_name
     * @param $id
     * @param $value
     * @return bool
     */

    public function sel_spe_rec($table_name, $id,$value){
        $this->db->where($id, $value);
        $query=$this->db->get($table_name);
        if($query->num_rows>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }

    /**
     * select all record of the table
     * @param $table_name
     * @return  Array or FALSE
     */

    public  function sel_all_rec($table_name){
        $query=$this->db->get($table_name);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }

    /**
     * insert record
     * @param $table_name
     * @param $data
     * @return mixed
     */
    public  function ins_rec($table_name, $data){
        if($this->db->insert($table_name, $data))
        {
            return TRUE;
        }
        else
            return FALSE;
    }

    /**
     * THIS  UPDATE THE RECORD
     * @param $table_name
     * @param $id
     * @param $value
     * @param $data
     * @return true | false
     */


    public function upd_rec($table_name, $id, $value,$data){
        $this->db->where($id,$value);
        if($this->db->update($table_name, $data)){
            return TRUE;
        }
        else
            return FALSE;
    }

    /**
     * THIS WILL DELETE THE RECORD
     * @param $table_name
     * @param $id
     * @param $value
     * @return TRUE | FALSE
     */

    public function del_rec($table_name, $id, $value){
        // @todo check the value for *. * can delete all the value
        if($this->db->delete($table_name,array($id=>$value))){
            return TRUE;
        }
        return FALSE;
    }

    public  function sel_id($table_name,$id,$value,$field_id){
        $data=$this->sel_spe_row_fields($table_name,array($id=>$value), $field_id);
        return $data[$field_id];

    }

    public function set_var($lang="English",$page=""){
        $lang_id=$this->sel_id("tbl_languages","lang_name",$lang,"lang_id");
//        $si=$this->sel_id("tbl_languages_description","ld_string_identifier","cu_title","ld_id");
//        $cu_title=$this->sel_spe_row_fields("tbl_lang_trans",array("lang_id"=>$lang_id,"ld_id"=>$si),"lt_trans");
//        $ind=$this->sel_spe_row_fields("tbl_languages_description",array("ld_id"=>5),
//            "ld_string_identifier");

        $rec=$this->sel_spe_rec("tbl_lang_trans","lang_id",$lang_id);
//        $ind=$this->sel_spe_row_fields("tbl_languages_description",array("ld_id"=>5),
//            "ld_string_identifier");
//        var_dump($ind);
//        echo br(2);
//        echo $ind["ld_string_identifier"];
        //var_dump($rec);
        if(isset($rec) && is_array($rec)) {

            foreach ($rec as $row) {
                $ind=$this->sel_spe_row_fields("tbl_languages_description",array("ld_id"=>$row["ld_id"]),
                    "ld_string_identifier");
                $this->config->set_item($ind["ld_string_identifier"], $row["lt_trans"]);
            }
        }
    }

}