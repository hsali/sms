<?php

class M_salary extends CI_Model
{

    public function __construct()
    {

    }


    public function get_total_deductions_of_staff($staff_id)
    {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT ABS(sum(salary_type_value)) as deductions from salary_setting WHERE branch_id='" . $branch_id . "' && deletable='1' && single_group='1' && staff_and_designations='" . $staff_id . "' && salary_type_value <0 ;";
        $query = $this->db->query($sql);

        $result = $query->row_array();
        if ($result["deductions"] > 0)
            return $result["deductions"];

        else
            return 0;
    }

    public function get_total_allowances_of_staff($staff_id)
    {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT ABS(sum(salary_type_value)) as allowances from salary_setting WHERE branch_id='" . $branch_id . "' && deletable='1' && single_group='1' && staff_and_designations='" . $staff_id . "' && salary_type_value >0 ;";
        $query = $this->db->query($sql);

        $result = $query->row_array();
        if ($result["allowances"] > 0)
            return $result["allowances"];
        else
            return 0;
    }

    public function get_total_deductions_of_designation($designation)
    {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT ABS(sum(salary_type_value)) as deductions from salary_setting WHERE branch_id='" . $branch_id . "' && deletable='1' && single_group='2' && staff_and_designations='" . $designation . "' && salary_type_value <0 ;";
        $query = $this->db->query($sql);

        $result = $query->row_array();
        if ($result["deductions"] > 0)
            return $result["deductions"];

        else
            return 0;
    }

    public function get_total_allowances_of_designation($designation)
    {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT ABS(sum(salary_type_value)) as allowances from salary_setting WHERE branch_id='" . $branch_id . "' && deletable='1' && single_group='2' && staff_and_designations='" . $designation . "' && salary_type_value >0 ;";
        $query = $this->db->query($sql);

        $result = $query->row_array();
        if ($result["allowances"] > 0)
            return $result["allowances"];
        else
            return 0;
    }

    public function select_all_salaries()
    {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT s.salary_id, s.staff_id,concat(pi.first_name,' ',pi.last_name) as name, MONTHNAME(STR_TO_DATE(s.month,'%m')) as month ,s.year,s.total_presents,s.total_absents,s.total_leaves,total_late_presents, s.payable,s.paid, s.allowances, s.deductions from salary as s
JOIN  staff_pi as pi on pi.staff_id=s.staff_id && pi.branch_id='" . $branch_id . "' && pi.deletable='1'
WHERE s.branch_id='" . $branch_id . "' && s.deletable='1' ;";
        $query = $this->db->query($sql);
        return $query->result_array();

    }

    /**
     * get the salaries of specific staff
     * @param $staff_id
     * @return mixed
     */
    public function select_all_salaries_of_staff($staff_id)
    {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT s.salary_id,s.arrears_and_advance, MONTHNAME(STR_TO_DATE(s.month,'%m')) as month ,s.year,s.total_presents,s.total_absents,s.total_leaves,total_late_presents, s.payable,s.paid, s.allowances, s.deductions from salary as s
JOIN  staff_pi as pi on pi.staff_id=s.staff_id && pi.branch_id='" . $branch_id . "' && pi.deletable='1'
WHERE s.branch_id='" . $branch_id . "' && s.deletable='1' && s.staff_id='" . $staff_id . "' ;";
        $query = $this->db->query($sql);
        return $query->result_array();

    }

    public function is_staff_present($staff_id)
    {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT count(*) as rows FROM salary_setting WHERE branch_id='" . $branch_id . "' && deletable='1' && single_group='1' && staff_and_designations='" . $staff_id . "';";
        $query = $this->db->query($sql);

        $result = $query->row_array();
        if ($result["rows"] > 0) {
            return TRUE;
        } else
            return FALSE;
    }

    public function total_attendance_select($status, $month, $year, $staff_id)
    {
        $branch_id = $this->session->userdata("branch_id");
        $sql = "SELECT count(*) as total from staff_attendance WHERE  YEAR(attendance_time)='" . $year . "' AND MONTH(attendance_time)='" . $month . "' && attendance_status='" . $status . "' && branch_id='" . $branch_id . "' && deletable='1' && staff_id='" . $staff_id . "';";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        if ($result["total"])
            return $result["total"];
        else
            return 0;
    }

    public function select_signle_field_and_row($table_name, $where, $field)
    {
        $this->db->select($field);
        $this->db->where($where);
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result[$field];
        } else
            return FALSE;
    }

}
