<?php

class M_settings extends CI_Model {

    public function __construct() {

    }

    /**
     * get roll no
     * @param $branch_id
     * @return mixed
     */
    public function get_staff_id($branch_id) {
        $this->db->where('branch_id', $branch_id);
        $this->db->from('staff_ji');
        $rn = $this->db->count_all_results();
        return $rn + 1;
    }

    /**
     * insert record and return id
     * @param $table_name
     * @param $data
     */
    public function ins_rec_ri($table_name, $data) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function view_sections() {
        $branch_id=$this->session->userdata("branch_id");
       $sql="SELECT s.section_id,c.class_name, s.section_name, s.created_date_time, s.editable  from sections as s
JOIN classes as c on c.class_id=s.class_id && s.branch_id='".$branch_id."'";
    $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }
    public function view_staff_salary_settings(){
        $branch_id=$this->session->userdata("branch_id");
        $sql="SELECT ss.ss_id,ss.staff_and_designations as staff_id,concat(sp.first_name,' ',sp.last_name) as name,vv.var_value, ss.salary_type_value, ss.editable from salary_setting as ss
JOIN var_value as vv on vv.var_vid=ss.salary_type
JOIN staff_pi as sp on sp.staff_id= ss.staff_and_designations
WHERE ss.branch_id='".$branch_id."' && ss.single_group='1' && ss.deletable='1';";
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }
    public function view_group_salary_settings(){
        $branch_id=$this->session->userdata("branch_id");
        $sql="SELECT ss.ss_id,ss.staff_and_designations ,vv.var_value, ss.salary_type_value, ss.editable from salary_setting as ss
JOIN var_value as vv on vv.var_vid=ss.salary_type
WHERE ss.branch_id='".$branch_id."' && ss.single_group='2' && ss.deletable='1';";
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }

}
