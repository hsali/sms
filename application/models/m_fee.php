<?php

class M_fee extends CI_Model {

    public function __construct() {

    }

    /**
     * get fees with specific fee ids
     * @param $ids
     * @return bool or array of received fees
     */
    public function get_received_fee_ids($ids){
        $sql="SELECT rf.rf_id,vv.var_value as fee_category ,rf.fee,rf.period FROM received_fee as rf
  JOIN var_value as vv on vv.var_vid=rf.fc_id
where rf.rf_id IN (".$ids.") && rf.deletable='1';";
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }
    public function view_receipt($receipt_id){
        $branch_id=$this->session->userdata("branch_id");
        $sql="SELECT fr.receipt_id, fr.student_id, sp.first_name, sp.last_name,c.class_name, fr.received_payment,fr.received_payment_date,fr.received_fee_ids from fee_receipts as fr
JOIN student_pi as sp on sp.student_id=fr.student_id
JOIN student_ei as se on se.student_id=sp.student_id && se.branch_id='".$branch_id."'
JOIN  classes as c on c.class_id= se.class_id && fr.deletable='1' && fr.receipt_id='".$receipt_id."';";
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->row_array();
        }
        else
            return FALSE;
    }

    /**
     * Get all receipts of fee
     * @param $branch_id
     * @return bool or Array
     */
    public function all_receipts($branch_id=""){
        $branch_id=$this->session->userdata("branch_id");
        $sql="SELECT fr.receipt_id, fr.student_id, sp.first_name, sp.last_name,c.class_name, fr.received_payment,fr.received_payment_date,fr.received_fee_ids from fee_receipts as fr
JOIN student_pi as sp on sp.student_id=fr.student_id
JOIN student_ei as se on se.student_id=sp.student_id && se.branch_id='".$branch_id."'
  JOIN  classes as c on c.class_id= se.class_id && fr.deletable='1';";
    $query=$this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else
            return FALSE;
    }
    /**
     * get roll no
     * @param $branch_id
     * @return mixed
     */
    public function get_roll_no($branch_id) {
        $this->db->where('branch_id', $branch_id);
        $this->db->from('student_ei');
        $rn = $this->db->count_all_results();
        return $rn + 1;
    }

    /**
     * insert record and return id
     * @param $table_name
     * @param $data
     */
    public function ins_rec_ri($table_name, $data) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function view_program_fee() {
        $branch_id = $this->session->userdata("branch_id");
            $sql = "SELECT spf.pf_id, vv.var_value, c.class_name,spf.period, spf.fee, spf.editable from set_prog_fee as spf
JOIN classes as c on c.class_id=spf.class_id && c.branch_id='".$branch_id."'
join var_value as vv on vv.var_vid=spf.fc_id
WHERE spf.branch_id='".$branch_id."';
         ";
            $query = $this->db->query($sql);
            if($query->num_rows()>0)
            return $query->result_array();
        else
            return FALSE;

    }

}
