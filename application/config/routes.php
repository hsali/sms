<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "index";
$route['upload/(:any)/(:any)'] = "admin/c_upload/index/$1/$2";
$route['upload/image'] = "admin/c_upload/upload/";


$route['admin'] = "admin/index";
$route['home'] = "index";

$route['super/users'] = "super/c_users/index";
$route['super/user/add'] = "super/c_users/add_user";
$route['super/user/edit/(:num)'] = "super/c_users/edit_user/$1";
$route['super/user/profile/(:num)'] = "super/c_users/view_profile/$1";
$route['super/user/delete/(:num)'] = "super/c_users/delete_user/$1";

$route['admin/branches/add'] = "admin/c_settings/add_branch";
$route['admin/branches'] = "admin/c_settings/view_branches";
$route['admin/branches/edit/(:num)'] = "admin/c_settings/edit_branch/$1";
$route['admin/branches/view/(:num)'] = "admin/c_settings/view_branch/$1";
$route['admin/branches/del/(:num)'] = "admin/c_settings/del_branch/$1";

$route['admin/classes/add'] = "admin/c_settings/add_class";
$route['admin/classes'] = "admin/c_settings/view_classes";
$route['admin/classes/edit/(:num)'] = "admin/c_settings/edit_class/$1";
$route['admin/classes/view/(:num)'] = "admin/c_settings/view_class/$1";
$route['admin/classes/del/(:num)'] = "admin/c_settings/del_class/$1";

$route['admin/sections/add'] = "admin/c_settings/add_section";
$route['admin/sections'] = "admin/c_settings/view_sections";
$route['admin/sections/edit/(:num)'] = "admin/c_settings/edit_section/$1";
$route['admin/sections/view/(:num)'] = "admin/c_settings/view_section/$1";
$route['admin/sections/del/(:num)'] = "admin/c_settings/del_section/$1";

// student management
$route['admin/students/add'] = "admin/c_student/add_student";
$route['admin/students'] = "admin/c_student/view_students";
$route['admin/students/edit/(:num)'] = "admin/c_student/edit_student/$1";
$route['admin/students/view/(:num)'] = "admin/c_student/view_student/$1";
$route['admin/students/del/(:num)'] = "admin/c_student/del_student/$1";
$route['admin/students/print/(:num)'] = "admin/c_student/print_form/$1";

// fee settings
$route['admin/settings'] = "admin/c_set_fee/index";
  // variable naming
    $route['admin/settings/variables'] = "admin/c_settings/view_all_variables";
    $route['admin/settings/variables/add'] = "admin/c_settings/add_variable";
    $route['admin/settings/variables/edit/(:num)'] = "admin/c_settings/edit_variable/$1";
    $route['admin/settings/variables/del/(:num)'] = "admin/c_settings/del_variable/$1";
    $route['admin/settings/variables/delete/(:num)'] = "admin/c_settings/delete_variable/$1";
    $route['admin/settings/variables/view/(:num)'] = "admin/c_settings/view_variable/$1";
// variable values

    $route['admin/settings/variable/values/add'] = "admin/c_settings/add_value";
    $route['admin/settings/variable/values/add/(:num)'] = "admin/c_settings/add_spe_value/$1";
    $route['admin/settings/variable/values/edit/(:num)'] = "admin/c_settings/edit_value/$1";
    $route['admin/settings/variable/values/del/(:num)'] = "admin/c_settings/del_value/$1";
    $route['admin/settings/variable/values/delete/(:num)'] = "admin/c_settings/delete_value/$1";
    $route['admin/settings/variable/values/view/(:num)'] = "admin/c_settings/view_value/$1";

    // fee category
    $route['admin/settings/fee/cat'] = "admin/c_set_fee/view_all_fee_cat";
    $route['admin/settings/fee/cat/add'] = "admin/c_set_fee/add_fee_cat";
    $route['admin/settings/fee/cat/edit/(:num)'] = "admin/c_set_fee/edit_fee_cat/$1";
    $route['admin/settings/fee/cat/del/(:num)'] = "admin/c_set_fee/del_fee_cat/$1";
    $route['admin/settings/fee/cat/view/(:num)'] = "admin/c_set_fee/view_fee_cat/$1";
    // program fee
    $route['admin/settings/program/fee'] = "admin/c_set_fee/view_all_program_fee";
    $route['admin/settings/program/fee/add'] = "admin/c_set_fee/add_program_fee";
    $route['admin/settings/program/fee/edit/(:num)'] = "admin/c_set_fee/edit_program_fee/$1";
    $route['admin/settings/program/fee/delete/(:num)'] = "admin/c_set_fee/delete_program_fee/$1";
    $route['admin/settings/program/fee/del/(:num)'] = "admin/c_set_fee/del_program_fee/$1";
    $route['admin/settings/program/fee/view/(:num)'] = "admin/c_set_fee/view_program_fee/$1";



// fee management
$route['admin/fee/admission/add'] = "admin/c_fee/add_admission_fee";
$route['admin/fee/receipts'] = "admin/c_fee/view_all_receipts";
$route['admin/fee/receipt/view/(:num)'] = "admin/c_fee/receipt_view/$1";


// Salary Setting Management
$route['admin/settings/salary/staff/add'] = "admin/c_settings/add_staff_salary_setting";
$route['admin/settings/salary/group/add'] = "admin/c_settings/add_group_salary_setting";
$route['admin/settings/salary/staff'] = "admin/c_settings/staff_salary_setting";
$route['admin/settings/salary/group'] = "admin/c_settings/group_salary_setting";
$route['admin/settings/salary/group/edit/(:num)'] = "admin/c_settings/edit_group_salary_setting/$1";
$route['admin/settings/salary/staff/edit/(:num)'] = "admin/c_settings/edit_staff_salary_setting/$1";
$route['admin/settings/salary/staff/delete/(:num)'] = "admin/c_settings/delete_staff_salary_setting/$1";
$route['admin/settings/salary/group/delete/(:num)'] = "admin/c_settings/delete_group_salary_setting/$1";
$route['admin/fee/receipt/view/(:num)'] = "admin/c_fee/receipt_view/$1";

// salary management
$route['admin/salary/select'] = "admin/c_salary/select_input";
$route['admin/salary/edit/(:num)'] = "admin/c_salary/edit_salary/$1";
$route['admin/salary/all'] = "admin/c_salary/view_all_salaries";
$route['admin/salary/staff/view/(:num)'] = "admin/c_salary/view_saff_salaries/$1";


// Result management
$route['admin/result/add'] = "admin/c_result/add_single";
$route['admin/result/student/(:num)'] = "admin/c_result/student_result/$1";
$route['admin/result/all'] = "admin/c_result/view_all_results";
$route['admin/result/select'] = "admin/c_result/select_input";
$route['admin/fee/receipt/view/(:num)'] = "admin/c_fee/receipt_view/$1";

// student management 
$route['admin/students/edit/(:num)'] = "admin/c_student/edit_student/$1";
$route['admin/students/view/(:num)'] = "admin/c_student/view_student/$1";
$route['admin/students/del/(:num)'] = "admin/c_student/del_student/$1";
$route['admin/students/print/(:num)'] = "admin/c_student/print_form/$1";

//Staff management 
$route['admin/staff'] = "admin/c_staff/view_staffs";
$route['admin/staff/add'] = "admin/c_staff/add_staff";
$route['admin/staff/edit/(:num)'] = "admin/c_staff/edit_staff/$1";
$route['admin/staff/view/(:num)'] = "admin/c_staff/view_staff/$1";
$route['admin/staff/del/(:num)'] = "admin/c_staff/del_staff/$1";
$route['admin/staff/print/(:num)'] = "admin/c_staff/print_form/$1";

/**
 * Student Attendance Management
 */
$route['admin/attendance/section/add'] = "admin/c_std_attendance/add_section_attendance";
$route['admin/attendance/section/edit/(:num)'] = "admin/c_std_attendance/edit_section_attendance/$1";
$route['admin/attendance/sections'] = "admin/c_std_attendance/view_section_attendance";
$route['admin/attendance/section/view/(:num)'] = "admin/c_std_attendance/view_single_section_attendance/$1";
$route['admin/attendance/section/del/(:num)'] = "admin/c_std_attendance/del_section_attendance/$1";

/**
 * staff attendance
 */

$route['admin/attendance/staff/add'] = "admin/c_staff_attendance/add";
$route['admin/attendance/staff/edit/(:num)'] = "admin/c_staff_attendance/edit/$1";
$route['admin/attendance/staff/view/(:num)'] = "admin/c_staff_attendance/view/$1";
$route['admin/attendance/staff/del/(:num)'] = "admin/c_staff_attendance/del/$1";
$route['admin/attendance/staff/summary/(:num)'] = "admin/c_staff_attendance/staff_summary/$1";
$route['admin/attendance/staff'] = "admin/c_staff_attendance/attendance_of_all_staff";
$route['admin/attendance/sections'] = "admin/c_std_attendance/view_section_attendance";
$route['admin/attendance/section/view/(:num)'] = "admin/c_std_attendance/view_single_section_attendance/$1";
$route['admin/attendance/section/del/(:num)'] = "admin/c_std_attendance/del_section_attendance/$1";


$route['login'] = "auth/login";
$route['signup'] = "auth/signup";
$route['logout'] = "auth/logout";

$route['404_override'] = '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */