<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_users extends CI_Controller {


    public function __construct(){
        parent::__construct(); // if it is not called. then below model will not work.
        $this->m_functions->login_control();
        if($this->session->userdata("user_type")!="1")
             redirect("home");
        $this->load->model("m_common","cquery");
        $this->load->model("m_functions","cfun");
    }

    /**
     * @param mixed $data
     * default function of this class
     */
    public function index($data="")
    {
        $data["widget_title"]="Users Management";
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("home")
            ),
            array(
                "name"=>"Users Management",
                "url"=>site_url("super/users")
            )
        );

        $data["view_users"]=$this->cquery->sel_spe_array_fields("users",array("deletable"=>"1"),"*");

        $this->load->view('admin/users/index',$data);
    }



    public function view_profile($id){
      $data["widget_title"]="Users Management";
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("home")
            ),
            array(
                "name"=>"Users Management",
                "url"=>site_url("super/users")
            )
        );
        $editable=$this->cquery->select_signle_field_and_row("users",array("user_id"=>$id),"editable");
        $deletable=$this->cquery->select_signle_field_and_row("users",array("user_id"=>$id),"deletable");
        if($deletable=="1"){
        $data["edit_user"] = $this->cquery->sel_spe_array_fields("users", array("user_id"=> $id,"deletable"=>"1"),"*");
        $this->load->view("admin/users/profile",$data);}
        elseif($deletable=="0"){
            $data["message"] = $this->cfun->code_sending(100, "Invalid Access. <a href=\"".site_url("super/users")."\">View Users</a> ");
            $this->index($data);
        }
    }

    /**
     * add question in the db. page will be loaded
     */
    public function add_user(){
        $data["widget_title"]="User Registration Form";
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("home")
            ),
            array(
                "name"=>"Users Management",
                "url"=>site_url("super/users")
            )
        );
        $data["a_branches"]=$this->cquery->sel_all_rec_fields("branches","branch_id,branch_name");

        if (isset($_POST["submit"])) {
            $data['branch_id'] 				= trim($this->input->post('branch_id'));
            $data['user_name']  	= trim($this->input->post('user_name'));
            $data['first_name'] 	= trim($this->input->post('first_name'));
            $data['last_name'] 		= trim($this->input->post('last_name'));
            $data['password'] 	  = trim($this->input->post('password'));
            $data['re_password'] 	  = trim($this->input->post('re_password'));
            $data['email'] 			= trim($this->input->post('email'));
            $data['about_me'] 			= trim($this->input->post('abouDt_me'));

            $this->form_validation->set_rules("user_name", "User Name", "required|is_unique[users.user_name]");
            $this->form_validation->set_rules("email", "User Email", "valid_email");
            $this->form_validation->set_rules("password", "User Password", "required|matches[re_password]");
            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    'branch_id'     		=>  $data["branch_id"],
                    "first_name" => $data["first_name"],
                    "last_name" => $data["last_name"],
                    "user_name" => $data["user_name"],
                    "email" => $data["email"],
                    "password" => $data["password"],
                    "about_me" => $data["about_me"],
                    "user_type"=> "2",
                    "status" => "1",
                    "created_date_time"=> date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("users", $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"User \"".$data["user_name"]."\" Added
                successfully");

            }
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate User \"".$data["user_name"]."\" ");
            }


        }
        $this->load->view("admin/users/add", $data);

    }




    public function delete_user($id)
    {
        $editable=$this->cquery->select_signle_field_and_row("users",array("user_id"=>$id),"editable");
        if($editable=="1") {
            if ($this->cquery->del_rec("users", "user_id", $id))
                $data["message"] = $this->cfun->code_sending(200, "User having user id <b> " . $id . "</b> has been deleted
            successfully");
            $this->index($data);
        } elseif($editable=="0"){
            $data["message"] = $this->cfun->code_sending(100, "This User having user id <b> " . $id . "</b> cannot be <strong>deleted</strong>");
            $this->index($data);
        }
    }


    public function edit_user($id)
    {
        $data["widget_title"] = "User Registration Form";
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("home")
            ),
            array(
                "name" => "Users Management",
                "url" => site_url("super/users")
            )
        );
        $editable = $this->cquery->select_signle_field_and_row("users", array("user_id" => $id), "editable");
        if ($editable == "1") {
        $data["a_branches"] = $this->cquery->sel_all_rec_fields("branches", "branch_id,branch_name");
        $dd = $this->cquery->sel_spe_row_fields("users", array("user_id" => $id), "user_name");
        $data["cr_user_name"] = $dd["user_name"];
        $data["user_pic_id"] = $id;

        if (isset($_POST["update_profile"])) {

            $data['branch_id'] = trim($this->input->post('branch_id'));
            $data['first_name'] = trim($this->input->post('first_name'));
            $data['last_name'] = trim($this->input->post('last_name'));
            $data['email'] = trim($this->input->post('email'));
            $data['about_me'] = trim($this->input->post('about_me'));
            $this->form_validation->set_rules("email", "User Email", "valid_email");

            if ($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    'branch_id' => $data["branch_id"],
                    "email" => $data["email"],
                    "first_name" => $data['first_name'],
                    "last_name" => $data['last_name'],
                    "about_me" => $data['about_me']
                );
                $this->cquery->upd_rec("users", "user_id", $id, $ins_data);
                $data["message"] = $this->cfun->code_sending(200, "User \"" . $data["cr_user_name"] . "\" Updated
                successfully");
            } // end of form validation

        }
        elseif (isset($_POST["upload_picture"])) {
            $data["edit_user"] = $this->cquery->sel_spe_rec("users", "user_id", $id);

            $ar = array(
                "field_name" => "profile_picture",
                "file_name" => "user_id_" . $id,
                "path_to_file" => "users/"
            );
            $dd = $this->m_functions->upload($ar);
            if ($dd ["condition"] == "1") {
                $data ["message"] = $this->cfun->code_sending(100, "Uploading Picture Failed ! " . $dd ["msg"]);
            } elseif ($dd ["condition"] == "2") {
                $data ["message"] = $this->cfun->code_sending(200, "Picture Uploaded Successfully  ");
            }
        }
        elseif (isset($_POST["change_password"])) {

            $data["edit_user"] = $this->cquery->sel_spe_rec("users", "user_id", $id);
            $data['current_password'] = trim($this->input->post('current_password'));
            $data['password'] = trim($this->input->post('password'));
            $data['re_password'] = trim($this->input->post('re_password'));

            $arr = array(
                "password" => $data['current_password'],
                "user_id" => $id,
            );
            $dd = $this->cquery->sel_spe_row_fields("users", $arr, "user_name");
            if ($dd["user_name"] == $data["cr_user_name"]) {

                $this->form_validation->set_rules("password", "Password", "required|matches[re_password]");
                if ($this->form_validation->run() === TRUE) {
                    $ins_data = array(
                        "password" => $data["password"],
                    );
                    $this->cquery->upd_rec("users", "user_id", $id, $ins_data);
                    $data["message"] = $this->cfun->code_sending(200, "Password for User  \"" . $data["cr_user_name"] . "\"
                    has changed successfully");
                } // end of form validation
                else {
                    $data["message"] = $this->cfun->code_sending(100, "Validation Security form ");
                }

            } else {
                $this->form_validation->set_message('current_password', 'The %s field contained wrong password"');
                $data["message"] = $this->cfun->code_sending(100, "Wrong password ");
            }


        }
        else {
            $data["edit_user"] = $this->cquery->sel_spe_rec("users", "user_id", $id);


        }
        $this->load->view("admin/users/edit", $data);
    }elseif($editable=="0"){
            $data["message"] = $this->cfun->code_sending(100, "Invalid Access");
            $this->index($data);
        }

    }



}
