<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index()
    {
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            )
        );
//        echo "hello";
        $this->load->view("testing/index",$data);
    }
    public function pic(){
        $img = $_POST['img'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/images/some_name.png';
        file_put_contents($file, $data);
    }

}

