<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if($this->session->userdata("user_type")=="1")
            redirect(site_url("super"));
        if($this->session->userdata("user_type")=="2")
            redirect(site_url("admin"));
        $this->m_functions->login_control();
        $this->load->model("home_summary");
    }

    public function index()
    {
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            )
        );
        $data["student_summary"]=$this->home_summary->student_summary();
        $data["staff_summary"]=$this->home_summary->staff_summary();
        $data["gender_staff_summary"]=array(
            array(
                "title"=>"Male",
                "value"=>"40"
                ),
            array(
                "title"=>"Female",
                "value"=>"60"
            )
        );
        $data["gender_student_summary"]=array(
            array(
                "title"=>"Male",
                "value"=>"60"
            ),
            array(
                "title"=>"Female",
                "value"=>"40"
            )
        );
        $data["student_detail"]=array(
            array(
                "id"=>"1",
                "names"=>"All Branches",
                "males"=>"40",
                "females"=>"30"
            ),
            array(
                "id"=>"2",
                "names"=>"Branch A",
                "males"=>"15",
                "females"=>"20"
            ),
            array(
                "id"=>"3",
                "names"=>"Class 4",
                "males"=>"20",
                "females"=>"12"
            ),
            array(
                "id"=>"4",
                "names"=>"Class 9",
                "males"=>"2",
                "females"=>"3"
            ),
            array(
                "id"=>"5",
                "names"=>"Class 10",
                "males"=>"5",
                "females"=>"4"
            ),
        );
        $this->load->view("super/index",$data);

    }
}

