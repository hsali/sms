<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

 public  function  __construct(){
     parent::__construct();
 }

    public function index()
    {

    }

    public  function login(){
        $data["message"]="";
        if(isset($_POST["submit"])) {
            $data["user_name"] = $this->input->post("user_name");
            $data["user_password"] = $this->input->post("user_password");

            $this->form_validation->set_rules("user_name", "User Name", "required");
            $this->form_validation->set_rules("user_password", "Password", "required");
            if ($this->form_validation->run() === TRUE) {
                $cd = array(
                    "user_name" => $data["user_name"],
                    "password" => $data["user_password"],
                    "status" => "1"
                );
                $d = $this->m_common->sel_spe_row_fields("users", $cd, "user_id,user_name,first_name,last_name, branch_id, user_type");


                if ($d) {
                    $this->session->set_userdata("user_name", $data["user_name"]);
                    $this->session->set_userdata("user_id", $d["user_id"]);
                    $this->session->set_userdata("first_name", $d["first_name"]);
                    $this->session->set_userdata("last_name", $d["last_name"]);
                    $this->session->set_userdata("branch_id", $d["branch_id"]);
                    $this->session->set_userdata("user_type", $d["user_type"]);
                    $this->session->set_userdata("login", TRUE);
                    redirect($this->session->userdata("pre_url"));
                } else {
                    $this->m_functions->code_sending("100", "Enter Valid User Name and Password");
                }
            } else {
                if ($this->session->userdata('login') == TRUE) {
                    redirect($this->session->userdata("pre_url"));
                }
            }
        }
        $this->load->view("admin/login", $data);

    }



        public function logout(){
            $this->session->unset_userdata("user_id", "");
            $this->session->unset_userdata("user_name", "");
            $this->session->unset_userdata("first_name", "");
            $this->session->unset_userdata("last_name", "");
            $this->session->unset_userdata("branch_id", "");
            $this->session->unset_userdata("user_type", "");
            $this->session->unset_userdata("login", FALSE);

            $this->session->sess_destroy();
            redirect(site_url("login"));
        }




    public  function  signup(){
        $this->load->view('admin/signup');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */