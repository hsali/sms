<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->m_functions->login_control();
        $this->load->model("super/home");
    }

    public function index()
    {
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            )
        );

        $data["student_summary"]=$this->home->student_summary();
        $data["staff_summary"]=$this->home->staff_summary();
        $data["summary"]=$this->home->summary();
        $data["staff_males_age"]=round($data["summary"]["total_staff_males"]/$data["summary"]["total_staffs"] *100,2);
        $data["staff_females_age"]=round($data["summary"]["total_staff_females"]/$data["summary"]["total_staffs"] *100,2);            $data["student_males_age"]=round($data["summary"]["total_student_males"]/$data["summary"]["total_students"] *100,2);
        $data["student_females_age"]=round($data["summary"]["total_student_females"]/$data["summary"]["total_students"] *100,2);
        if($data["summary"]["total_fee"]+$data["summary"]["total_salary"]){
            $data["total_fee_age"]=round($data["summary"]["total_fee"]/($data["summary"]["total_fee"]+$data["summary"]["total_salary"]) *100,1);
            $data["total_salary_age"]=round($data["summary"]["total_salary"]/($data["summary"]["total_fee"]+$data["summary"]["total_salary"]) *100,1);
        }
        else{

            $data["total_fee_age"]=50;
            $data["total_salary_age"]=50;
        }

        $data["gender_staff_summary"]=array(
            array(
                "title"=>"Male",
                "value"=>$data["staff_males_age"]
            ),
            array(
                "title"=>"Female",
                "value"=>$data["staff_females_age"]
            )
        );
        $data["gender_student_summary"]=array(
            array(
                "title"=>"Male",
                "value"=>$data["student_males_age"]
            ),
            array(
                "title"=>"Female",
                "value"=>$data["student_females_age"]
            )
        );

        $data["fee_and_salary_summary"]=array(
            array(
                "title"=>"Fee",
                "value"=>$data["total_fee_age"]
            ),
            array(
                "title"=>"Salary",
                "value"=>$data["total_salary_age"]
            )
        );
        $data["student_detail"]=$this->home->student_detail();
        $data["staff_detail"]=$this->home->staff_detail();


        $this->load->view("super/index",$data);

    }
}

