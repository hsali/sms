<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_result extends CI_Controller
{


    public function __construct()
    {
        parent::__construct(); // if it is not called. then below model will not work.
        $this->m_functions->login_control();
        $this->load->model("m_result");

    }

    public function select_input(){

        $data["widget_title"]="Result Management";

        $data["a_students"] = $this->m_common->select_students();
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Result Management",
                "url" => site_url("admin/result/all")
            )
        );
        $data["student_id"] = trim($this->input->post("student_id"));
        $data["from_date"] = trim($this->input->post("from_date"));
        $data["to_date"] = trim($this->input->post("to_date"));
        $data["result_category"] = trim($this->input->post("result_category"));
        $data["subject_id"] = trim($this->input->post("subject_id"));
        $data["class_id"] = trim($this->input->post("class_id"));
        
        if (isset($_POST["submit"])) {

            $data["student_id"]=empty($data["student_id"])? ">'0'" : "='".$data["student_id"]."'";
            $data["subject_id"]=empty($data["subject_id"])? ">'0'" : "='".$data["subject_id"]."'";
            $data["result_category"]=empty($data["result_category"])? ">'0'" : "='".$data["result_category"]."'";
            $data["class_id"]=empty($data["class_id"])? ">'0'" : "='".$data["class_id"]."'";
            $data["from_date"]=empty($data["from_date"])? '0-0-0': $data["from_date"] ;
            $data["to_date"]=empty($data["to_date"])? date('Y-m-d'): $data["to_date"] ;

            $data["view_data"]=$this->m_result->specific_result($data["student_id"],$data["result_category"],$data["subject_id"],$data["class_id"],$data["from_date"],$data["to_date"]);

//            var_dump($data);exit;
            $this->load->view('admin/results/all_results', $data);
        }else {
            $this->load->view('admin/results/select_input', $data);
        }
    }
    /**
     * @param mixed $data
     * default function of this class
     */
    public function view_all_results($data = "")
    {
        $data["widget_title"]="Result Management";
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Result Management",
                "url" => site_url("admin/result/all")
            )
        );

        $data["view_data"] = $this->m_result->view_all_results();
        $this->load->view('admin/results/all_results', $data);
    }


    public function print_form($id = "1")
    {

        $this->load->view("admin/student_admission/print");
    }

    public function student_result($id="")
    {
        $data["widget_title"]="Result Management";
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Fee Receipts Management",
                "url" => site_url("admin/fee/receipts")
            )
        );

        $data["view_data"] =$this->m_fee->view_receipt($id);
        $data["received_fee"]=$this->m_fee->get_received_fee_ids($data["view_data"]["received_fee_ids"]);


        $this->load->view("admin/fee/receipt_view", $data);
    }


    public function add_single()
    {
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Fee",
                "url" => site_url("admin/fee/receipts")
            )
        );
        //initialize
        $data["branch_id"] = $this->session->userdata("branch_id");
        $data["a_students"] = $this->m_common->select_students($data["branch_id"]);
        if (isset($_POST["submit"])) {
            $data["student_id"] = trim($this->input->post("student_id"));
            $data["exam_date"] = trim($this->input->post("exam_date"));
            $data["result_category"] = trim($this->input->post("result_category"));
            $data["subject_id"] = count($this->input->post('subject_id')) ? $this->input->post('subject_id') : array();
            $data["obtained_marks"] = count($this->input->post('obtained_marks')) ? $this->input->post('obtained_marks') : array();
            $data["total_marks"] = count($this->input->post('total_marks')) ? $this->input->post('total_marks') : array();
            $data["input_data"] = array();
            foreach ($data["subject_id"] as $rows) {
                array_push($data["input_data"], array("subject_id" => $rows, "obtained_marks" => "", "total_marks" => ""));
            }
            foreach ($data["obtained_marks"] as $key => $value) {
                    $data["input_data"][$key]["obtained_marks"] = $value;
            }
            foreach ($data["total_marks"] as $key => $value) {
                /*if(empty($value)){
                    $data["fc_id"]=$data["input_data"][$key]["fee_category"];
                    $data["fetch_class_id"]=$this->m_common->sel_spe_row_fields("student_ei",array("student_id"=>$data["student_id"],"deletable"=>"1"),"class_id");
                    $data["class_id"]=$data["fetch_class_id"]["class_id"];
                    $data["fetch_prog"]=$this->m_common->sel_spe_row_fields("set_prog_fee",array("class_id"=>$data["class_id"],"branch_id"=>$data["branch_id"],"fc_id"=>$data["fc_id"]),"period");

                    $data["input_data"][$key]["period"] = $data["fetch_prog"]["period"];
                }
                else*/
                    $data["input_data"][$key]["total_marks"] = $value;
            }

            $this->form_validation->set_rules("student_id", "Student ID", "required");

            if ($this->form_validation->run() === TRUE) {


                foreach ($data["input_data"] as $rows) {


                    $ins_data = array(
                        'student_id' => $data["student_id"],
                        'exam_date' => $data["exam_date"],
                        'result_cat_id' => $data["result_category"],
                        'subject_id' => $rows["subject_id"],
                        'obtained_marks' => $rows["obtained_marks"],
                        'total_marks' => $rows["total_marks"],
                        'created_date_time' => date('Y-m-d H:i:s')
                    );

                    $this->m_common->ins_rec("result", $ins_data);

                }
                $data["message"] = $this->m_functions->code_sending(200, "Result has added");
                $this->view_all_results();


            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }


        } else{

            $this->load->view("admin/results/add_single", $data);
        }

    }


}
