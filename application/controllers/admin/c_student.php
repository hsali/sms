<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class C_student extends CI_Controller {
	public function __construct() {
		parent::__construct (); // if it is not called. then below model will not work.
		$this->m_functions->login_control ();
		$this->load->model ( "m_common", "cquery" );
		$this->load->model ( "m_functions", "cfun" );
	}

	/**
	 *
	 * @param mixed $data
	 *        	default function of this class
	 */
	public function view_students($data = "") {


		$data ["breadcrumb"] = array (
			array (
				"name" => "Dashboard",
				"url" => site_url ( "admin" )
			),
			array (
				"name" => "Students Management",
				"url" => site_url ( "admin/students" )
			)
		);

		$data ["view_data"] = $this->m_student->view_students ();

		$this->load->view ( 'admin/student_admission/index', $data );
	}
	public function print_form($id = "1") {
		$data ["breadcrumb"] = array (
			array (
				"name" => "Dashboard",
				"url" => site_url ( "admin" )
			),
			array (
				"name" => "Students",
				"url" => site_url ( "admin/students" )
			)
		);

		$data ["a_classes"] = $this->cquery->sel_all_rec_fields ( "classes", "class_id,class_name" );
		$data ["a_sections"] = $this->cquery->sel_all_rec_fields ( "sections", "section_id,section_name" );
		$data ["a_branches"] = $this->cquery->sel_all_rec_fields ( "branches", "branch_id,branch_name" );
		$data ["data_pi"] = $this->cquery->sel_spe_rec_objects ( "student_pi", "student_id", $id );
		$data ["data_ai"] = $this->cquery->sel_spe_rec_objects ( "student_ai", "student_id", $id );
		$data ["data_gi"] = $this->cquery->sel_spe_rec_objects ( "student_gi", "student_id", $id );
		$data ["data_ei"] = $this->cquery->sel_spe_rec_objects ( "student_ei", "student_id", $id );

		$this->load->view ( "admin/student_admission/admin_print", $data );
	}
	public function view_student($id) {
		$data["d_branch_id"]=$this->m_common->select_signle_field_and_row("student_pi",array("deletable"=>"1","editable"=>"1","student_id"=>$id),"branch_id");
		$data["branch_id"]=$this->session->userdata("branch_id");
		if(!($data["d_branch_id"]==$data["branch_id"])){
			$data ["message"] = $this->cfun->code_sending ( 100, "This is not valid access");
			$this->view_students ( $data );

		}else{
			$data ["breadcrumb"] = array (
				array (
					"name" => "Dashboard",
					"url" => site_url ( "admin" )
				),
				array (
					"name" => "Students Management",
					"url" => site_url ( "admin/students" )
				)
			);
			$data ["a_classes"] = $this->cquery->sel_all_rec_fields ( "classes", "class_id,class_name" );
			$data ["a_sections"] = $this->cquery->sel_all_rec_fields ( "sections", "section_id,section_name" );
			$data ["a_branches"] = $this->cquery->sel_all_rec_fields ( "branches", "branch_id,branch_name" );
			$data ["data_pi"] = $this->cquery->sel_spe_rec_objects ( "student_pi", "student_id", $id );
			$data ["data_ai"] = $this->cquery->sel_spe_rec_objects ( "student_ai", "student_id", $id );
			$data ["data_gi"] = $this->cquery->sel_spe_rec_objects ( "student_gi", "student_id", $id );
			$data ["data_ei"] = $this->cquery->sel_spe_rec_objects ( "student_ei", "student_id", $id );
			$this->load->view ( "admin/student_admission/view", $data );
		}

	}

	/**
	 * add question in the db.
	 * page will be loaded
	 */
	public function add_student() {
		$data ["breadcrumb"] = array (
			array (
				"name" => "Dashboard",
				"url" => site_url ( "admin" )
			),
			array (
				"name" => "Students Management",
				"url" => site_url ( "admin/students" )
			)
		);

		$data ["a_classes"] = $this->cquery->sel_all_rec_fields ( "classes", "class_id,class_name" );
		$data ["a_sections"] = $this->cquery->sel_all_rec_fields ( "sections", "section_id,section_name" );
		$data ["a_branches"] = $this->cquery->sel_all_rec_fields ( "branches", "branch_id,branch_name" );

		if (isset ( $_POST ["submit"] )) {

			$data ['first_name'] = trim ( $this->input->post ( 'first_name' ) );
			$data ['last_name'] = trim ( $this->input->post ( 'last_name' ) );
			$data ['gender'] = trim ( $this->input->post ( 'gender' ) );
			$data ['dob'] = trim ( $this->input->post ( 'dob' ) );
			$data ['cnic'] = trim ( $this->input->post ( 'cnic' ) );
			$data ['religion'] = trim ( $this->input->post ( 'religion' ) );
			$data ['domicile'] = trim ( $this->input->post ( 'domicile' ) );
			$data ['caste'] = trim ( $this->input->post ( 'caste' ) );
			$data ['tnbs'] = trim ( $this->input->post ( 'tnbs' ) );
			$data ['pbse'] = trim ( $this->input->post ( 'pbse' ) );
			$data ['email'] = trim ( $this->input->post ( 'email' ) );
			$data ['cell_no'] = trim ( $this->input->post ( 'cell_no' ) );
			$data ['country'] = trim ( $this->input->post ( 'country' ) );
			$data ['city'] = trim ( $this->input->post ( 'city' ) );
			$data ['other_city'] = trim ( $this->input->post ( 'other_city' ) );
			$data ['town'] = trim ( $this->input->post ( 'town' ) );
			$data ['permanent_address'] = trim ( $this->input->post ( 'permanent_address' ) );
			$data ['present_address'] = trim ( $this->input->post ( 'present_address' ) );

			$data ['guardian_type'] = trim ( $this->input->post ( 'guardian_type' ) );
			$data ['guardian_name'] = trim ( $this->input->post ( 'guardian_name' ) );
			$data ['guardian_cnic'] = trim ( $this->input->post ( 'guardian_cnic' ) );
			$data ['guardian_occupation'] = trim ( $this->input->post ( 'guardian_occupation' ) );
			$data ['guardian_status'] = trim ( $this->input->post ( 'guardian_status' ) );
			$data ['guardian_education'] = trim ( $this->input->post ( 'guardian_education' ) );
			$data ['guardian_phone'] = trim ( $this->input->post ( 'guardian_phone' ) );
			$data ['guardian_email'] = trim ( $this->input->post ( 'guardian_email' ) );
			$data ['guardian_cellno1'] = trim ( $this->input->post ( 'guardian_cellno1' ) );
			$data ['guardian_cellno2'] = trim ( $this->input->post ( 'guardian_cellno2' ) );

			$data ['a_degree_type'] = trim ( $this->input->post ( 'a_degree_type' ) );
			$data ['a_class_name'] = trim ( $this->input->post ( 'a_class_name' ) );
			$data ['a_institute'] = trim ( $this->input->post ( 'a_institute' ) );
			$data ['a_obtained_marks'] = trim ( $this->input->post ( 'a_obtained_marks' ) );
			$data ['a_total_marks'] = trim ( $this->input->post ( 'a_total_marks' ) );
			$data ['a_starting_date'] = trim ( $this->input->post ( 'a_starting_date' ) );
			$data ['a_ending_date'] = trim ( $this->input->post ( 'a_ending_date' ) );

			$data ['dor'] = trim ( $this->input->post ( 'dor' ) );
			$data ['class_id'] = trim ( $this->input->post ( 'class_id' ) );
			$data ['section_id'] = trim ( $this->input->post ( 'section_id' ) );

			$this->form_validation->set_rules ( "first_name", "First Name", "required" );

			if ($this->form_validation->run () === TRUE) {
				$data ["roll_no"] = $this->m_student->get_roll_no ( $data ["branch_id"] );
				$ins_data = array (
					'roll_no' => $data ["roll_no"],
					'first_name' => $data ["first_name"],
					'last_name' => $data ["last_name"],
					'gender' => $data ["gender"],
					'caste' => $data ["caste"],
					'cnic' => $data ["cnic"],
					'tnbs' => $data ["tnbs"],
					'pbse' => $data ["pbse"],
					'dob' => $data ["dob"],
					'email' => $data ["email"],
					'cell_no' => $data ["cell_no"],
					'domicile' => $data ["domicile"],
					'religion' => $data ["religion"],
					'town' => $data ["town"],
					'city' => $data ["city"],
					'other_city' => $data ["other_city"],
					'country' => $data ["country"],
					'permanent_address' => $data ["permanent_address"],
					'present_address' => $data ["present_address"],
					'created_date_time' => date ( 'Y-m-d H:i:s' )
				);

				$data ["student_id"] = $this->m_student->ins_rec_ri ( "student_pi", $ins_data );

				$ins_data_gi = array (
					'student_id' => $data ["student_id"],
					'guardian_type' => $data ["guardian_type"],
					'guardian_name' => $data ["guardian_name"],
					'guardian_cnic' => $data ["guardian_cnic"],
					'guardian_status' => $data ["guardian_status"],
					'guardian_occupation' => $data ["guardian_occupation"],
					'guardian_education' => $data ["guardian_education"],
					'guardian_phone' => $data ["guardian_phone"],
					'guardian_email' => $data ["guardian_email"],
					'guardian_cellno1' => $data ["guardian_cellno1"],
					'guardian_cellno2' => $data ["guardian_cellno2"],
					'created_date_time' => date ( 'Y-m-d H:i:s' )
				);

				$this->cquery->ins_rec ( "student_gi", $ins_data_gi );

				$data_ai = array (
					'student_id' => $data ["student_id"],
					'a_degree_type' => $data ["a_degree_type"],
					'a_class_name' => $data ["a_class_name"],
					'a_institute' => $data ["a_institute"],
					'a_obtained_marks' => $data ["a_obtained_marks"],
					'a_total_marks' => $data ["a_total_marks"],
					'a_starting_date' => $data ["a_starting_date"],
					'a_ending_date' => $data ["a_ending_date"],
					'created_date_time' => date ( 'Y-m-d H:i:s' )
				);
				$this->cquery->ins_rec ( "student_ai", $data_ai );

				if ($this->session->userdata ( "branch_id" ) == '-1') {
					$bid = $data ['branch_id'];
				} else {
					$bid = $this->session->userdata ( "branch_id" );
				}

				$data_ei = array (
					'branch_id' => $bid,
					'student_id' => $data ["student_id"],
					'dor' => $data ["dor"],
					'class_id' => $data ["class_id"],
					'section_id' => $data ["section_id"],
					'enrollment_status' => "1",
					'created_date_time' => date ( 'Y-m-d H:i:s' )
				);
				$this->cquery->ins_rec ( "student_ei", $data_ei );
				$data ["message"] = $this->cfun->code_sending ( 200, "Student having Student ID \"" . $data ["student_id"] . "\"
                 and Roll No \"" . $data ["roll_no"] . "\" has added Successfully" );
			} else {
				$data ["message"] = $this->cfun->code_sending ( 100, "Validate Form" );
			}
		}
		$this->load->view ( "admin/student_admission/add", $data );
	}
	public function del_student($id) {
		$data["d_branch_id"]=$this->m_common->select_signle_field_and_row("student_pi",array("deletable"=>"1","editable"=>"1","student_id"=>$id),"branch_id");
		$data["branch_id"]=$this->session->userdata("branch_id");
		if(!($data["d_branch_id"]==$data["branch_id"])){
			$data ["message"] = $this->cfun->code_sending ( 100, "This is not valid access");
			$this->view_students ( $data );
		}else {
			$this->cquery->upd_rec("student_pi","student_id", $id,array("deletable"=>"0"));
			$this->cquery->upd_rec("student_ai", "student_id", $id,array("deletable"=>"0"));
			$this->cquery->upd_rec("student_gi", "student_id", $id,array("deletable"=>"0"));
			$this->cquery->upd_rec("student_ei", "student_id", $id,array("deletable"=>"0"));
			$data ["message"] = $this->cfun->code_sending(200, "Student  having Student ID <b> " . $id . "</b> has Deleted Successfully");
			$this->view_students($data);
		}
	}
	public function edit_student($id) {
		$data["d_branch_id"]=$this->m_common->select_signle_field_and_row("student_pi",array("deletable"=>"1","editable"=>"1","student_id"=>$id),"branch_id");
		$data["branch_id"]=$this->session->userdata("branch_id");
		if(!($data["d_branch_id"]==$data["branch_id"])){
				$data ["message"] = $this->cfun->code_sending ( 100, "This is not valid access");
				$this->view_students ( $data );
		}
		else {
			$data ["student_id"] = $id;
			$data ["breadcrumb"] = array(
				array(
					"name" => "Dashboard",
					"url" => site_url("admin")
				),
				array(
					"name" => "Student Management",
					"url" => site_url("admin/students")
				)
			);

			$data ["a_classes"] = $this->cquery->sel_all_rec_fields("classes", "class_id,class_name");
			$data ["a_sections"] = $this->cquery->sel_all_rec_fields("sections", "section_id,section_name");
			$data ["a_branches"] = $this->cquery->sel_all_rec_fields("branches", "branch_id,branch_name");

			if (isset ($_POST ["submit"])) {
				$data ['first_name'] = trim($this->input->post('first_name'));
				$data ['last_name'] = trim($this->input->post('last_name'));
				$data ['gender'] = trim($this->input->post('gender'));
				$data ['dob'] = trim($this->input->post('dob'));
				$data ['cnic'] = trim($this->input->post('cnic'));
				$data ['religion'] = trim($this->input->post('religion'));
				$data ['domicile'] = trim($this->input->post('domicile'));
				$data ['caste'] = trim($this->input->post('caste'));
				$data ['tnbs'] = trim($this->input->post('tnbs'));
				$data ['pbse'] = trim($this->input->post('pbse'));
				$data ['email'] = trim($this->input->post('email'));
				$data ['cell_no'] = trim($this->input->post('cell_no'));
				$data ['country'] = trim($this->input->post('country'));
				$data ['city'] = trim($this->input->post('city'));
				$data ['other_city'] = trim($this->input->post('other_city'));
				$data ['town'] = trim($this->input->post('town'));
				$data ['permanent_address'] = trim($this->input->post('permanent_address'));
				$data ['present_address'] = trim($this->input->post('present_address'));

				$data ['guardian_type'] = trim($this->input->post('guardian_type'));
				$data ['guardian_name'] = trim($this->input->post('guardian_name'));
				$data ['guardian_cnic'] = trim($this->input->post('guardian_cnic'));
				$data ['guardian_occupation'] = trim($this->input->post('guardian_occupation'));
				$data ['guardian_status'] = trim($this->input->post('guardian_status'));
				$data ['guardian_education'] = trim($this->input->post('guardian_education'));
				$data ['guardian_phone'] = trim($this->input->post('guardian_phone'));
				$data ['guardian_email'] = trim($this->input->post('guardian_email'));
				$data ['guardian_cellno1'] = trim($this->input->post('guardian_cellno1'));
				$data ['guardian_cellno2'] = trim($this->input->post('guardian_cellno2'));

				$data ['a_degree_type'] = trim($this->input->post('a_degree_type'));
				$data ['a_class_name'] = trim($this->input->post('a_class_name'));
				$data ['a_institute'] = trim($this->input->post('a_institute'));
				$data ['a_obtained_marks'] = trim($this->input->post('a_obtained_marks'));
				$data ['a_total_marks'] = trim($this->input->post('a_total_marks'));
				$data ['a_starting_date'] = trim($this->input->post('a_starting_date'));
				$data ['a_ending_date'] = trim($this->input->post('a_ending_date'));

				$data ['dor'] = trim($this->input->post('dor'));
				$data ['class_id'] = trim($this->input->post('class_id'));
				$data ['section_id'] = trim($this->input->post('section_id'));
				$data ['fee'] = trim($this->input->post('fee'));
				$data ['enrollment_status'] = trim($this->input->post('enrollment_status'));

				$this->form_validation->set_rules("first_name", "First Name", "required");

				if ($this->form_validation->run() === TRUE) {

					$ins_data = array(

						'first_name' => $data ["first_name"],
						'last_name' => $data ["last_name"],
						'gender' => $data ["gender"],
						'caste' => $data ["caste"],
						'cnic' => $data ["cnic"],
						'tnbs' => $data ["tnbs"],
						'pbse' => $data ["pbse"],
						'dob' => $data ["dob"],
						'email' => $data ["email"],
						'cell_no' => $data ["cell_no"],
						'domicile' => $data ["domicile"],
						'religion' => $data ["religion"],
						'town' => $data ["town"],
						'city' => $data ["city"],
						'other_city' => $data ["other_city"],
						'country' => $data ["country"],
						'permanent_address' => $data ["permanent_address"],
						'present_address' => $data ["present_address"]
					);

					$this->cquery->upd_rec("student_pi", "student_id", $id, $ins_data);

					$ins_data_gi = array(
						'guardian_type' => $data ["guardian_type"],
						'guardian_name' => $data ["guardian_name"],
						'guardian_cnic' => $data ["guardian_cnic"],
						'guardian_status' => $data ["guardian_status"],
						'guardian_occupation' => $data ["guardian_occupation"],
						'guardian_education' => $data ["guardian_education"],
						'guardian_phone' => $data ["guardian_phone"],
						'guardian_email' => $data ["guardian_email"],
						'guardian_cellno1' => $data ["guardian_cellno1"],
						'guardian_cellno2' => $data ["guardian_cellno2"]
					);

					$this->cquery->upd_rec("student_gi", "student_id", $id, $ins_data_gi);

					$data_ai = array(
						'a_degree_type' => $data ["a_degree_type"],
						'a_class_name' => $data ["a_class_name"],
						'a_institute' => $data ["a_institute"],
						'a_obtained_marks' => $data ["a_obtained_marks"],
						'a_total_marks' => $data ["a_total_marks"],
						'a_starting_date' => $data ["a_starting_date"],
						'a_ending_date' => $data ["a_ending_date"]
					);
					$this->cquery->upd_rec("student_ai", "student_id", $id, $data_ai);

					if ($this->m_functions->user_access($this->session->userdata('user_type')) == 'super') {
						$data ['branch_id'] = trim($this->input->post('branch_id'));

						$data_ei = array(
							'dor' => $data ["dor"],
							'branch_id' => $data ["branch_id"],
							'class_id' => $data ["class_id"],
							'section_id' => $data ["section_id"],
							'fee' => $data ["fee"],
							'enrollment_status' => $data ["enrollment_status"],
							'created_date_time' => date('Y-m-d H:i:s')
						);
					} else {
						$data_ei = array(
							'dor' => $data ["dor"],
							'class_id' => $data ["class_id"],
							'section_id' => $data ["section_id"],
							'fee' => $data ["fee"],
							'enrollment_status' => $data ["enrollment_status"],
							'created_date_time' => date('Y-m-d H:i:s')
						);
					}
					$this->cquery->upd_rec("student_ei", "student_id", $id, $data_ei);
					$data ["message"] = $this->cfun->code_sending(200, "Student having Student ID \"" . $data ["student_id"] . "\"
                  has updated Successfully");
				} else {
					$data ["message"] = $this->cfun->code_sending(100, "Validate Form");
				}
			}
			elseif (isset ($_POST ["upload_picture"])) {
				$data ["data_pi"] = $this->cquery->sel_spe_rec_objects("student_pi", "student_id", $id);
				$data ["data_ai"] = $this->cquery->sel_spe_rec_objects("student_ai", "student_id", $id);
				$data ["data_gi"] = $this->cquery->sel_spe_rec_objects("student_gi", "student_id", $id);
				$data ["data_ei"] = $this->cquery->sel_spe_rec_objects("student_ei", "student_id", $id);


				$ar = array(
					"field_name" => "profile_picture",
					"file_name" => "student_id_" . $id,
					"path_to_file" => "students/"
				);
				$dd = $this->m_functions->upload($ar);
				if ($dd ["condition"] == "1") {
					$data ["message"] = $this->cfun->code_sending(100, "Uploading Picture Failed ! " . $dd ["msg"]);
				} elseif ($dd ["condition"] == "2") {
					$data ["message"] = $this->cfun->code_sending(200, "Picture Uploaded Successfully  ");
				}

			}
			else {
				$data ["data_pi"] = $this->cquery->sel_spe_rec_objects("student_pi", "student_id", $id);
				$data ["data_ai"] = $this->cquery->sel_spe_rec_objects("student_ai", "student_id", $id);
				$data ["data_gi"] = $this->cquery->sel_spe_rec_objects("student_gi", "student_id", $id);
				$data ["data_ei"] = $this->cquery->sel_spe_rec_objects("student_ei", "student_id", $id);
			}
			$this->load->view("admin/student_admission/edit", $data);
		}
	}
}
