<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_staff extends CI_Controller {


    public function __construct(){
        parent::__construct(); // if it is not called. then below model will not work.
        $this->m_functions->login_control();
        $this->load->model("m_common","cquery");
        $this->load->model("m_functions","cfun");
        $this->load->model("m_staff");
    }

    /**
     * @param mixed $data
     * default function of this class
     */


    public function view_staffs($data="")
    {
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Staff",
                "url"=>site_url("admin/staff")
            )
        );

        $data["widget_title"] = "Staff Management";
        $data["view_data"]=$this->m_staff->view_staff();
        $this->load->view('admin/staff/view_all', $data);
    }


    public function print_form($id)
    {

        $data["branch_id"] = $this->session->userdata("branch_id");
        $data["a_spouse_type"] = $this->cquery->select_variable("a_spouse_type");
        $data["a_gender"] = $this->cquery->select_variable("gender");
        $data["a_experience_type"] = $this->cquery->select_variable("a_experience_type");
        $data["a_staff_designation"] = $this->cquery->select_variable("a_staff_designation");
        $data["a_job_type"] = $this->cquery->select_variable("a_job_type");
        $data["data_pi"] = $this->cquery->sel_spe_rec_fields_objects("staff_pi", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_ai"] = $this->cquery->sel_spe_rec_fields_objects("staff_ai", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_exi"] = $this->cquery->sel_spe_rec_fields_objects("staff_exi", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_ji"] = $this->cquery->sel_spe_rec_fields_objects("staff_ji", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));

        $this->load->view("admin/staff/print",$data);
}

    public function view_staff($id){
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Staff",
                "url"=>site_url("admin/staff")
            )
        );


        $data["widget_title"] = "Staff Management";
        $data["branch_id"] = $this->session->userdata("branch_id");
        $data["a_spouse_type"] = $this->cquery->select_variable("a_spouse_type");
        $data["a_gender"] = $this->cquery->select_variable("gender");
        $data["a_experience_type"] = $this->cquery->select_variable("a_experience_type");
        $data["a_staff_designation"] = $this->cquery->select_variable("a_staff_designation");
        $data["a_job_type"] = $this->cquery->select_variable("a_job_type");
        $data["data_pi"] = $this->cquery->sel_spe_rec_fields_objects("staff_pi", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_ai"] = $this->cquery->sel_spe_rec_fields_objects("staff_ai", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_exi"] = $this->cquery->sel_spe_rec_fields_objects("staff_exi", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_ji"] = $this->cquery->sel_spe_rec_fields_objects("staff_ji", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));

        $this->load->view("admin/staff/view",$data);
    }

    /**
     * add question in the db. page will be loaded
     */
    public function add_staff(){

        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Staff",
                "url"=>site_url("admin/staff")
            )
        );
        $data["widget_title"] = "Staff Management";
        $data["branch_id"]=$this->session->userdata("branch_id");
        $data["a_spouse_type"] = $this->cquery->select_variable("a_spouse_type");
        $data["a_gender"] = $this->cquery->select_variable("gender");
        $data["a_experience_type"] = $this->cquery->select_variable("a_experience_type");
        $data["a_staff_designation"] = $this->cquery->select_variable("a_staff_designation");
        $data["a_job_type"] = $this->cquery->select_variable("a_job_type");

        if (isset($_POST["submit"])) {


            $data['first_name'] 			= trim($this->input->post('first_name'));
            $data['last_name'] 		 		= trim($this->input->post('last_name'));
            $data['spouse_type'] 		 	= trim($this->input->post('spouse_type'));
            $data['spouse_name'] 		 		= trim($this->input->post('spouse_name'));
            $data['gender'] 				= trim($this->input->post('gender'));
            $data['dob'] 					= trim($this->input->post('dob'));
            $data['cnic'] 					= trim($this->input->post('cnic'));
            $data['religion'] 				= trim($this->input->post('religion'));
            $data['domicile'] 				= trim($this->input->post('domicile'));
            $data['phone_no'] 					= trim($this->input->post('phone_no'));
            $data['marital_status'] 					= trim($this->input->post('marital_status'));
            $data['email'] 					= trim($this->input->post('email'));
            $data['cell_no'] 				= trim($this->input->post('cell_no'));
            $data['country'] 				= trim($this->input->post('country'));
            $data['city'] 					= trim($this->input->post('city'));
            $data['other_city'] 			= trim($this->input->post('other_city'));
            $data['town'] 					= trim($this->input->post('town'));
            $data['permanent_address'] 	    = trim($this->input->post('permanent_address'));
            $data['present_address'] 		= trim($this->input->post('present_address'));



            $data['a_degree_type'] 			= trim($this->input->post('a_degree_type'));
            $data['a_class_name'] 			= trim($this->input->post('a_class_name'));
            $data['a_institute'] 			= trim($this->input->post('a_institute'));
            $data['a_obtained_marks'] 		= trim($this->input->post('a_obtained_marks'));
            $data['a_total_marks'] 			= trim($this->input->post('a_total_marks'));
            $data['a_starting_date'] 		= trim($this->input->post('a_starting_date'));
            $data['a_ending_date'] 			= trim($this->input->post('a_ending_date'));

            $data['experience_type'] 			= trim($this->input->post('experience_type'));
            $data['designation_name'] 			= trim($this->input->post('designation_name'));
            $data['organization_name'] 			= trim($this->input->post('organization_name'));
            $data['salary'] 		            = trim($this->input->post('salary'));
            $data['starting_date'] 			= trim($this->input->post('starting_date'));
            $data['ending_date'] 		= trim($this->input->post('ending_date'));

            $data['doa'] 					= trim($this->input->post('doa'));
            $data['job_type'] 				= trim($this->input->post('job_type'));
            $data['staff_designation'] 			= trim($this->input->post('staff_designation'));
            $data['basic_salary'] 					= trim($this->input->post('basic_salary'));
            $data['comments'] 					= trim($this->input->post('comments'));




            $this->form_validation->set_rules("first_name","First Name", "required");

            if($this->form_validation->run() === TRUE) {
                $data["b_staff_id"]=$this->m_staff->get_staff_id($data["branch_id"]);
                $ins_data = array(
                        'first_name'			=>	$data["first_name"],
                        'last_name'     		=>  $data["last_name"],
                        'gender'				=>	$data["gender"],
                    'spouse_type'				=>	$data["spouse_type"],
                    'spouse_name'				=>	$data["spouse_name"],
                    'phone_no'     			=>  $data["phone_no"],
                    'marital_status'     			=>  $data["marital_status"],
                        'cnic'     				=>  $data["cnic"],
                        'dob'     				=>  $data["dob"],
                        'email'     			=>  $data["email"],
                        'cell_no'				=>	$data["cell_no"],
                        'domicile'     			=>  $data["domicile"],
                        'religion'				=>	$data["religion"],
                        'town'     				=>  $data["town"],
                        'city'					=>	$data["city"],
                        'other_city'			=>	$data["other_city"],
                        'country'				=>	$data["country"],
                        'permanent_address'     =>  $data["permanent_address"],
                        'present_address'     	=>  $data["present_address"],
                    'branch_id' => $data["branch_id"],
                        'created_date_time'		=>	date('Y-m-d H:i:s')
                );

                $data["staff_id"]=$this->m_staff->ins_rec_ri("staff_pi", $ins_data);



                $data_ai = array(
                    'staff_id'			    =>  $data["staff_id"],
                    'a_degree_type'			=>	$data["a_degree_type"],
                    'a_class_name'    	 	=>  $data["a_class_name"],
                    'a_institute'			=>	$data["a_institute"],
                    'a_obtained_marks'     	=>  $data["a_obtained_marks"],
                    'a_total_marks' 		=>  $data["a_total_marks"],
                    'a_starting_date' 		=>  $data["a_starting_date"],
                    'a_ending_date'			=>	$data["a_ending_date"],
                    'branch_id' => $data["branch_id"],
                    'created_date_time'     =>  date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("staff_ai",$data_ai);

                $data_exi = array(
                    'staff_id'			    =>  $data["staff_id"],
                    'organization_name'			=>	$data["organization_name"],
                    'experience_type'    	 	=>  $data["experience_type"],
                    'designation_name'			=>	$data["designation_name"],
                    'salary'     	=>  $data["salary"],
                    'starting_date' 		=>  $data["starting_date"],
                    'ending_date' 		=>  $data["ending_date"],
                    'branch_id' => $data["branch_id"],
                    'created_date_time'     =>  date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("staff_exi",$data_exi);

                    $data_ji = array (
                        'staff_id' => $data ["staff_id"],
                        'branch_id' => $data["branch_id"],
                        'doa' => $data ["doa"],
                        'branch_id' => $data ["branch_id"],
                        'basic_salary' => $data ["basic_salary"],
                        'job_type' => $data ["job_type"],
                        'staff_designation' => $data ["staff_designation"],
                        'comments' => $data ["comments"],
                        'created_date_time' => date ( 'Y-m-d H:i:s' )
                    );

                $this->cquery->ins_rec("staff_ji",$data_ji);
                $data["message"]=$this->cfun->code_sending(200,"Staff has added Successfully");

            }
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form");
            }


        }
        $this->load->view("admin/staff/add", $data);

    }




    public function del_staff($id)
    {
        $delete_data = array("deletable" => '0');
        $this->cquery->upd_rec("staff_pi", "staff_id", $id, $delete_data);
        $this->cquery->upd_rec("staff_ai", "staff_id", $id, $delete_data);
        $this->cquery->upd_rec("staff_exi", "staff_id", $id, $delete_data);
        $this->cquery->upd_rec("staff_ji", "staff_id", $id, $delete_data);
            $data["message"]=$this->cfun->code_sending(200,"Staff Data has  Deleted Successfully");
        $this->view_staffs($data);
    }


    public function edit_staff($id){

        $data["staff_id"]=$id;
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Staff",
                "url"=>site_url("admin/staff")
            )
        );
        $data["widget_title"] = "Staff Management";
        $data["branch_id"] = $this->session->userdata("branch_id");
        $data["a_spouse_type"] = $this->cquery->select_variable("a_spouse_type");
        $data["a_gender"] = $this->cquery->select_variable("gender");
        $data["a_experience_type"] = $this->cquery->select_variable("a_experience_type");
        $data["a_staff_designation"] = $this->cquery->select_variable("a_staff_designation");
        $data["a_job_type"] = $this->cquery->select_variable("a_job_type");

    if(isset($_POST["submit"])){


        $data['first_name'] 			= trim($this->input->post('first_name'));
        $data['last_name'] 		 		= trim($this->input->post('last_name'));
        $data['spouse_type'] 		 	= trim($this->input->post('spouse_type'));
        $data['spouse_name'] 		 		= trim($this->input->post('spouse_name'));
        $data['gender'] 				= trim($this->input->post('gender'));
        $data['dob'] 					= trim($this->input->post('dob'));
        $data['cnic'] 					= trim($this->input->post('cnic'));
        $data['religion'] 				= trim($this->input->post('religion'));
        $data['domicile'] 				= trim($this->input->post('domicile'));
        $data['phone_no'] 					= trim($this->input->post('phone_no'));
        $data['marital_status'] 					= trim($this->input->post('marital_status'));
        $data['email'] 					= trim($this->input->post('email'));
        $data['cell_no'] 				= trim($this->input->post('cell_no'));
        $data['country'] 				= trim($this->input->post('country'));
        $data['city'] 					= trim($this->input->post('city'));
        $data['other_city'] 			= trim($this->input->post('other_city'));
        $data['town'] 					= trim($this->input->post('town'));
        $data['permanent_address'] 	    = trim($this->input->post('permanent_address'));
        $data['present_address'] 		= trim($this->input->post('present_address'));



        $data['a_degree_type'] 			= trim($this->input->post('a_degree_type'));
        $data['a_class_name'] 			= trim($this->input->post('a_class_name'));
        $data['a_institute'] 			= trim($this->input->post('a_institute'));
        $data['a_obtained_marks'] 		= trim($this->input->post('a_obtained_marks'));
        $data['a_total_marks'] 			= trim($this->input->post('a_total_marks'));
        $data['a_starting_date'] 		= trim($this->input->post('a_starting_date'));
        $data['a_ending_date'] 			= trim($this->input->post('a_ending_date'));

        $data['experience_type'] 			= trim($this->input->post('experience_type'));
        $data['designation_name'] 			= trim($this->input->post('designation_name'));
        $data['organization_name'] 			= trim($this->input->post('organization_name'));
        $data['salary'] 		            = trim($this->input->post('salary'));
        $data['starting_date'] 			= trim($this->input->post('starting_date'));
        $data['ending_date'] 		= trim($this->input->post('ending_date'));

        $data['doa'] 					= trim($this->input->post('doa'));
        $data['job_type'] 				= trim($this->input->post('job_type'));
        $data['staff_designation'] 			= trim($this->input->post('staff_designation'));
        $data['basic_salary'] 					= trim($this->input->post('basic_salary'));
        $data['allowance'] 					= trim($this->input->post('allowance'));
        $data['comments'] 					= trim($this->input->post('comments'));
        $data['allowance_description'] 		= trim($this->input->post('allowance_description'));

        $this->form_validation->set_rules("first_name","First Name", "required");

        if($this->form_validation->run() === TRUE) {
            $ins_data = array(
                'first_name'			=>	$data["first_name"],
                'last_name'     		=>  $data["last_name"],
                'gender'				=>	$data["gender"],
                'spouse_type'				=>	$data["spouse_type"],
                'spouse_name'				=>	$data["spouse_name"],
                'phone_no'     			=>  $data["phone_no"],
                'marital_status'     			=>  $data["marital_status"],
                'cnic'     				=>  $data["cnic"],
                'dob'     				=>  $data["dob"],
                'email'     			=>  $data["email"],
                'cell_no'				=>	$data["cell_no"],
                'domicile'     			=>  $data["domicile"],
                'religion'				=>	$data["religion"],
                'town'     				=>  $data["town"],
                'city'					=>	$data["city"],
                'other_city'			=>	$data["other_city"],
                'country'				=>	$data["country"],
                'permanent_address'     =>  $data["permanent_address"],
                'present_address'     	=>  $data["present_address"]
            );

            $this->cquery->upd_rec("staff_pi","staff_id",$id, $ins_data);


            $data_ai = array(
                'a_degree_type'			=>	$data["a_degree_type"],
                'a_class_name'    	 	=>  $data["a_class_name"],
                'a_institute'			=>	$data["a_institute"],
                'a_obtained_marks'     	=>  $data["a_obtained_marks"],
                'a_total_marks' 		=>  $data["a_total_marks"],
                'a_starting_date' 		=>  $data["a_starting_date"],
                'a_ending_date'			=>	$data["a_ending_date"]
            );

            $this->cquery->upd_rec("staff_ai","staff_id",$id, $data_ai);

            $data_exi = array(
                'organization_name'			=>	$data["organization_name"],
                'experience_type'    	 	=>  $data["experience_type"],
                'designation_name'			=>	$data["designation_name"],
                'salary'     	            =>  $data["salary"],
                'starting_date' 		=>  $data["starting_date"],
                'ending_date' 		=>  $data["ending_date"]
            );
            $this->cquery->upd_rec("staff_exi","staff_id",$id, $data_exi);
                  $data_ji = array (
                    'staff_id' => $data ["staff_id"],
                    'doa' => $data ["doa"],
                    'basic_salary' => $data ["basic_salary"],
                    'job_type' => $data ["job_type"],
                    'staff_designation' => $data ["staff_designation"],
                    'comments' => $data ["comments"]
                );


            $this->cquery->upd_rec("staff_ji","staff_id",$id, $data_ji);
            $data["message"]=$this->cfun->code_sending(200,"Staff' information has updated Successfully");

        }
        else{
            $data["message"]=$this->cfun->code_sending(100,"Validate Form");
        }


    }
    elseif(isset($_POST["upload_picture"])){
        $data["data_pi"] = $this->cquery->sel_spe_rec_fields_objects("staff_pi", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_ai"] = $this->cquery->sel_spe_rec_fields_objects("staff_ai", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_exi"] = $this->cquery->sel_spe_rec_fields_objects("staff_exi", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_ji"] = $this->cquery->sel_spe_rec_fields_objects("staff_ji", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        if(file_exists("pdb/staff"."/staff_id_".$id.".jpg"))
            unlink("pdb/staff/staff_id_".$id.".jpg");
        elseif(file_exists("pdb/staff"."/staff_id_".$id.".jpeg"))
            unlink("pdb/staff/staff_id_".$id.".jpeg");
        elseif(file_exists("pdb/staff"."/staff_id_".$id.".png"))
            unlink("pdb/staff/staff_id_".$id.".png");
        elseif(file_exists("pdb/staff"."/staff_id_".$id.".gif"))
            unlink("pdb/staff/staff_id_".$id.".gif");

        $ar=array(
            "field_name"=>"userfile",
            "file_name"=>"staff_id_".$id,
            "path_to_file"=>"staff"
        );
        $dd=$this->m_functions->upload($ar);
        if($dd["condition"]=="1"){
//                $data["error"]=$dd["msg"];
            $data["message"]=$this->cfun->code_sending(100,"Uploading Picture Failed ! ".$dd["msg"]);
//                $this->load->view("testing/picture", $data);
        }elseif($dd["condition"]=="2"){
//                $data["upload_data"]=$dd["msg"];
            $data["message"]=$this->cfun->code_sending(200,"Picture Uploaded Successfully  ");


        }
    }
    else{
        $data["data_pi"] = $this->cquery->sel_spe_rec_fields_objects("staff_pi", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_ai"] = $this->cquery->sel_spe_rec_fields_objects("staff_ai", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_exi"] = $this->cquery->sel_spe_rec_fields_objects("staff_exi", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
        $data["data_ji"] = $this->cquery->sel_spe_rec_fields_objects("staff_ji", array("staff_id" => $id, "deletable" => '1', "branch_id" => $data["branch_id"]));
    }

$this->load->view("admin/staff/edit", $data);

}



}

