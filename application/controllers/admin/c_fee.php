<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_fee extends CI_Controller
{


    public function __construct()
    {
        parent::__construct(); // if it is not called. then below model will not work.
        $this->m_functions->login_control();

    }

    /**
     * @param mixed $data
     * default function of this class
     */
    public function view_all_receipts($data = "")
    {
        $data["widget_title"]="Fee Management";
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Fee Receipts Management",
                "url" => site_url("admin/fee/receipts")
            )
        );

        $data["view_data"] = $this->m_fee->all_receipts();

        $this->load->view('admin/fee/all_receipts', $data);
    }


    public function print_form($id = "1")
    {

        $this->load->view("admin/student_admission/print");
    }

    public function receipt_view($id="")
    {
        $data["widget_title"]="Fee Receipt";
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Fee Receipts Management",
                "url" => site_url("admin/fee/receipts")
            )
        );

        $data["view_data"] =$this->m_fee->view_receipt($id);
        $data["received_fee"]=$this->m_fee->get_received_fee_ids($data["view_data"]["received_fee_ids"]);


        $this->load->view("admin/fee/receipt_view", $data);
    }


    public function add_admission_fee()
    {
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Fee",
                "url" => site_url("admin/fee/receipts")
            )
        );
        //initialize
        $data["branch_id"] = $this->session->userdata("branch_id");
        $data["a_students"] = $this->m_common->select_students($data["branch_id"]);
        $data["a_fee_category"] = $this->m_common->select_variable("fee_category");
        if (isset($_POST["submit"])) {
            $data["student_id"] = trim($this->input->post("student_id"));
            $data["fee_category"] = count($this->input->post('fee_category')) ? $this->input->post('fee_category') : array();
            $data["fee"] = count($this->input->post('fee')) ? $this->input->post('fee') : array();
            $data["period"] = count($this->input->post('period')) ? $this->input->post('period') : array();


            $data["input_data"] = array();



            foreach ($data["fee_category"] as $rows) {
                array_push($data["input_data"], array("fee_category" => $rows, "fee" => "", "period" => ""));
            }
            foreach ($data["fee"] as $key => $value) {
                if(empty($value)){
                    $data["fc_id"]=$data["input_data"][$key]["fee_category"];
                    $data["fetch_class_id"]=$this->m_common->sel_spe_row_fields("student_ei",array("student_id"=>$data["student_id"],"deletable"=>"1"),"class_id");
                    $data["class_id"]=$data["fetch_class_id"]["class_id"];
                    $data["fetch_prog_fee"]=$this->m_common->sel_spe_row_fields("set_prog_fee",array("class_id"=>$data["class_id"],"branch_id"=>$data["branch_id"],"fc_id"=>$data["fc_id"]),"fee");

                    $data["input_data"][$key]["fee"] = $data["fetch_prog_fee"]["fee"];
                }
                else
                 $data["input_data"][$key]["fee"] = $value;
            }
            foreach ($data["period"] as $key => $value) {
                if(empty($value)){
                    $data["fc_id"]=$data["input_data"][$key]["fee_category"];
                    $data["fetch_class_id"]=$this->m_common->sel_spe_row_fields("student_ei",array("student_id"=>$data["student_id"],"deletable"=>"1"),"class_id");
                    $data["class_id"]=$data["fetch_class_id"]["class_id"];
                    $data["fetch_prog"]=$this->m_common->sel_spe_row_fields("set_prog_fee",array("class_id"=>$data["class_id"],"branch_id"=>$data["branch_id"],"fc_id"=>$data["fc_id"]),"period");

                    $data["input_data"][$key]["period"] = $data["fetch_prog"]["period"];
                }
                else
                    $data["input_data"][$key]["period"] = $value;
            }

            $this->form_validation->set_rules("student_id", "Student ID", "required");

            if ($this->form_validation->run() === TRUE) {
                $data["received_payment"]=0;
                $data["received_fee_ids"]="";
                $i=0;

                foreach ($data["input_data"] as $rows) {
                    $i++;

                    $ins_data = array(
                        'student_id' => $data["student_id"],
                        'fc_id' => $rows["fee_category"],
                        'fee' => $rows["fee"],
                        'period' => $rows["period"],
                        'created_date_time' => date('Y-m-d H:i:s')
                    );

                    $data["received_fee_ids"] .= count($this->input->post("fee_category"))>$i ? $this->m_student->ins_rec_ri("received_fee", $ins_data)."," :$this->m_student->ins_rec_ri("received_fee", $ins_data);
                    $data["received_payment"]+=$rows["fee"];

                }
                $ins_data=array(
                    "student_id"=>$data["student_id"],
                    "received_payment"=>$data["received_payment"],
                    "received_fee_ids"=>$data["received_fee_ids"],
                    "received_payment_date"=>date('Y-m-d'),
                    "created_date_time"=>date('Y-m-d H:i:s')
                );
                $data["receipt_id"]=$this->m_student->ins_rec_ri("fee_receipts",$ins_data);



                $data["message"] = $this->m_functions->code_sending(200, "Fee has added");
                $this->receipt_view($data["receipt_id"]);


            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }


        } else{

            $this->load->view("admin/fee/admission_fee", $data);
        }

    }






}
