<?php

class C_upload extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * type are : students, staff, user
     * @param string $type
     * @param string $id
     */
    function index($type="student",$id="1")
    {
        $data["id"]=$id;
        $data["type"]=$type;
        $this->load->view("html5cam/capture",$data);
    }


    function upload(){

        define('UPLOAD_DIR', 'pdb/');

        $data["type"]=$_POST["type"];
        $data["id"]=$_POST["id"];
        $img = $_POST['img'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data["image"] = base64_decode($img);
        switch($data["type"]){
            case "student":
                $data["upload_path"] = UPLOAD_DIR."students/";
                $data["file_name"] = 'student_id_'.$data["id"]. '.png';
                break;
            case "staff":
                echo "staff";
                break;
            case "user":
                echo "user";
                break;

        }

        $this->m_functions->check_file($data["upload_path"],$data["file_name"]);
        $data["file_config"]=$data["upload_path"].$data["file_name"];

        $success = file_put_contents($data["file_config"], $data["image"]);

        echo $success ? "1" : "-1";

    }
}
?>