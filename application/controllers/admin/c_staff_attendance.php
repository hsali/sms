<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by IntelliJ IDEA.
 * User: shehbaz
 * Date: 6/28/15
 * Time: 7:02 PM
 */

class C_staff_attendance extends CI_Controller
{


    public function __construct()
    {
        parent::__construct(); // if it is not called. then below model will not work.
        $this->m_functions->login_control();
    }

    /**
     * @param string $data
     * page will show the setting of page..
     * @todo make it in future
     */
    public function index($data = "")
    {

    }

    /**
     * @param mixed $data
     * default function of
     */
    public function attendance_of_all_staff($data = "")
    {

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Attendance",
                "url" => site_url("admin/attendance/staff")
            )
        );

        $data["view_data"] = $this->m_attendance->view_staff_attendance();
        $this->load->view('admin/staff_attendance/index', $data);
    }

    public function add()
    {
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Staff Attendance",
                "url" => site_url("admin/attendance/staff")
            ),
        );
        $data['branch_id'] = $this->session->userdata("branch_id");
        $data["a_staffs"]=$this->m_common->select_staffs($data["branch_id"]);

        if (isset($_POST["submit"])) {
            $data['staff_id'] = trim($this->input->post('staff_id'));
            $data['attendance'] = trim($this->input->post('attendance'));
            $data['attendance_time'] = $this->input->post('attendance_time');
            $this->form_validation->set_rules("staff_id", "Staff ID", "required");
            $this->form_validation->set_rules("attendance", "Attendance ", "required");
            $this->form_validation->set_rules("attendance_time", "Attendance Time", "required");

            if ($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "staff_id" => $data["staff_id"],
                    "attendance_status" => $data["attendance"],
                    "attendance_time" => $data["attendance_time"],
                    "branch_id" => $data["branch_id"],
                    "created_date_time" => date('Y-m-d H:i:s')
                );
                $this->m_common->ins_rec("staff_attendance", $ins_data);
                $data["message"] = $this->m_functions->code_sending(200, "Attendance of Staff has added Successfully");

            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }

        } else {
       /*     $data['attendance'] = "3";
            $data['staff_id'] = "3";
            $data['attendance_time'] = "2015-07-08 12:10:31";*/

        }
        $this->load->view("admin/staff_attendance/add", $data);

    }
    public function edit($id="")
    {
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Staff Attendance",
                "url" => site_url("admin/attendance/staff")
            ),
        );
        $data["a_staffs"]=$this->m_common->sel_spe_array_fields("staff_pi",array("deletable"=>"1"),"staff_id,first_name, last_name");

        if (isset($_POST["submit"])) {
            $data['staff_id'] = trim($this->input->post('staff_id'));
            $data['attendance'] = trim($this->input->post('attendance'));
            $data['attendance_time'] = $this->input->post('attendance_time');
            $this->form_validation->set_rules("staff_id", "Staff ID", "required");
            $this->form_validation->set_rules("attendance", "Attendance ", "required");
            $this->form_validation->set_rules("attendance_time", "Attendance Time", "required");

            if ($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "staff_id" => $data["staff_id"],
                    "attendance_status" => $data["attendance"],
                    "attendance_time" => $data["attendance_time"],
                );
                $this->m_common->upd_rec("staff_attendance","a_id",$id, $ins_data);
                $data["message"] = $this->m_functions->code_sending(200, "Attendance of Staff has added Successfully");

            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }

        } else {
            $data["view_data"]=$this->m_common->sel_spe_array_fields("staff_attendance",array("deletable"=>"1","a_id"=>$id),"attendance_status,attendance_time, staff_id");


        }
        $this->load->view("admin/staff_attendance/edit", $data);

    }

    public function staff_summary($id=""){
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Attendance",
                "url" => site_url("admin/attendance/staff")
            ),
        );


        $data["attendance_table"]=$this->m_common->sel_spe_array_fields_order("staff_attendance",array("deletable"=>"1","staff_id"=>$id),"a_id,attendance_time, attendance_status","attendance_time");
        $data["att"]=$this->m_common->sel_spe_row_fields("staff_attendance",array("deletable"=>"1","a_id"=>$id),"a_id,attendance_time, attendance_status");
        $data["staff_name"]=$this->m_common->sel_spe_row_fields("staff_pi",array("staff_id"=>$id),"first_name,last_name");
        $data["staff_id"]=$id;
        $data["id"]=$id;
        $data["total_present"]=$this->m_common->sel_spe_array_counts("staff_attendance",array("deletable"=>"1","staff_id"=>$id,"attendance_status"=>"1"),"a_id,attendance_status");
        $data["total_absent"]=$this->m_common->sel_spe_array_counts("staff_attendance",array("deletable"=>"1","staff_id"=>$id,"attendance_status"=>"2"),"a_id,attendance_status");
        $data["total_leave"]=$this->m_common->sel_spe_array_counts("staff_attendance",array("deletable"=>"1","staff_id"=>$id,"attendance_status"=>"3"),"a_id,attendance_status");
        $data["total_late"]=$this->m_common->sel_spe_array_counts("staff_attendance",array("deletable"=>"1","staff_id"=>$id,"attendance_status"=>"4"),"a_id,attendance_status");
     /*   var_dump($data);
        $mytime=$data["att"]["attendance_time"];
        echo "Time :".$mytime."</br>";
        $t1=$mytime;
        echo "Unix time : ".human_to_unix($mytime)."</br>";
        echo "Human time : ".unix_to_human("1436340980")."</br>";
        $m=mdate("%m",human_to_unix($mytime));
         echo $this->m_functions->num_into_month_name(07);
        exit;*/
//        var_dump($data);exit;
        $this->load->view("admin/staff_attendance/summary", $data);

}



    public function del($id)
    {   $upd_data=array("deletable"=>"0");
        $this->m_common->upd_rec("staff_attendance", "a_id", $id,$upd_data);
        $data["message"] = $this->m_functions->code_sending(200, "Variable has deleted Successfully");
        $this->attendance_of_all_staff($data);
    }


}