<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_settings extends CI_Controller {


    public function __construct(){
        parent::__construct(); // if it is not called. then below model will not work.
        $this->m_functions->login_control();
        $this->load->model("m_common","cquery");
        $this->load->model("m_functions","cfun");
        $this->load->model("m_settings");
    }



    public function view_all_values(){

    }

    public function add_spe_value($id){
        $data["id"]=$id;

        $ddd=$this->cquery->sel_spe_row_fields("var_name",array("var_id"=>$data["id"]),"var_title");

        $data["var_name"]=$ddd["var_title"];
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>$data["var_name"],
                "url"=>site_url("admin/settings/variables/view/".$data["id"])
            )
        );
        if (isset($_POST["submit"])) {
            $data['var_value']  	= trim($this->input->post('var_value'));
            $data['variable_value']  	= trim($this->input->post('variable_value'));
            $this->form_validation->set_rules("var_value", "Variable Value", "required");
            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "var_id" => $id,
                    "var_value" => $data["var_value"],
                    "variable_value" => $data["variable_value"],
                    "created_date_time"=> date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("var_value", $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Variable' Value Has Added Successfully");

            }
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form");
            }

        } else{

        }
        $this->load->view("admin/variable_values/add_specific_value", $data);

    }

    public function add_value(){

        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Values",
                "url"=>site_url("admin/settings/variable/values")
            )
        );
        $data["a_var_name"]=$this->cquery->sel_spe_array_fields("var_name",array("deletable"=>"1"),"var_id,var_name");
        if (isset($_POST["submit"])) {



            $data['var_id']  	= trim($this->input->post('var_id'));
            $data['var_value']  	= trim($this->input->post('var_value'));
            $data['variable_value']  	= empty(trim($this->input->post('variable_value')))?  "": trim($this->input->post('variable_value'));;
            $this->form_validation->set_rules("var_id", "Variable Value", "required");
            $this->form_validation->set_rules("var_value", "Variable Value", "required");
            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "var_id" => $data["var_id"],
                    "var_value" => $data["var_value"],
                    "variable_value"=>$data["variable_value"],
                    "created_date_time"=> date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("var_value", $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Variable' Value Has Added Successfully");

            }
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form");
            }

        } else{

        }
        $this->load->view("admin/variable_values/add", $data);

    }

    public function edit_value($id){
        $data["id"]=$id;
        $dd=$this->cquery->sel_spe_row_fields("var_value",array("var_vid"=>$id),"var_id");
        $ddd=$this->cquery->sel_spe_row_fields("var_name",array("var_id"=>$dd["var_id"]),"var_title");


        $data["var_name"]=$ddd["var_title"];
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>$data["var_name"],
                "url"=>site_url("admin/settings/variables/view/".$dd["var_id"])
            )
        );

        if (isset($_POST["submit"])) {
            $data['var_value']  	= trim($this->input->post('var_value'));
            $data['variable_value']  	= empty(trim($this->input->post('variable_value')))?  "": trim($this->input->post('variable_value'));
            $this->form_validation->set_rules("var_value", "Variable Value", "required");
            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "var_vid" => $id,
                    "var_value" => $data["var_value"],
                    "variable_value"=>$data["variable_value"],
                );
                $this->cquery->upd_rec("var_value","var_vid",$id, $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Variable' Value Has Updated Successfully");

            }
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form");
            }

        } else{
            $data["edit_data"]=$this->cquery->sel_spe_array_fields("var_value",array("var_vid"=>$id,"deletable"=>"1"),
            "var_value, var_vid,variable_value");
        }

        $this->load->view("admin/variable_values/edit", $data);
    }

    public function del_value($id){
        $this->cquery->upd_rec("var_value", "var_vid", $id, array("deletable"=>"0"));
        $data["message"]=$this->cfun->code_sending(200,"Value has deleted Successfully");
        $dd=$this->cquery->sel_spe_row_fields("var_value",array("var_vid"=>$id),"var_id");
        $this->view_variable($dd["var_id"],$data);
    }

    public function view_all_variables($data=""){
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Variables",
                "url"=>site_url("admin/settings/variables")
            )
        );

        $data["view_data"]=$this->cquery->sel_spe_array_fields("var_name",array("deletable"=>'1'),"var_id, var_title,
        var_description, created_date_time, editable");

        $this->load->view('admin/variables/index',$data);
    }
    public function view_variable($id,$data=""){

        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Variables",
                "url"=>site_url("admin/settings/variables")
            )
        );
        $dd=$this->cquery->sel_spe_row_fields("var_name",array("var_id"=>$id),"var_title");
        $data["var_title"]=$dd["var_title"];
        $data["view_data"]=$this->cquery->sel_spe_array_fields("var_value",array("deletable"=>'1',"var_id"=>$id),
            "var_vid, var_value,created_date_time,editable");
        $this->load->view('admin/variables/view',$data);
    }
    public function add_variable(){

        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Variables",
                "url"=>site_url("admin/settings/variables")
            )
        );

        if (isset($_POST["submit"])) {


            $data['var_name']  	= trim($this->input->post('var_name'));
            $data['var_title']  	= trim($this->input->post('var_title'));
            $data['var_description'] 	= trim($this->input->post('var_description'));
            $this->form_validation->set_rules("var_name", "Variable Name", "required|is_unique[var_name.var_name]");
            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "var_name" => $data["var_name"],
                    "var_title" => $data["var_title"],
                    "var_description" => $data["var_description"],
                    "created_date_time"=> date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("var_name", $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Variable Has Added Successfully");

            }
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form");
            }

        } else{

        }
        $this->load->view("admin/variables/add", $data);



    }


    public  function edit_variable($id){

        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Variables",
                "url"=>site_url("admin/settings/variables")
            )
        );


        if (isset($_POST["submit"])) {


            $data['var_name']  	= trim($this->input->post('var_name'));
            $data['var_title']  	= trim($this->input->post('var_title'));
            $data['var_description'] 	= trim($this->input->post('var_description'));
            $this->form_validation->set_rules("var_name", "Variable Name", "required");

            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "var_name" => $data["var_name"],
                    "var_title" => $data["var_title"],
                    "var_description" => $data["var_description"]
                );
                $this->cquery->upd_rec("var_name", "var_id", $id, $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Variable Data has Updated Successfully");
            } // end of form validation
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form");
            }

        }
        else {
            $data["edit_data"] = $this->cquery->sel_spe_rec("var_name", "var_id", $id);
        }
        $this->load->view("admin/variables/edit", $data);

    }

    public function del_variable($id){
       $this->cquery->upd_rec("var_name", "var_id", $id, array("deletable"=>"0"));
            $data["message"]=$this->cfun->code_sending(200,"Variable has deleted Successfully");
        $this->view_all_variables($data);
    }


    public function delete_variable($id){
        $this->cquery->del_rec("var_name", "var_id", $id);
        $data["message"]=$this->cfun->code_sending(200,"Variable has deleted Successfully Completely");
        $this->view_all_variables($data);
    }
    /**
     * @param mixed $data
     * default function of this class
     */
    public function view_branches($data="")
    {
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Branches",
                "url"=>site_url("admin/branches")
            )
        );

        $data["view_users"]=$this->cquery->sel_all_rec("branches");
        $this->load->view('admin/branches/index',$data);
    }



    public function view_branch($id){
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Branches",
                "url"=>site_url("admin/branches")
            )
        );
        $data["view_data"] = $this->cquery->sel_spe_rec("branches", "branch_id", $id);
        $this->load->view("admin/branches/view",$data);
    }

    /**
     * add question in the db. page will be loaded
     */
    public function add_branch(){
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Branches",
                "url"=>site_url("admin/branches")
            )
        );

       if (isset($_POST["submit"])) {


            $data['branch_name']  	= trim($this->input->post('branch_name'));
            $data['branch_phone'] 	= trim($this->input->post('branch_phone'));
            $data['branch_email'] 	= trim($this->input->post('branch_email'));
            $data['branch_cell'] 		= trim($this->input->post('branch_cell'));
            $data['branch_fax'] 	  = trim($this->input->post('branch_fax'));
            $data['branch_address'] 	  = trim($this->input->post('branch_address'));
            $data['branch_description'] 			= trim($this->input->post('branch_description'));

            $this->form_validation->set_rules("branch_name", "Branch Name", "required|is_unique[branches.branch_name]");
            $this->form_validation->set_rules("branch_email", "Branch Email", "valid_email");
            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "branch_name" => $data["branch_name"],
                    "branch_phone" => $data["branch_phone"],
                    "branch_cell" => $data["branch_cell"],
                    "branch_email" => $data["branch_email"],
                    "branch_address" => $data["branch_address"],
                    "branch_description" => $data["branch_description"],
                    "created_date_time"=> date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("branches", $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"User \"".$data["branch_name"]."\" Has Added
                Successfully");

            }
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form \"".$data["branch_name"]."\" ");
            }

        } else{

       }
        $this->load->view("admin/branches/add", $data);

    }




    public function del_branch($id)
    {
        if ($this->cquery->del_rec("branches", "branch_id", $id))
            $data["message"]=$this->cfun->code_sending(200,"Branch having Branch id <b> ".$id."</b> has deleted
            Successfully");
        $this->view_branches($data);
    }


    public function edit_branch($id){
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Branches",
                "url"=>site_url("admin/branches")
            )
        );

        $dd=$this->cquery->sel_spe_row_fields("branches",array("branch_id"=>$id), "branch_name");
        $branch_name=$dd["branch_name"];

        if (isset($_POST["submit"])) {


            $data['branch_name']  	= trim($this->input->post('branch_name'));
            $data['branch_phone'] 	= trim($this->input->post('branch_phone'));
            $data['branch_email'] 	= trim($this->input->post('branch_email'));
            $data['branch_cell'] 		= trim($this->input->post('branch_cell'));
            $data['branch_fax'] 	  = trim($this->input->post('branch_fax'));
            $data['branch_address'] 	  = trim($this->input->post('branch_address'));
            $data['branch_description'] 			= trim($this->input->post('branch_description'));
            $this->form_validation->set_rules("branch_email", "Branch Email", "valid_email");

            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "branch_name" => $data["branch_name"],
                    "branch_phone" => $data["branch_phone"],
                    "branch_cell" => $data["branch_cell"],
                    "branch_email" => $data["branch_email"],
                    "branch_address" => $data["branch_address"],
                    "branch_description" => $data["branch_description"]
                );
                $this->cquery->upd_rec("branches", "branch_id", $id, $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Branch  \"".$branch_name."\" Data has
                Updated Successfully");
            } // end of form validation
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form");
            }

        }
        else {
            $data["edit_data"] = $this->cquery->sel_spe_rec("branches", "branch_id", $id);
        }
        $this->load->view("admin/branches/edit", $data);

    }

    /**
     * add classes
     */
    public function add_class(){
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Classes",
                "url"=>site_url("admin/classes")
            )
        );

        $data["branch_id"]=$this->session->userdata("branch_id");
        if (isset($_POST["submit"])) {


            $data['class_name']  	= trim($this->input->post('class_name'));

            $this->form_validation->set_rules("class_name", "Class Name", "required|is_unique[classes.class_name]");

            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "class_name" => $data["class_name"],
                    "branch_id" => $data["branch_id"],
                    "created_date_time"=> date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("classes", $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Class \"".$data["class_name"]."\" Has Added
                Successfully");

            }
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validation Form is required");
            }

        } else{

        }
        $this->load->view("admin/classes/add", $data);

    }

    /**
     * edit class
     */
    public function edit_class($id){

        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Classes",
                "url"=>site_url("admin/classes")
            )
        );

        $dd=$this->cquery->sel_spe_row_fields("classes",array("class_id"=>$id), "class_name");
        $class_name=$dd["class_name"];

        if (isset($_POST["submit"])) {

            $data['class_name']  	= trim($this->input->post('class_name'));

            $this->form_validation->set_rules("class_name", "Class Name", "required");



            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "class_name" => $data["class_name"],
                );
                $this->cquery->upd_rec("classes", "class_id", $id, $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Class \"".$class_name."\" Has Changed
                Successfully");
            } // end of form validation
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form");
            }

        }
        else {
            $data["edit_data"] = $this->cquery->sel_spe_rec("classes", "class_id", $id);
        }
        $this->load->view("admin/classes/edit", $data);

    }

    public function del_class($id){

        if ($this->cquery->del_rec("classes", "class_id", $id))
            $data["message"]=$this->cfun->code_sending(200,"Class having class id <b> ".$id."</b> has deleted
            Successfully");
        $this->view_classes($data);
    }
    /**
     * @param $id class id
     * view single class. profile
     */


    public function view_classes($data=""){
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Classes",
                "url"=>site_url("admin/classes")
            )
        );
        $data["branch_id"]=$this->session->userdata("branch_id");
        $data["view_data"]=$this->cquery->sel_spe_array_fields("classes",array("deletable"=>"1","branch_id"=>$data["branch_id"]),"class_id, class_name,created_date_time,editable");
        $this->load->view('admin/classes/index',$data);

    }



    public function add_section(){
        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Sections",
                "url"=>site_url("admin/sections")
            )
        );
        $data["branch_id"]=$this->session->userdata("branch_id");
        $data["a_classes"]=$this->cquery->sel_spe_array_fields("classes",array("deletable"=>"1","branch_id"=>$data["branch_id"]),"class_id,class_name");

        if (isset($_POST["submit"])) {


            $data['section_name']  	= trim($this->input->post('section_name'));

            $data['class_id']  	= trim($this->input->post('class_id'));
            $this->form_validation->set_rules("section_name", "Section Name", "required|is_unique[sections.section_name]");
            $this->form_validation->set_rules("class_name", "Class ", "required");

            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "class_id" => $data["class_id"],
                    "branch_id" => $data["branch_id"],
                    "section_name" => $data["section_name"],
                    "created_date_time"=> date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("sections", $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Section \"".$data["section_name"]."\" Has Added
                Successfully");

            }
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validation Form is required");
            }

        } else{

        }
        $this->load->view("admin/sections/add", $data);

    }

    /**
     * edit class
     */
    public function edit_section($id){

        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Sections",
                "url"=>site_url("admin/sections")
            )
        );
        $data["branch_id"]=$this->session->userdata("branch_id");
        $data["a_classes"]=$this->cquery->sel_spe_array_fields("classes",array("deletable"=>"1","branch_id"=>$data["branch_id"]),"class_id,class_name");

        if (isset($_POST["submit"])) {
            $data['section_name']  	= trim($this->input->post('section_name'));
            $data['class_id']  	= trim($this->input->post('class_id'));
            $this->form_validation->set_rules("section_name", "Section Name", "required");

            if($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "class_id" => $data["class_id"],
                    "section_name" => $data["section_name"],
                );
                $this->cquery->upd_rec("sections", "section_id", $id, $ins_data);
                $data["message"]=$this->cfun->code_sending(200,"Section' Data Has Changed
                Successfully");
            } // end of form validation
            else{
                $data["message"]=$this->cfun->code_sending(100,"Validate Form");
            }

        }
        else {
            $data["edit_data"] = $this->cquery->sel_spe_array_fields("sections", array("section_id"=>$id,"branch_id"=>$data["branch_id"],"deletable"=>"1"),"class_id, section_name");
        }
        $this->load->view("admin/sections/edit", $data);

    }

    public function del_section($id){

        if ($this->cquery->del_rec("classes", "class_id", $id))
            $data["message"]=$this->cfun->code_sending(200,"Class having class id <b> ".$id."</b> has deleted
            Successfully");
        $this->view_classes($data);
    }
    /**
     * @param $id class id
     * view single class. profile
     */



    public function view_sections($data=""){

        $data["breadcrumb"]=array(
            array(
                "name"=>"Dashboard",
                "url"=>site_url("admin")
            ),
            array(
                "name"=>"Sections",
                "url"=>site_url("admin/sections")
            )
        );

        $data["view_data"]=$this->m_settings->view_sections();
        $this->load->view('admin/sections/index',$data);

    }

    public function add_staff_salary_setting(){
        $data["widget_title"]="Salary Setting";
        $data["form_legend_title"]="Staff Salary Setting";

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Salary Setting",
                "url" => site_url("admin/settings/salary")
            )
        );
        //initialize
        $data["branch_id"] = $this->session->userdata("branch_id");
        $data["a_deductions"] = $this->m_common->select_variable("staff_deduction_type");
        $data["a_allowances"] = $this->m_common->select_variable("staff_allowances_type");

        $data["a_salary_type"]=array_merge($data["a_allowances"], $data["a_deductions"]);
        if (isset($_POST["submit"])) {
            $data["staff_id"] = trim($this->input->post("staff_id"));
            $data["salary_type"] = count($this->input->post('salary_type')) ? $this->input->post('salary_type') : array();
            $data["salary_type_value"] = count($this->input->post('salary_type_value')) ? $this->input->post('salary_type_value') : array();

            $data["input_data"] = array();



            foreach ($data["salary_type"] as $rows) {
                array_push($data["input_data"], array("salary_type" => $rows, "salary_type_value" => ""));
            }
            foreach ($data["salary_type_value"] as $key => $value) {
                    $data["input_data"][$key]["salary_type_value"] = $value;
            }


            $this->form_validation->set_rules("staff_id", "Staff ID", "required");

            if ($this->form_validation->run() === TRUE) {

                foreach ($data["input_data"] as $rows) {
                    $ins_data = array(
                        'staff_and_designations' => $data["staff_id"],
                        'salary_type' => $rows["salary_type"],
                        'salary_type_value' => $rows["salary_type_value"],
                        'single_group' => "1",
                        'branch_id' => $data["branch_id"],
                        'created_date_time' => date('Y-m-d H:i:s')
                    );

                    $this->m_common->ins_rec("salary_setting", $ins_data);

                }

                $data["message"] = $this->m_functions->code_sending(200, "Salary Setting of staff has added successfully");


                $this->staff_salary_setting($data);


            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }


        } else{


            $this->load->view("admin/salary_setting/add_staff", $data);
        }

    }

    public  function edit_staff_salary_setting($id){

        $data["widget_title"]="Salary Setting";
        $data["form_legend_title"]="Staff Salary Setting";

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Staff Salary Setting",
                "url" => site_url("admin/settings/salary/staff")
            )
        );
        //initialize
        $data["branch_id"] = $this->session->userdata("branch_id");
        $data["a_deductions"] = $this->m_common->select_variable("staff_deduction_type");
        $data["a_allowances"] = $this->m_common->select_variable("staff_allowances_type");
        $data["a_salary_type"]=array_merge($data["a_allowances"], $data["a_deductions"]);
        $editable=$this->m_common->sel_spe_row_fields("salary_setting",array("ss_id"=>$id,"branch_id"=>$data["branch_id"],"deletable"=>"1"),"editable");


        if (isset($_POST["submit"])) {
            $data["staff_id"] = trim($this->input->post("staff_id"));
            $data["salary_type"] = $this->input->post('salary_type');
            $data["salary_type_value"] =$this->input->post('salary_type_value');




            $this->form_validation->set_rules("staff_id", "staff_id", "required");

            if ($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    'staff_and_designations' => $data["staff_id"],
                    'salary_type' => $data["salary_type"],
                    'salary_type_value' => $data["salary_type_value"]
                );

                $this->m_common->upd_rec("salary_setting", "ss_id",$id, $ins_data);
                $data["message"] = $this->m_functions->code_sending(200, "Salary Setting of specific Staff has updated successfully");



            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }


        } else{
            $data["edit_data"]= $this->m_common->sel_spe_array_fields("salary_setting",array("branch_id"=>$data["branch_id"],"editable"=>"1","deletable"=>"1","ss_id"=>$id),"staff_and_designations, salary_type, salary_type_value");

        }
        $this->load->view("admin/salary_setting/edit_staff", $data);



    }
    public function staff_salary_setting($data=""){
        $data["widget_title"]="Salary Setting";
        $data["form_legend_title"]="Staff Salary Setting";

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Salary Setting",
                "url" => site_url("admin/settings/salary")
            )
        );
        $data["view_data"]=$this->m_settings->view_staff_salary_settings();

        $this->load->view("admin/salary_setting/view_all_staff", $data);
    }


    public function delete_staff_salary_setting($id){
        if ($this->cquery->del_rec("salary_setting", "ss_id", $id))
            $data["message"]=$this->cfun->code_sending(200,"Salary setting for staff having id <b> ".$id."</b> has completely deleted
            Successfully");
        $this->staff_salary_setting($data);
    }


    public function add_group_salary_setting(){

        $data["widget_title"]="Salary Setting";
        $data["form_legend_title"]="Group Salary Setting";

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Salary Setting",
                "url" => site_url("admin/settings/salary")
            )
        );
        //initialize
        $data["branch_id"] = $this->session->userdata("branch_id");
        $data["a_deductions"] = $this->m_common->select_variable("staff_deduction_type");
        $data["a_allowances"] = $this->m_common->select_variable("staff_allowances_type");

        $data["a_salary_type"]=array_merge($data["a_allowances"], $data["a_deductions"]);
        if (isset($_POST["submit"])) {
            $data["designation"] = trim($this->input->post("designation"));
            $data["salary_type"] = count($this->input->post('salary_type')) ? $this->input->post('salary_type') : array();
            $data["salary_type_value"] = count($this->input->post('salary_type_value')) ? $this->input->post('salary_type_value') : array();

            $data["input_data"] = array();



            foreach ($data["salary_type"] as $rows) {
                array_push($data["input_data"], array("salary_type" => $rows, "salary_type_value" => ""));
            }
            foreach ($data["salary_type_value"] as $key => $value) {
                $data["input_data"][$key]["salary_type_value"] = $value;
            }


            $this->form_validation->set_rules("designation", "Designation", "required");

            if ($this->form_validation->run() === TRUE) {


                foreach ($data["input_data"] as $rows) {
                    $ins_data = array(
                        'staff_and_designations' => $data["designation"],
                        'salary_type' => $rows["salary_type"],
                        'salary_type_value' => $rows["salary_type_value"],
                        'single_group' => "2",
                        'branch_id' => $data["branch_id"],
                        'created_date_time' => date('Y-m-d H:i:s')
                    );

                    $this->m_common->ins_rec("salary_setting", $ins_data);

                }

                $data["message"] = $this->m_functions->code_sending(200, "Salary Setting of specific designation has added successfully");


                $this->group_salary_setting($data);


            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }


        } else{


            $this->load->view("admin/salary_setting/add_group", $data);
        }

    }
    public function edit_group_salary_setting($id){

        $data["widget_title"]="Salary Setting";
        $data["form_legend_title"]="Group Salary Setting";

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Group Salary Setting",
                "url" => site_url("admin/settings/salary/group")
            )
        );
        //initialize
        $data["branch_id"] = $this->session->userdata("branch_id");
        $data["a_deductions"] = $this->m_common->select_variable("staff_deduction_type");
        $data["a_allowances"] = $this->m_common->select_variable("staff_allowances_type");
        $data["a_salary_type"]=array_merge($data["a_allowances"], $data["a_deductions"]);
        if (isset($_POST["submit"])) {
            $data["designation"] = trim($this->input->post("designation"));
            $data["salary_type"] = $this->input->post('salary_type');
            $data["salary_type_value"] =$this->input->post('salary_type_value');




            $this->form_validation->set_rules("designation", "Designation", "required");

            if ($this->form_validation->run() === TRUE) {
                    $ins_data = array(
                        'staff_and_designations' => $data["designation"],
                        'salary_type' => $data["salary_type"],
                        'salary_type_value' => $data["salary_type_value"]
                    );

                    $this->m_common->upd_rec("salary_setting", "ss_id",$id, $ins_data);



                $data["message"] = $this->m_functions->code_sending(200, "Salary Setting of specific designation has updated successfully");





            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }


        } else{
           $data["edit_data"]= $this->m_common->sel_spe_array_fields("salary_setting",array("branch_id"=>$data["branch_id"],"editable"=>"1","deletable"=>"1","ss_id"=>$id),"staff_and_designations, salary_type, salary_type_value");

        }
        $this->load->view("admin/salary_setting/edit_group", $data);


    }

    public function group_salary_setting($data=""){

        $data["widget_title"]="Salary Setting";
        $data["form_legend_title"]="Staff Salary Setting";

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Salary Setting",
                "url" => site_url("admin/settings/salary")
            )
        );
        $data["view_data"]=$this->m_settings->view_group_salary_settings();
        $this->load->view("admin/salary_setting/view_all_groups", $data);

    }

    public function delete_group_salary_setting($id){
        if ($this->cquery->del_rec("salary_setting", "ss_id", $id))
            $data["message"]=$this->cfun->code_sending(200,"Salary setting for designation having id <b> ".$id."</b> has completely deleted
            Successfully");
        $this->group_salary_setting($data);
    }
}
