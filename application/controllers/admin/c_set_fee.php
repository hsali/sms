<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_set_fee extends CI_Controller
{


    public function __construct()
    {
        parent::__construct(); // if it is not called. then below model will not work.
        $this->m_functions->login_control();
        $this->load->model("m_common", "cquery");
        $this->load->model("m_functions", "cfun");
    }

    /**
     * @param string $data
     * page will show the setting of page..
     * @todo make it in future
     */
    public function index($data = "")
    {

    }

    /**
     * @param mixed $data
     * default function of
     */
    public function view_all_program_fee($data = "")
    {
        $data["widget_title"]="Program Fee Management";

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),

            array(
                "name" => "Program Fee",
                "url" => site_url("admin/settings/program/fee")
            )
        );

        $data["view_data"] = $this->m_fee->view_program_fee();
        $this->load->view('admin/program_fee/index', $data);
    }

    public function add_program_fee()
    {
        $data["widget_title"]="Program Fee Management";
        $data["form_legend_title"]="Add Program Fee";

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Program Fee",
                "url" => site_url("admin/settings/program/fee")
            )
        );
        $data["branch_id"]=$this->session->userdata("branch_id");
        $dd=$this->cquery->sel_spe_row_fields("var_name",array("var_name"=>"fee_category", "deletable"=>"1"),"var_id");
        $data["a_fee_category"]=$this->cquery->sel_spe_array_fields("var_value",array("var_id"=>$dd["var_id"],
        "deletable"=>"1"),"var_vid, var_value");
        $data["a_classes"]=$this->cquery->sel_spe_array_fields("classes",array("deletable"=>"1","branch_id"=>$data["branch_id"]),"class_id,class_name");
        if (isset($_POST["submit"])) {


            $data['fc_id'] = trim($this->input->post('fc_id'));
            $data['class_id'] = trim($this->input->post('class_id'));
            $data['fee'] = trim($this->input->post('fee'));
            $data['period'] = trim($this->input->post('period'));

            $this->form_validation->set_rules("fc_id", "Fee Category", "required");
            $this->form_validation->set_rules("class_id", "Class", "required");
            $this->form_validation->set_rules("fee", "Fee", "required");
            $this->form_validation->set_rules("period", "Period", "required");
            if ($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "fc_id" => $data["fc_id"],
                    "branch_id" => $data["branch_id"],
                    "class_id" => $data["class_id"],
                    "fee" => $data["fee"],
                    "period" => $data["period"],
                    "created_date_time" => date('Y-m-d H:i:s')
                );
                $this->cquery->ins_rec("set_prog_fee", $ins_data);
                $data["message"] = $this->cfun->code_sending(200, "Program Fee Has Added Successfully");

            } else {
                $data["message"] = $this->cfun->code_sending(100, "Validate Form");
            }

        } else {

        }
        $this->load->view("admin/program_fee/add", $data);

    }

    public function edit_program_fee($id)
    {
        $data["widget_title"]="Program Fee Management";
        $data["form_legend_title"]="Edit Program Fee";

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Program Fee",
                "url" => site_url("admin/settings/program/fee")
            )
        );
        $data["branch_id"]=$this->session->userdata("branch_id");
        $dd=$this->cquery->sel_spe_row_fields("var_name",array("var_name"=>"fee_category", "deletable"=>"1"),"var_id");
        $data["a_fee_category"]=$this->cquery->sel_spe_array_fields("var_value",array("var_id"=>$dd["var_id"],
            "deletable"=>"1"),"var_vid, var_value");
        $data["a_classes"]=$this->cquery->sel_spe_array_fields("classes",array("deletable"=>"1","branch_id"=>$data["branch_id"]),"class_id,class_name");

        if (isset($_POST["submit"])) {

            $data['fc_id'] = trim($this->input->post('fc_id'));
            $data['class_id'] = trim($this->input->post('class_id'));
            $data['fee'] = trim($this->input->post('fee'));
            $data['period'] = trim($this->input->post('period'));

            $this->form_validation->set_rules("fc_id", "Fee Category", "required");
            $this->form_validation->set_rules("class_id", "Class", "required");
            $this->form_validation->set_rules("fee", "Fee", "required");
            $this->form_validation->set_rules("period", "Period", "required");
            if ($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "fc_id" => $data["fc_id"],
                    "class_id" => $data["class_id"],
                    "fee" => $data["fee"],
                    "period" => $data["period"],
                );
                $this->cquery->upd_rec("set_prog_fee", "pf_id", $id, $ins_data);
                $data["message"] = $this->cfun->code_sending(200, "Program Fee' Data has
                Updated Successfully");
            } // end of form validation
            else {
                $data["message"] = $this->cfun->code_sending(100, "Validate Form");
            }

        } else {
            $data["edit_data"] = $this->cquery->sel_spe_array_fields("set_prog_fee", array("deletable"=>"1","branch_id"=>$data["branch_id"],"editable"=>"1","pf_id"=>$id),"fc_id, class_id,period,fee");
        }
        $this->load->view("admin/program_fee/edit", $data);

    }

    public function del_program_fee($id)
    {
        $ins_data=array(
            "deletable"=>"0"
        );
        if ($this->cquery->upd_rec("set_prog_fee", "pf_id", $id,$ins_data))
            $data["message"] = $this->cfun->code_sending(200, "Program Fee has deleted
            Successfully");
        $this->view_branches($data);
    }
    public function delete_program_fee($id)
    {
        if ($this->cquery->del_rec("set_prog_fee", "pf_id", $id))
            $data["message"] = $this->cfun->code_sending(200, "Program fee has completely deleted
            Successfully");
        $this->view_branches($data);
    }


}