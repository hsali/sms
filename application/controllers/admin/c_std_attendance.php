<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by IntelliJ IDEA.
 * User: shehbaz
 * Date: 6/28/15
 * Time: 7:02 PM
 */

class C_std_attendance extends CI_Controller
{


    public function __construct()
    {
        parent::__construct(); // if it is not called. then below model will not work.
        $this->m_functions->login_control();
    }

    /**
     * @param string $data
     * page will show the setting of page..
     * @todo make it in future
     */
    public function index($data = "")
    {

    }

    /**
     * @param mixed $data
     * default function of
     */
    public function view_section_attendance($data = "")
    {

        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Attendance",
                "url" => site_url("admin/attendance/sections")
            )
        );

        $data["view_data"] = $this->m_attendance->view_section_attendance();

        $this->load->view('admin/section_attendance/index', $data);
    }

    public function add_section_attendance()
    {
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Attendance",
                "url" => site_url("admin/attendance/sections")
            ),
        );

        $data["a_branches"]=$this->m_common->sel_spe_array_fields("branches",array("deletable"=>"1"),"branch_id,branch_name");
        $data["a_classes"]=$this->m_common->sel_spe_array_fields("classes",array("deletable"=>"1"),"class_id,class_name");
        $data["a_subjects"]=$this->m_common->sel_spe_array_fields("var_value",array("deletable"=>"1","var_id"=>"9"),"var_vid,
        var_value");
        $data["a_sections"]=$this->m_common->sel_spe_array_fields("sections",array("deletable"=>"1"),"section_id,section_name");
        if (isset($_POST["submit"])) {
            $data['class_id'] = trim($this->input->post('class_id'));
            $data['section_id'] = trim($this->input->post('section_id'));
            $data['subject_id'] = trim($this->input->post('subject_id'));
            $data['total_present'] = trim($this->input->post('total_present'));
            $data['total_absent'] = trim($this->input->post('total_absent'));
            $data['total_leave'] = trim($this->input->post('total_leave'));
            $data['total_late'] = trim($this->input->post('total_late'));
            $data['attendance_date'] = $this->input->post('attendance_date');
            $data['branch_id'] = $this->session->userdata("branch_id");
//            $data["ad"]=preg_replace('#(\d{2})/(\d{2})/(\d{4})\s(.*)#', '$3-$2-$1 $4', $data["attendance_date"]);
//            $data["ad"]=date('Y-m-d', strtotime(str_replace('-', '/', $data["attendance_date"])));
//            var_dump($data);exit;
            $this->form_validation->set_rules("section_id", "Section Id", "required");
            $this->form_validation->set_rules("subject_id", "Subject id", "required");
            $this->form_validation->set_rules("total_present", "Total Present", "required");

            if ($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "section_id" => $data["section_id"],
                    "class_id" => $data["class_id"],
                    "subject_id" => $data["subject_id"],
                    "attendance_date" => $data["attendance_date"],
                    "branch_id" => $data["branch_id"],
                    "total_absent" => $data["total_absent"],
                    "total_present" => $data["total_present"],
                    "total_late" => $data["total_late"],
                    "total_leave" => $data["total_leave"],
                    "created_date_time" => date('Y-m-d H:i:s')
                );
                $this->m_common->ins_rec("section_attendance", $ins_data);
                $data["message"] = $this->m_functions->code_sending(200, "Attendance of Section has added Successfully");

            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }

        } else {

        }
        $this->load->view("admin/section_attendance/add", $data);

    }
    public function edit_section_attendance($id="")
    {
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Attendance",
                "url" => site_url("admin/attendance/sections")
            ),
        );

        $data["a_classes"] = $this->m_common->sel_spe_array_fields("classes", array("deletable" => "1"), "class_id,class_name");
        $data["a_subjects"]=$this->m_common->sel_spe_array_fields("var_value",array("deletable"=>"1","var_id"=>"9"),"var_vid,
        var_value");
        $data["a_sections"]=$this->m_common->sel_spe_array_fields("sections",array("deletable"=>"1"),"section_id,section_name");
        if (isset($_POST["submit"])) {
            $data['class_id'] = trim($this->input->post('class_id'));
            $data['section_id'] = trim($this->input->post('section_id'));
            $data['subject_id'] = trim($this->input->post('subject_id'));
            $data['total_present'] = trim($this->input->post('total_present'));
            $data['total_absent'] = trim($this->input->post('total_absent'));
            $data['total_leave'] = trim($this->input->post('total_leave'));
            $data['total_late'] = trim($this->input->post('total_late'));
            $data['attendance_date'] = $this->input->post('attendance_date');
            $data['branch_id'] = $this->session->userdata("branch_id");

//            $data["ad"]=date('Y-m-d', strtotime(str_replace('-', '/', $data["attendance_date"]));
            $this->form_validation->set_rules("section_id", "Section Id", "required");
            $this->form_validation->set_rules("subject_id", "Subject id", "required");
            $this->form_validation->set_rules("total_present", "Total Present", "required");

            if ($this->form_validation->run() === TRUE) {
                $ins_data = array(
                    "section_id" => $data["section_id"],
                    "class_id" => $data["class_id"],
                    "subject_id" => $data["subject_id"],
                    "attendance_date" => $data["attendance_date"],
                    "total_absent" => $data["total_absent"],
                    "total_present" => $data["total_present"],
                    "total_late" => $data["total_late"],
                    "total_leave" => $data["total_leave"]
                );
                $this->m_common->upd_rec("section_attendance","sa_id",$id, $ins_data);
                $data["message"] = $this->m_functions->code_sending(200, "Attendance of Section has Updated Successfully");

            } else {
                $data["message"] = $this->m_functions->code_sending(100, "Validate Form");
            }

        } else {
            $data["view_data"]=$this->m_common->sel_spe_rec("section_attendance","sa_id",$id);
//            var_dump($data);exit;

        }
        $this->load->view("admin/section_attendance/edit", $data);

    }

    public function view_single_section_attendance($id)
    {
        $data["breadcrumb"] = array(
            array(
                "name" => "Dashboard",
                "url" => site_url("admin")
            ),
            array(
                "name" => "Attendance",
                "url" => site_url("admin/attendance/sections")
            ),
        );

        $data["a_branches"]=$this->m_common->sel_spe_array_fields("branches",array("deletable"=>"1"),"branch_id,branch_name");
        $data["a_classes"]=$this->m_common->sel_spe_array_fields("classes",array("deletable"=>"1"),"class_id,class_name");
        $data["a_subjects"]=$this->m_common->sel_spe_array_fields("var_value",array("deletable"=>"1","var_id"=>"9"),"var_vid,
        var_value");
        $data["a_sections"]=$this->m_common->sel_spe_array_fields("sections",array("deletable"=>"1"),"section_id,section_name");

        $data["view_data"] = $this->m_attendance->view_section_attendance_by_id($id);
        $data["id"] = $id;

        $this->load->view("admin/section_attendance/view_section_attendance", $data);
    }

    public function del_section_attendance($id)
    {
        $this->m_common->del_rec("section_attendance", "sa_id", $id);
        $data["message"] = $this->m_functions->code_sending(200, "Variable has deleted Successfully Completely");
        $this->view_section_attendance($data);
    }


}