<?php
//$a_branch=$this->config->item('branches');
$a_domicile = $this->config->item('domicile');

$a_religion = $this->config->item('religion');
$a_country = $this->config->item('country');
$a_city = $this->config->item('city');
$a_guardian_type = $this->config->item('guardian_type');
$a_marital_status = $this->config->item('a_marital_status');
$a_guardian_status = $this->config->item('guardian_status');
$a_degree_types = $a_guardian_education = $this->config->item('degree_type');

?>
<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>



            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-145"
                         data-widget-editbutton="false"
                         role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                    
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse">-->
                                <!--                                    <i class="fa fa-angle-down "></i>-->
                                <!--                                </a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                    <i class="fa fa-expand "></i>
                                </a>
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete">-->
                                <!--                                    <i class="fa fa-times"></i>-->
                                <!--                                </a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>

                            <h2><?php if (isset($widget_title)) echo $widget_title; else echo "Staff Management"; ?></h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal" method="post" enctype="multipart/form-data" >

                                    <fieldset>
                                        <legend>Personal Information</legend>
                                        </br>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("first_name")) echo "has-error";
                                                    elseif (!empty($first_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">First Name</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="first_name" name="first_name"
                                                               placeholder="Type First Name" type="text"
                                                               value="<?php if (isset($first_name)) echo $first_name; ?>">
                                                  <span class="help-Block"><?php echo form_error("first_name");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Last Name-->
                                                <div class="form-group <?php if (form_error("last_name")) echo "has-error";
                                                    elseif (!empty($last_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Last Name</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="last_name" name="last_name"
                                                               placeholder="Type Last Name" type="text"
                                                               value="<?php if (isset($last_name)) echo $last_name; ?>">
                                               <span class="help-Block"><?php echo form_error("last_name");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("marital_status")) echo "has-error"; elseif (!empty($marital_status)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Marital Status </label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="marital_status"
                                                                name="marital_status">
                                                            <option value="" selected="" disabled="">
                                                                Select Marital Status
                                                            </option>
                                                            <?php foreach ($a_marital_status as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($marital_status) && $marital_status == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                         <span
                                                             class="help-Block"><?php echo form_error("marital_status");
                                                             ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("spouse_type")) echo "has-error";
                                                    elseif (!empty($spouse_type)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Relation Type</label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="spouse_type"
                                                                name="spouse_type">
                                                            <option value="" selected="" disabled="">
                                                                Select Spouse Type
                                                            </option>
                                                            <?php foreach ($a_spouse_type as $rows) {
                                                                $key = $rows["var_vid"];
                                                                $value = $rows["var_value"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($spouse_type) && $spouse_type == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span
                                                            class="help-Block"><?php echo form_error("spouse_type"); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("spouse_name")) echo "has-error";
                                                elseif (!empty($spouse_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Relation Name</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="spouse_name" name="spouse_name"
                                                               placeholder="Type Spouse' Name" type="text"
                                                               value="<?php if (isset($spouse_name)) echo $spouse_name; ?>">
                                               <span class="help-Block"><?php echo form_error("spouse_name");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("cnic")) echo "has-error";
                                                elseif (!empty($cnic)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">CNIC No</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="cnic" name="cnic"
                                                               placeholder="Type CNIC Number" type="text"
                                                               value="<?php if (isset($cnic)) echo $cnic; ?>">
                                                  <span class="help-Block"><?php echo form_error("cnic");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Last Name-->
                                                <div class="form-group <?php if (form_error("domicile")) echo
                                                "has-error";
                                                elseif (!empty($domicile)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Domicile</label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="domicile" name="domicile">
                                                            <option value="" selected="" disabled="">Select
                                                                Domicile
                                                            </option>
                                                            <?php foreach ($a_domicile as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($domicile) && $domicile == $key) {
                                                                    echo
                                                                    'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                               <span class="help-Block"><?php echo form_error("domicile");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("gender")) echo "has-error";
                                                elseif (!empty($gender)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Gender</label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="gender" name="gender">
                                                            <option value="" selected="" disabled="">Select Gender
                                                            </option>
                                                            <?php foreach ($a_gender as $rows) {
                                                                $key = $rows["var_vid"];
                                                                $value = $rows["var_value"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($gender) && $gender == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                  <span class="help-Block"><?php echo form_error("gender");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("dob")) echo "has-error"; elseif (!empty($dob)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Date of Birth</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                            <input class="form-control" placeholder="Choose Date of Birth" id="dob" name="dob" type="text" value="<?php if (isset($dob)) echo $dob; ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <span class="help-Block"><?php echo form_error("dob");
                                                            ?></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("phone_no")) echo "has-error";
                                                elseif (!empty($phone_no)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Phone No</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="phone_no" name="phone_no" type="text" placeholder="Type Phone No" value="<?php if (isset($phone_no)) echo $phone_no; ?>">
                                                  <span class="help-Block"><?php echo form_error("phone_no");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("religion")) echo "has-error";
                                                    elseif (!empty($religion)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Religion</label>

                                                    <div class="col-md-8">

                                                        <select class="form-control" id="religion" name="religion">
                                                            <option value="" selected="" disabled="">
                                                                Select Religion
                                                            </option>
                                                            <?php foreach ($a_religion as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($religion) && $religion == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                  <span class="help-Block"><?php echo form_error("religion");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <!--email and cell no -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("cell_no")) echo "has-error"; elseif (!empty($cell_no)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Cell No</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="cell_no" name="cell_no" placeholder="Type Cell No" type="text" value="<?php if (isset($cell_no)) echo $cell_no; ?>">
                                                  <span class="help-Block"><?php echo form_error("cell_no");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("email")) echo
                                                "has-error";
                                                elseif (!empty($email)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Email</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="email" name="email" type="text"
                                                               placeholder="Type Email"
                                                               value="<?php if (isset($email)) echo $email; ?>">
                                                  <span class="help-Block"><?php echo form_error("email");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--country and city -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("country")) echo "has-error";
                                                    elseif (!empty($country)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Country </label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="country" name="country">
                                                            <option value="" selected="" disabled="">Select Country
                                                            </option>
                                                            <?php foreach ($a_country as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($country) && $country == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("country");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("city")) echo "has-error";
                                                elseif (!empty($city)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">City </label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="city" name="city">
                                                            <option value="" selected="" disabled="">
                                                                Select City
                                                            </option>
                                                            <?php foreach ($a_city as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($city) && $city == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                         <span class="help-Block"><?php echo form_error("city");
                                                             ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("other_city")) echo
                                                "has-error";
                                                elseif (!empty($other_city)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Other City </label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="other_city" name="other_city"
                                                               placeholder="Type City Name"
                                                               type="text" value="<?php
                                                        if (isset($other_city))
                                                            echo $other_city; ?>">
                                                  <span class="help-Block"><?php echo form_error("other_city");
                                                      ?>Note: If city selected is "Other".</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("town")) echo "has-error";
                                                elseif (!empty($town)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Town </label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="town" name="town" type="text"
                                                               placeholder="Type Town name"
                                                               value="<?php if (isset($town)) echo $town; ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("town"); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--                                        permanent address -->

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group <?php if (form_error("permanent_address")) echo
                                                "has-error";
                                                elseif (!empty($permanent_address)) echo "has-success" ?>">
                                                    <label class="col-md-2 control-label">Permanent Address</label>

                                                    <div class="col-md-10">
                                                <textarea id="permanent_address" name="permanent_address"
                                                          class="form-control"
                                                          placeholder="Type Address House No# , Street # , Town,City, Postal Code, Country"
                                                          rows="3"><?php if (isset($permanent_address)) echo
                                                    $permanent_address; ?></textarea>
                                                  <span class="help-Block"><?php echo form_error("permanent_address");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--                                        present address -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group <?php if (form_error("present_address")) echo
                                                "has-error";
                                                elseif (!empty($present_address)) echo "has-success" ?>">
                                                    <label class="col-md-2 control-label">Present Address</label>

                                                    <div class="col-md-10">
                                                <textarea id="present_address" name="present_address"
                                                          class="form-control"
                                                          placeholder="Type Address House No# , Street # , Town,City, Postal Code, Country"
                                                          rows="3"><?php if (isset($present_address)) echo
                                                    $present_address; ?></textarea>
                                                  <span class="help-Block"><?php echo form_error("present_address");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset id="academic_informatin">
                                        <legend>Academic Information </legend>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group <?php if (form_error("a_institute"))
                                                    echo "has-error";
                                                elseif (!empty($a_institute)) echo "has-success" ?>">
                                                    <label class="col-md-2 control-label">Institute Name
                                                    </label>

                                                    <div class="col-md-10">
                                                        <input class="form-control" id="a_institute"
                                                               name="a_institute"
                                                               placeholder="Type Institute Name" type="text"
                                                               value="<?php if (isset($a_institute)) echo $a_institute; ?>">
                                               <span class="help-Block"><?php echo form_error("a_institute");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("a_degree_type"))
                                                    echo "has-error"; elseif (!empty($a_degree_type)) echo
                                                "has-success" ?>">
                                                    <label class="col-md-4 control-label">Degree Type </label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="a_degree_type"
                                                                name="a_degree_type">
                                                            <option value="" selected="" disabled="">
                                                                Select Degree
                                                            </option>
                                                            <?php foreach ($a_degree_types as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($a_degree_type) && $a_degree_type == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                  <span class="help-Block"><?php echo form_error("a_degree_type");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Class Name-->
                                                <div class="form-group <?php if (form_error("a_class_name")) echo
                                                "has-error"; elseif (!empty($a_class_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Class Name</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="a_class_name"
                                                               name="a_class_name"
                                                               placeholder="Type Class Name" type="text"
                                                               value="<?php if (isset($a_class_name))
                                                                   echo $a_class_name; ?>">
                                               <span class="help-Block"><?php echo form_error("a_class_name");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("a_obtained_marks"))
                                                    echo "has-error"; elseif (!empty($a_obtained_marks)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Obtained Marks</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="a_obtained_marks"
                                                               name="a_obtained_marks"
                                                               placeholder="Type Obtained Marks" type="text"
                                                               value="<?php if (isset($a_obtained_marks)) echo
                                                               $a_obtained_marks; ?>">
                                               <span class="help-Block"><?php echo form_error("a_obtained_marks");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Class Name-->
                                                <div class="form-group <?php if (form_error("a_total_marks")) echo
                                                "has-error"; elseif (!empty($a_total_marks)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Total Marks</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="a_total_marks"
                                                               name="a_total_marks"
                                                               placeholder="Type Total Marks" type="text"
                                                               value="<?php if (isset($a_total_marks)) echo
                                                               $a_total_marks; ?>">
                                               <span class="help-Block"><?php echo form_error("a_total_marks");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("a_starting_date")) echo "has-error"; elseif (!empty($a_starting_date)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Starting Date</label>

                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                            <input class="form-control" id="a_starting_date" name="a_starting_date" type="text" placeholder="Choose Starting Date" value="<?php if (isset($a_starting_date)) echo $a_starting_date;?>">
                                                             <span class="input-group-addon">
                                                                 <span class="glyphicon glyphicon-calendar"></span>
                                                             </span>
                                                            <span class="help-Block"><?php echo form_error("a_starting_date");
                                                                ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Class Name-->
                                                <div class="form-group <?php if (form_error("a_ending_date"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($a_ending_date))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Ending Date</label>

                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                            <input class="form-control" id="a_ending_date" name="a_ending_date" type="text" placeholder="Choose  Ending Date" value="<?php if (isset($a_ending_date)) echo $a_ending_date;?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                            <span class="help-Block"><?php echo form_error("a_ending_date");?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </fieldset>
                                    <fieldset id="academic_informatin">
                                        <legend>Experience Information </legend>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group <?php if (form_error("organization_name"))
                                                    echo "has-error";
                                                elseif (!empty($organization_name)) echo "has-success" ?>">
                                                    <label class="col-md-2 control-label">Organization Name
                                                    </label>

                                                    <div class="col-md-10">
                                                        <input class="form-control" id="organization_name"
                                                               name="organization_name"
                                                               placeholder="Type Organization Name" type="text"
                                                               value="<?php if (isset($organization_name)) echo $organization_name; ?>">
                                               <span class="help-Block"><?php echo form_error("organization_name");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("experience_type"))
                                                    echo "has-error"; elseif (!empty($experience_type)) echo
                                                "has-success" ?>">
                                                    <label class="col-md-4 control-label">Experience Type </label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="experience_type"
                                                                name="experience_type">
                                                            <option value="" selected="" disabled="">
                                                                Select Experience Type
                                                            </option>
                                                            <?php foreach ($a_experience_type as $rows) {
                                                                $key=$rows["var_vid"];
                                                                $value=$rows["var_value"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($experience_type) && $experience_type == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                  <span class="help-Block"><?php echo form_error("experience_type");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Class Name-->
                                                <div class="form-group <?php if (form_error("designation_name")) echo
                                                "has-error"; elseif (!empty($designation_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Designation Name</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="designation_name"
                                                               name="designation_name"
                                                               placeholder="Type Designation Name" type="text"
                                                               value="<?php if (isset($designation_name))
                                                                   echo $designation_name; ?>">
                                               <span class="help-Block"><?php echo form_error("designation_name");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group <?php if (form_error("salary"))
                                                    echo "has-error"; elseif (!empty($salary)) echo "has-success" ?>">
                                                    <label class="col-md-2 control-label">Salary</label>

                                                    <div class="col-md-10">
                                                        <input class="form-control" id="salary" name="salary" placeholder="Type Salary" type="text"  value="<?php   if(isset($salary)){
                                                            echo $salary;
                                                        }
                                                        ?>">
                                               <span class="help-Block"><?php echo form_error("salary");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("starting_date")) echo "has-error"; elseif (!empty($starting_date)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Starting Date</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                        <input class="form-control" id="starting_date" name="starting_date" type="text" placeholder="Choose Starting Date" value="<?php if (isset($starting_date)) echo $starting_date; ?>">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                            <span class="help-Block"><?php echo form_error("starting_date");?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Ending date -->
                                                <div class="form-group <?php if (form_error("ending_date")) echo "has-error"; elseif (!empty($ending_date)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Ending Date</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                        <input class="form-control" id="ending_date" name="ending_date" type="text" placeholder="Choose Ending Date" value="<?php if (isset($ending_date)) echo $ending_date; ?>">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                            <span class="help-Block"><?php echo form_error("ending_date");?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </fieldset>





                                    <fieldset id="enrollment_info">
                                        <legend>Job Information</legend>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("staff_designation"))
                                                    echo "has-error"; elseif (!empty($staff_designation)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Staff Designation</label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="staff_designation"
                                                                name="staff_designation">
                                                            <option value="" selected="" disabled="">
                                                                Select Staff Designation
                                                            </option>
                                                            <?php foreach ($a_staff_designation as $rows) {
                                                                $key=$rows["var_vid"];
                                                                $value=$rows["var_value"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($staff_designation) && $staff_designation == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                               <span class="help-Block"><?php echo form_error("staff_designation");
                                                   ?></span>
                                                    </div>
                                                </div>
                                                </div>

                                            <div class="col-md-6">

                                                <!--Date of Registration-->
                                                <div class="form-group <?php if (form_error("doa")) echo "has-error"; elseif (!empty($doa)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Date Of Appointment</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                        <input class="form-control" id="doa" name="doa" type="text" placeholder="Choose The appointment Date" value="<?php if (isset($doa)) echo $doa; ?>">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                            <span class="help-Block"><?php echo form_error("doa");?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                            <div class="form-group <?php if (form_error("job_type"))
                                                echo "has-error"; elseif (!empty($job_type)) echo "has-success" ?>">
                                                <label class="col-md-4 control-label">Job Type</label>

                                                <div class="col-md-8">
                                                    <select class="form-control" id="job_type" name="job_type">
                                                        <option value="" selected="" disabled="">
                                                            Select Job Type
                                                        </option>
                                                        <?php foreach ($a_job_type as $rows) {
                                                            $key=$rows["var_vid"];
                                                            $value=$rows["var_value"];
                                                            echo "<option label='{$value}' value='{$key}'";
                                                            if (isset($job_type) && $job_type == $key) {
                                                                echo 'selected="selected"';
                                                            }
                                                            echo ">" . $value . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                               <span class="help-Block"><?php echo form_error("job_type");
                                                   ?></span>
                                                </div>
                                            </div>
                                        </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php if(form_error("basic_salary")) echo "has-error"; elseif (!empty($basic_salary)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Basic Salary</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="basic_salary" name="basic_salary" placeholder="Type Basic Salary" type="text"  value="<?php   if(isset($basic_salary)){
                                                            echo $basic_salary;
                                                        }
                                                        ?>">

                                                  <span class="help-Block"><?php echo form_error("basic_salary");
                                                      ?></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group <?php if (form_error("comments")) echo "has-error"; elseif (!empty($comments)) echo "has-success" ?>">
                                                    <label class="col-md-2 control-label">Comments</label>

                                                    <div class="col-md-10">
                                                <textarea id="comments" name="comments" class="form-control" placeholder="Type Any Comments about this staff here" rows="3"><?php if (isset($comments))
                                                        echo $comments; ?></textarea>
                                                  <span class="help-Block"><?php echo form_error("comments");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset
                                                </button>
                                                <button type="submit" name="submit" id="submit" class="btn
                                              btn-success btn-lg">Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
