<html>
<head>
    <title>EMS: Print the Admission Form</title>
    <style>

        .formp-print{
            /*size: 7in 9.25in;*/
            width: 7in;
            height: 9.25in;
            margin: 1in;
            border:0.010in black solid;
            background-color: #e5e5e5;
        }
        .formp-header{
            width: 7in;
            height: 0.5in;
            border-radius: 10px;
            background-color: #00ff00;
        }

        .formp-footer{
            display: block;
            background-color: #008DC4;
            width:7in;
            height: 1.5in;
            border-width:2px;
            border-style: solid;
            border-color: gray;
            border-radius: 5px;
            position: absolute;
            top:7.75in;
        }
        p.field{
            display: inline;
            font-size:14px;
            margin-left:3%;
            margin-right:3%;

        }

        p.value{
            display: inline;
            font-size: 13px;
            text-decoration-line: underline;
            margin-left: 3%;
            margin-right:3%;
        }
        p.field + p.value{
            margin-left:5px;
        }

        .profile-pic{
            widht:150px;
            height:150px;

            border-style: solid;
            border-color: gainsboro;
            display: inline;
            border-radius: 100%;
        }
        .img-circle{
            border-radius: 100%;
        }
        .row{
            display:block;
            widht: 100%;
        }
        .col-4{
            display: inline;
            width:33%;
            height: auto;
        }
        div{
            display: inherit;
        }
        .col-8{
            display:inline;
            width: 66%;
        }

        .row-inline{
            display :inline;
        }
        .row-block{
            display:block;
        }
        .col-6{
            widht:50%;
            display:inline;
        }
        div.row div.row {
            /*display:inline;*/
        }
        div .up-pos{
            position: relative;
            top:5px;

        }
        th{
            width:15%;
            border: 2px  #AAAAAA solid;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
<?php
$a_domicile = $this->config->item('domicile');
$a_gender = $this->config->item('gender');
$a_religion = $this->config->item('religion');
$a_country = $this->config->item('country');
$a_city = $this->config->item('city');
$a_guardian_type = $this->config->item('guardian_type');
$a_marital_status = $this->config->item('a_marital_status');
$a_guardian_status = $this->config->item('guardian_status');
$a_degree_types = $a_guardian_education = $this->config->item('degree_type');
?>
<?php
if(isset($data_pi)) {
    foreach ($data_pi as $row) {
        $staff_id = $row->staff_id;
        $cr_first_name = $row->first_name;
        $cr_last_name = $row->last_name;
        $cr_spouse_type = $row->spouse_type;
        $cr_spouse_name = $row->spouse_name;
        $cr_gender = $row->gender;
        $cr_dob = $row->dob;
        $cr_cnic = $row->cnic;
        $cr_religion = $row->religion;
        $cr_domicile = $row->domicile;
        $cr_marital_status = $row->marital_status;
        $cr_phone_no = $row->phone_no;
        $cr_email = $row->email;
        $cr_cell_no = $row->cell_no;
        $cr_country = $row->country;
        $cr_city = $row->city;
        $cr_other_city = $row->other_city;
        $cr_town = $row->town;
        $cr_permanent_address = $row->permanent_address;
        $cr_present_address = $row->present_address;
    }
}

if(isset($data_ai)) {
    foreach ($data_ai as $row) {
        $cr_a_degree_type = $row->a_degree_type;
        $cr_a_class_name = $row->a_class_name;
        $cr_a_institute = $row->a_institute;
        $cr_a_obtained_marks = $row->a_obtained_marks;
        $cr_a_total_marks = $row->a_total_marks;
        $cr_a_starting_date = $row->a_starting_date;
        $cr_a_ending_date = $row->a_ending_date;
    }
}
if(isset($data_exi)) {
    foreach ($data_exi as $row) {
        $cr_experience_type = $row->experience_type;
        $cr_designation_name = $row->designation_name;
        $cr_organization_name = $row->organization_name;
        $cr_salary = $row->salary;
        $cr_starting_date = $row->starting_date;
        $cr_ending_date = $row->ending_date;
    }
}
if(isset($data_ji)){
    foreach($data_ji as $row){
        $cr_branch_id 				=$row->branch_id;
        $cr_staff_designation	=$row->staff_designation;
        $cr_doa 				=$row->doa 				;
        $cr_job_type 			=$row->job_type			;
        $cr_basic_salary 		=$row->basic_salary 	;
        $cr_allowance          	=$row->allowance;
        $cr_allowance_description      =$row->allowance_description;
        $cr_comments          	=$row->comments;
    }

}
?>
<div class="formp-print">
    <div class="formp-header"></div>
    <div class="formp-body">
        <div class="formp-box">

            <div class="row" style="height:180px;" >
                <div class="col-4">
                    <img  class="profile-pic " src="<?php
                    if (file_exists("pdb/staff/staff_id_" . $staff_id . ".jpg")) {
                        echo base_url("pdb/staff/staff_id_" . $staff_id . ".jpg");
                    }
                    else if (file_exists("pdb/staff/staff_id_" . $staff_id . ".jpeg")) {
                        echo base_url("pdb/staff/staff_id_" . $staff_id . ".jpeg");
                    }
                    else if (file_exists("pdb/staff/staff_id_" . $staff_id . ".png")) {
                        echo base_url("pdb/staff/staff_id_" . $staff_id . ".png");
                    }
                    else if (file_exists("pdb/staff/staff_id_" . $staff_id . ".gif")) {
                        echo base_url("pdb/staff/staff_id_" . $staff_id . ".gif");
                    }
                    else {
                        echo base_url("pdb/profile.jpg");
                    }
                    ?>">
                     </div>
                <div class="col-8" style="position:relative; top: -130px; left:170px">
                    <div class=" row-inline row-block "  >
                        <div class="col-6"><p class="field">Name</p> <p class="value" ><?php echo $cr_first_name." ".$cr_last_name; ?></p></div>
                        <div class="col-6"><p class="field">Marital Status</p> <p class="value"><?php echo $cr_marital_status; ?></p></div>
                    </div>
                    <div class=" row-inline row-block "  >
                        <div class="col-6"><p class="field">Relative</p> <p class="value" ><?php echo $cr_spouse_type; ?></p></div>
                        <div class="col-6"><p class="field">Name</p> <p class="value"><?php echo $cr_spouse_name; ?></p></div>
                    </div>
                    <div class=" row-inline row-block "  >
                        <div class="col-6"><p class="field">Gender</p> <p class="value" ><?php
                                foreach ($a_gender as $key=>$value){
                                    if( !empty($cr_gender) && $cr_gender==$key )
                                        echo $value;
                                }
                                ?></p></div>
                        <div class="col-6"><p class="field">DOB</p> <p class="value"><?php echo $cr_dob; ?></p></div>
                    </div>
                    <div class=" row-inline row-block "  >
                        <div class="col-6"><p class="field">CNIC  #</p> <p class="value" ><?php echo $cr_cnic; ?></p></div>
                        <div class="col-6"><p class="field">Domicile</p> <p class="value"><?php
                                foreach ($a_domicile as $key=>$value){
                                    if( !empty($cr_domicile) && $cr_domicile==$key )
                                        echo $value;
                                }
                                ?></p></div>
                    </div>

                    <div class=" row-inline row-block "  >
                        <div class="col-6"><p class="field">Phone No</p> <p class="value" ><?php echo $cr_phone_no; ?></p></div>
                        <div class="col-6"><p class="field">Cell No</p> <p class="value"><?php echo $cr_cell_no; ?></p></div>
                    </div>
                    <div class=" row-inline row-block "  >
                        <div class="col-6"><p class="field">Email</p> <p class="value" ><?php echo $cr_email; ?></p></div>

                    </div>
                    <div class=" row-inline row-block "  >
                        <div class="col-6"><p class="field">Religion</p> <p class="value" ><?php
                                foreach ($a_religion as $key=>$value){
                                    if( !empty($cr_religion) && $cr_religion==$key )
                                        echo $value;
                                }
                                ?></p></div>
                        <div class="col-6"><p class="field">Town</p> <p class="value"><?php echo $cr_town; ?></p></div>
                    </div>
                    <div class=" row-inline row-block "  >
                        <?php
                        if(empty($cr_other_city)){ //
                            ?>
                            <div class="col-6"><p class="field">City</p> <p class="value" ><?php
                                    foreach ($a_city as $key=>$value){
                                        if( !empty($cr_city) && $cr_city==$key )
                                            echo $value;
                                    }
                                    ?></p></div>
                            <?php     /* */    }else{    // ?>
                            <div class="col-6"><p class="field">City</p> <p class="value" ><?php
                                    echo $cr_other_city;
                                    ?></p></div>

                            <?php     /* */ }    // ?>

                        <div class="col-6"><p class="field">Country</p> <p class="value"><?php
                                foreach ($a_country as $key=>$value){
                                    if( !empty($cr_country) && $cr_country==$key )
                                        echo $value;
                                }
                                ?></p></div>

                    </div>

                </div>
            </div>
            <div class="row">
                <p class="field">Present Address: </p> <p class="value"><?php  echo $cr_present_address; ?></p>
            </div>
            <div class="row">
                <p class="field">Permanent Address: </p> <p class="value"><?php  echo $cr_present_address;  ?></p>
            </div>

        </div>

        <div class="formp-box">
            <h1>Academic Information</h1>
            <table >
                <thead>
                <th>Institute</th>
                <th>Marks</th>
                <th>Starting Date</th>
                <th>Ending Date</th>
                </thead>
                <tbody>
                <tr>
                    <td><?php echo $cr_a_institute; ?></td>
                    <td><?php echo (($cr_a_obtained_marks/$cr_a_total_marks)*100)."%"; ?></td>
                    <td><?php echo $cr_a_starting_date; ?></td>
                    <td><?php echo $cr_a_ending_date; ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="formp-box">
            <h1>Experience Information</h1>
            <table >
                <thead>
                <th>Experience</th>
                <th>Organization</th>
                <th>Designation</th>
                <th>Salary</th>
                <th>Duration</th>
                </thead>
                <tbody>
                <tr>
                    <td><?php echo $cr_experience_type; ?></td>
                    <td><?php echo $cr_organization_name; ?></td>
                    <td><?php echo $cr_designation_name; ?></td>
                    <td><?php echo $cr_salary; ?></td>
                    <td><?php echo $cr_starting_date."/". $cr_ending_date; ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="formp-box">
            <h1>Job Information</h1>
            <div class=" row row-block "  >
                <div class="col-4"><p class="field">DOA</p> <p class="value" ><?php
                       echo $cr_doa;
                        ?></p></div>
                <div class="col-4"><p class="field">Job Type</p> <p class="value"><?php
                        foreach($a_job_type as $rows){
                            $key = $rows["var_vid"];
                            $value = $rows["var_value"];
                            if( !empty($cr_job_type) && $cr_job_type==$key)
                                echo $value;
                        }
                        ?></p></div>
                <div class="col-4"><p class="field">Basic Salary</p> <p class="value"><?php
                        echo $cr_basic_salary;
                        ?></p></div>
            </div>
            <div class=" row row-block "  >
                <div class="col-4"><p class="field">Designation</p> <p class="value" ><?php
                        echo $cr_staff_designation;
                        ?></p></div>
                <div class="col-4"><p class="field">Allowances</p> <p class="value"><?php
                        echo $cr_allowance;
                        ?></p></div>

            </div>
            <div class=" row row-block "  >
                <div class="col-12"><p class="field">Allowances Description</p> <p class="value" ><?php
                        echo $cr_staff_designation;
                        ?></p></div>

            </div>
            <div class=" row row-block "  >
                <div class="col-12"><p class="field">Comments </p> <p class="value" ><?php
                        echo $cr_comments;
                        ?></p></div>

            </div>
        </div>
    </div>
    <div class="formp-footer"></div>
</div>
</body>
</html>