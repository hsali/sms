<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main" xmlns="http://www.w3.org/1999/html">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2");
        ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-0"
                         data-widget-editbutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse">-->
                                <!--                                    <i class="fa fa-angle-down "></i>-->
                                <!--                                </a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                    <i class="fa fa-expand "></i>
                                </a>
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete">-->
                                <!--                                    <i class="fa fa-times"></i>-->
                                <!--                                </a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>

                            <h2><?php if (isset($widget_title)) echo $widget_title; else echo "Staff Management"; ?></h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <fom class="form-horizontal">
                                    <?php
                                    $a_domicile = $this->config->item('domicile');
                                    $a_gender = $this->config->item('gender');
                                    $a_religion = $this->config->item('religion');
                                    $a_country = $this->config->item('country');
                                    $a_city = $this->config->item('city');
                                    $a_guardian_type = $this->config->item('guardian_type');
                                    $a_marital_status = $this->config->item('a_marital_status');
                                    $a_guardian_status = $this->config->item('guardian_status');
                                    $a_degree_types = $a_guardian_education = $this->config->item('degree_type');
                                    ?>
                                    <?php
                                    if(isset($data_pi)) {
                                        foreach ($data_pi as $row) {
                                            $staff_id = $row->staff_id;
                                            $cr_first_name = $row->first_name;
                                            $cr_last_name = $row->last_name;
                                            $cr_spouse_type = $row->spouse_type;
                                            $cr_spouse_name = $row->spouse_name;
                                            $cr_gender = $row->gender;
                                            $cr_dob = $row->dob;
                                            $cr_cnic = $row->cnic;
                                            $cr_religion = $row->religion;
                                            $cr_domicile = $row->domicile;
                                            $cr_marital_status = $row->marital_status;
                                            $cr_phone_no = $row->phone_no;
                                            $cr_email = $row->email;
                                            $cr_cell_no = $row->cell_no;
                                            $cr_country = $row->country;
                                            $cr_city = $row->city;
                                            $cr_other_city = $row->other_city;
                                            $cr_town = $row->town;
                                            $cr_permanent_address = $row->permanent_address;
                                            $cr_present_address = $row->present_address;
                                        }
                                    }

                                    if(isset($data_ai)) {
                                        foreach ($data_ai as $row) {
                                            $cr_a_degree_type = $row->a_degree_type;
                                            $cr_a_class_name = $row->a_class_name;
                                            $cr_a_institute = $row->a_institute;
                                            $cr_a_obtained_marks = $row->a_obtained_marks;
                                            $cr_a_total_marks = $row->a_total_marks;
                                            $cr_a_starting_date = $row->a_starting_date;
                                            $cr_a_ending_date = $row->a_ending_date;
                                        }
                                    }
                                    if(isset($data_exi)) {
                                        foreach ($data_exi as $row) {
                                            $cr_experience_type = $row->experience_type;
                                            $cr_designation_name = $row->designation_name;
                                            $cr_organization_name = $row->organization_name;
                                            $cr_salary = $row->salary;
                                            $cr_starting_date = $row->starting_date;
                                            $cr_ending_date = $row->ending_date;
                                        }
                                    }
                                    if(isset($data_ji)){
                                        foreach($data_ji as $row){
                                            $cr_staff_id = $row->staff_id;
                                            $cr_b_staff_id = $row->b_staff_id;
                                            $cr_branch_id 				=$row->branch_id;
                                            $cr_staff_designation	=$row->staff_designation;
                                            $cr_doa 				=$row->doa 				;
                                            $cr_job_type 			=$row->job_type			;
                                            $cr_basic_salary 		=$row->basic_salary 	;
                                            $cr_comments          	=$row->comments;
                                        }

                                    }
                                    ?>

                                    <fieldset>
                                        <h1><strong>Personal Information</strong></h1>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img width="200" height="200" class="img-responsive img-rounded" src="<?php
                                                if (file_exists("pdb/staff/staff_id_" . $staff_id . ".jpg")) {
                                                    echo base_url("pdb/staff/staff_id_" . $staff_id . ".jpg");
                                                }
                                                else if (file_exists("pdb/staff/staff_id_" . $staff_id . ".jpeg")) {
                                                    echo base_url("pdb/staff/staff_id_" . $staff_id . ".jpeg");
                                                }
                                                else if (file_exists("pdb/staff/staff_id_" . $staff_id . ".png")) {
                                                    echo base_url("pdb/staff/staff_id_" . $staff_id . ".png");
                                                }
                                                else if (file_exists("pdb/staff/staff_id_" . $staff_id . ".gif")) {
                                                    echo base_url("pdb/staff/staff_id_" . $staff_id . ".gif");
                                                }
                                                else {
                                                    echo base_url("pdb/profile.jpg");
                                                }
                                                ?>">

                                            </div>
                                            <br>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="font-md">Staff ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php echo $cr_staff_id; ?></ins>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <p class="font-md">Serial No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php echo $cr_b_staff_id; ?></ins>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="font-md">Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php echo $cr_first_name . " " . $cr_last_name; ?></ins>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <p class="font-md">Marital Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php
                                                                foreach($a_marital_status as $key=>$value)
                                                                    if(!empty($cr_marital_status) && $cr_marital_status == $key)
                                                                        echo $value;
                                                                ?></ins></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="font-md"><span title="">Relative Type</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php
                                                                foreach($a_spouse_type as $rows) {
                                                                    $key = $rows["var_vid"];
                                                                    $value = $rows["var_value"];
                                                                    if (!empty($cr_spouse_type) && $cr_spouse_type == $key)
                                                                        echo $value;
                                                                }
                                                                ?></ins> </p>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <p class="font-md"><span title="">Relative Name</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php echo $cr_spouse_name; ?></ins>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="font-md">Gender &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php
                                                                foreach ($a_gender as $key=>$value){
                                                                    if( !empty($cr_gender) && $cr_gender==$key )
                                                                        echo $value;
                                                                }
                                                                ?></ins> </p>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <p class="font-md"><span title="Date Of Birth">DOB</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php echo $cr_dob; ?></ins>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="font-md">CNIC # &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php echo $cr_cnic; ?></ins>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <p class="font-md">Domicile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php
                                                                foreach($a_domicile as $key=>$value){
                                                                    if( !empty($cr_domicile) && $cr_domicile==$key)
                                                                        echo $value;
                                                                }
                                                                ?></ins> </p>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="font-md">Cell No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php echo $cr_cell_no; ?></ins>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <p class="font-md">Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php echo $cr_email; ?></ins>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="font-md">Religion &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php
                                                                foreach($a_religion as $key=>$value){
                                                                    if( !empty($cr_religion) && $cr_religion==$key)
                                                                        echo $value;
                                                                }
                                                                ?></ins> </p>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <p class="font-md">Town &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ins><?php echo $cr_town; ?></ins>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p class="font-md">Country &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php
                                                        foreach($a_country as $key=>$value){
                                                            if( !empty($cr_country) && $cr_country==$key)
                                                                echo $value;
                                                        }
                                                        ?></ins> </p>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="font-md">City &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php
                                                        foreach($a_city as $key=>$value){
                                                            if( !empty($cr_city) && $cr_city==$key)
                                                                echo $value;
                                                        }
                                                        ?></ins> </p>
                                            </div>
                                            <div class="col-md-4 ">
                                                <p class="font-md">Other City &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php echo $cr_other_city; ?></ins>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="font-md">Present Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php echo $cr_present_address; ?></ins>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="font-md">Permanent Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php echo $cr_permanent_address; ?></ins>
                                                </p>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <hr>
                                    <fieldset>
                                        <h1><strong>Academic Information</strong></h1>
                                        <table class="table table-bordered" width="100%">
                                            <thead>
                                            <tr>
                                                <th data-hide="phone">Institute</th>
                                                <th data-hide="phone">Type</th>
                                                <th data-class="expand">Class</th>
                                                <th data-hide="phone">Obtained Marks</th>
                                                <th data-hide="phone">Total Marks</th>
                                                <th data-hide="phone">Starting Date</th>
                                                <th data-hide="phone,tablet">Ending Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr id="">
                                                <td><?php echo $cr_a_institute; ?> </td>
                                                <td>
                                                    <?php
                                                    foreach($a_degree_types as $key=>$value){
                                                        if( !empty($cr_a_degree_type) && $cr_a_degree_type==$key)
                                                            echo $value;
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $cr_a_class_name; ?></td>
                                                <td><?php echo $cr_a_obtained_marks; ?></td>
                                                <td><?php echo $cr_a_total_marks; ?></td>
                                                <td><?php echo $cr_a_starting_date; ?></td>
                                                <td><?php echo $cr_a_ending_date; ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </fieldset>
                                    <hr>
                                    <fieldset>
                                        <h1><strong>Experience Information</strong></h1>
                                        <table class="table table-bordered " width="100%">
                                            <thead>
                                            <tr>
                                                <th data-hide="phone">Experience</th>
                                                <th data-class="expand">Organization</th>
                                                <th data-class="expand">Designation</th>
                                                <th data-hide="phone">Salary</th>
                                                <th data-hide="phone">Starting Date </th>
                                                <th data-hide="phone,tablet">Ending Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr id="">
                                                <td>
                                                    <?php
                                                    foreach ($a_experience_type as $rows) {
                                                        $key = $rows["var_vid"];
                                                        $value = $rows["var_value"];
                                                        if( !empty($cr_experience_type) && $cr_experience_type==$key)
                                                            echo $value;
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $cr_organization_name; ?></td>
                                                <td><?php echo $cr_designation_name; ?></td>
                                                <td><?php echo $cr_salary; ?></td>
                                                <td><?php echo $cr_starting_date; ?></td>
                                                <td><?php echo $cr_ending_date; ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </fieldset>
                                    <fieldset>
                                        <h1><strong>Enrollment Information</strong></h1>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="font-md">Date of Appointment &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php echo $cr_doa; ?></ins>
                                                </p>
                                            </div>
                                            <div class="col-md-6 ">
                                                <p class="font-md"><span title="">Job Type</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php
                                                        foreach($a_job_type as $rows){
                                                            $key=$rows['var_vid'];
                                                            $value=$rows['var_value'];
                                                            if( !empty($cr_job_type) && $cr_job_type==$key)
                                                                echo $value;
                                                        }
                                                        ?></ins> </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="font-md">Basic Salary &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php

                                                            if( !empty($cr_basic_salary))
                                                                echo $cr_basic_salary;

                                                        ?></ins> </p>
                                            </div>
                                            <div class="col-md-6 ">
                                                <p class="font-md"><span title="">Designation</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php
                                                        foreach($a_staff_designation as $rows){
                                                            $key = $rows["var_vid"];
                                                            $value = $rows["var_value"];
                                                            if( !empty($cr_staff_designation) && $cr_staff_designation==$key)
                                                                echo $value;
                                                        }
                                                        ?></ins> </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="font-md">Comments : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <ins><?php
                                                        if( !empty($cr_comments))
                                                            echo $cr_comments;
                                                        ?></ins> </p>
                                            </div>
                                        </div
                                    </fieldset>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <a href="<?php echo site_url("admin/staff/del/" . $staff_id); ?>"
                                                   class="btn btn-danger btn-lg fa fa-times-circle"> Delete</a>
                                                <a href="<?php echo site_url("admin/staff/edit/" . $staff_id); ?>"
                                                   class="btn btn-warning btn-lg fa fa-edit "> Edit</a>
                                                <a href="<?php echo site_url("admin/staff/print/" . $staff_id); ?>"
                                                   class="btn btn-success btn-lg fa fa-print"> Print</a>

                                            </div>
                                        </div>
                                    </div>
                                </fom>


                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>




                <!-- WIDGET END -->

            </div>

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->





</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->



<?php
// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
