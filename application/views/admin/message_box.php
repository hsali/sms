<div class="row">

    <!-- NEW WIDGET START -->
    <article class="col-sm-12">

        <?php
        if(isset($message["code"])):

            ?>

            <div class="alert alert-<?php echo $message["msg_class"]; ?>  fade in">

                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa fa-<?php echo $message["msg_icon"]; ?>"></i>

                <strong><?php echo $message["msg_header"]; ?> </strong> <?php echo $message["msg_body"];  ?>
            </div>
        <?php  endif; ?>

        <!--                    <div class="alert alert-warning fade in">-->
        <!--                        <button class="close" data-dismiss="alert">-->
        <!--                            ×-->
        <!--                        </button>-->
        <!--                        <i class="fa-fw fa fa-warning"></i>-->
        <!--                        <strong>Warning</strong> Your monthly traffic is reaching limit.-->
        <!--                    </div>-->
        <!---->
        <!--                    <div class="alert alert-success fade in">-->
        <!--                        <button class="close" data-dismiss="alert">-->
        <!--                            ×-->
        <!--                        </button>-->
        <!--                        <i class="fa-fw fa fa-check"></i>-->
        <!--                        <strong>Success</strong> The page has been added.-->
        <!--                    </div>-->
        <!---->
        <!--                    <div class="alert alert-info fade in">-->
        <!--                        <button class="close" data-dismiss="alert">-->
        <!--                            ×-->
        <!--                        </button>-->
        <!--                        <i class="fa-fw fa fa-info"></i>-->
        <!--                        <strong>Info!</strong> You have 198 unread messages.-->
        <!--                    </div>-->
        <!---->
        <!--                    <div class="alert alert-danger fade in">-->
        <!--                        <button class="close" data-dismiss="alert">-->
        <!--                            ×-->
        <!--                        </button>-->
        <!--                        <i class="fa-fw fa fa-times"></i>-->
        <!--                        <strong>Error!</strong> The daily cronjob has failed.-->
        <!--                    </div>-->

    </article>
    <!-- WIDGET END -->

</div>