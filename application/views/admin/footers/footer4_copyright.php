<!-- PAGE FOOTER -->
<hr>
<div class="page-footer">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <span class="txt-color-white"><?php echo $this->config->item("page_title"); ?> Copyright &copy; 2014-<?php echo date("y"); ?> Created by <a href="http://hsali.me">Hafiz Shehbaz Ali</a></span>
        </div>
    <?php // $this->load->view("admin/footers/footer_menu_right"); ?>

    </div>
</div>
<!-- END PAGE FOOTER -->
