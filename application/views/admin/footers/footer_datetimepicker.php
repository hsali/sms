<script type="text/javascript">
    $(function () {
        $('.date-picker').datetimepicker({
            viewMode: 'days',
            format: 'YYYY-MM-DD'

        });
    });
    $(function () {
        $('.date-time-picker').datetimepicker({
            viewMode: 'days',
            format: 'YYYY-MM-DD H:m:s',

        });
    });
    $(function () {
        $('.date-month-year-picker').datetimepicker({
            viewMode: 'months',
            format: 'YYYY-MM',

        });
    });
</script>