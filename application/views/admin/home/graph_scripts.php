
<!--         donut-->
        if ($('#student-summary').length) {
            Morris.Donut({
                element : 'student-summary',
                data : [<?php foreach($student_summary as $row){ ?>{
                    value : <?php echo $row["value"]; ?>,
                    label : '<?php echo $row["title"]; ?>'
                },
            <?php          /* */ }     //  ?>
                ],
                formatter : function(x) {
                    return x + "%"
                }
            });
        }
<!--Staff Summary-->

        if ($('#staff-summary').length) {
        Morris.Donut({
        element : 'staff-summary',
        data : [<?php foreach($staff_summary as $row){ ?>{
            value : <?php echo $row["value"]; ?>,
            label : '<?php echo $row["title"]; ?>'
            },
            <?php          /* */ }     //  ?>
        ],
        formatter : function(x) {
        return x + "%"
        }
        });
        }

<!--Gender Staff Summary-->

if ($('#gender-staff-summary').length) {
Morris.Donut({
element : 'gender-staff-summary',
data : [<?php foreach($gender_staff_summary as $row){ ?>{
    value : <?php echo $row["value"]; ?>,
    label : '<?php echo $row["title"]; ?>'
    },
    <?php          /* */ }     //  ?>
],
formatter : function(x) {
return x + "%"
}
});
}

<!--Gender Student Summary-->

if ($('#gender-student-summary').length) {
Morris.Donut({
element : 'gender-student-summary',
data : [<?php foreach($gender_student_summary as $row){ ?>{
    value : <?php echo $row["value"]; ?>,
    label : '<?php echo $row["title"]; ?>'
    },
    <?php          /* */ }     //  ?>
],
formatter : function(x) {
return x + "%"
}
});
}
