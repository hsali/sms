<!DOCTYPE html>
<html lang="en-us" id="extr-page">
<head>
    <meta charset="utf-8">
    <title> <?php echo $this->config->item("page_title"); ?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url("media/admin_panel"); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url("media/admin_panel"); ?>/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url("media/admin_panel"); ?>/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url("media/admin_panel"); ?>/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support is under construction
         This RTL CSS will be released in version 1.5
    <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.min.css"> -->

    <!-- We recommend you use "your_style.css" to override SmartAdmin
         specific styles this will also ensure you retrain your customization with each SmartAdmin update.
    <link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url("media/admin_panel"); ?>/css/demo.min.css">

    <!-- #FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url("media/admin_panel/img/favicon/favicon.ico"); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url($this->config->item("favicon")); ?>" type="image/x-icon">

    <!-- #GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- #APP SCREEN / ICONS -->
    <!-- Specifying a Webpage Icon for Web Clip
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="<?php echo base_url("media/admin_panel"); ?>/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url("media/admin_panel"); ?>/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url("media/admin_panel"); ?>/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url("media/admin_panel"); ?>/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="<?php echo base_url("media/admin_panel"); ?>/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="<?php echo base_url("media/admin_panel"); ?>/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="<?php echo base_url("media/admin_panel"); ?>/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

</head>

<body class="animated fadeInDown" >

<header id="header" style="background-color:#E8DDB7">

    <div id="logo-group">
        <span id="logo"> <img src="<?php echo base_url($this->config->item("wide_logo")); ?>" alt="Logo"> </span>
    </div>

   <!-- <span id="extr-page-header-space"> <span class="hidden-mobile">Need an account?</span> <a href="#" class="btn btn-danger">Create account</a> </span>-->

</header>

<div id="main" role="main" >

    <!-- MAIN CONTENT -->
    <div id="content" class="container" >

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
                <div class="well">
                    <h3><?php echo $this->config->item("welcome_institute_name"); ?></h3>
                    <div class="jumbotron">
                        <h1><?php echo $this->config->item("page_title"); ?></h1>
                    <p>
                        <?php echo $this->config->item("slogan");  ?>

                        </p>
                        <p><?php echo $this->config->item("mission_statement"); ?>
                        </p>
                    </div>
                </div>


            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                <div class="well no-padding">
                    <form method="post" enctype="multipart/form-data" id="login-form" class="smart-form client-form">
                        <header>
                            Sign In
                        </header>

                        <fieldset>

                            <section>
                                <label class="label">User Name</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input required type="text" name="user_name" placeholder="Username">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter username</b></label>
                                <span class="help-Block"><?php echo form_error("user_name");
                                    ?></span>
                            </section>

                            <section>
                                <label class="label">Password</label>
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input  type="password" name="user_password" placeholder="*********">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
                                 <span class="help-Block"><?php echo form_error("user_password");
                                     ?></span>
                               <!-- <div class="note">
                                    <a href="forgotpassword.html">Forgot password?</a>
                                </div>-->
                            </section>

                           <!-- <section>
                                <label class="checkbox">
                                    <input type="checkbox" name="remember" checked="">
                                    <i></i>Stay signed in</label>
                            </section>-->
                            <section>
                                <div class="note">
                                    Only Authenticated Users are allowed.<br>
                                    Only Administrator can allow the users to access the system
                                </div>
                            </section>
                            <br>
                            <br>
                        </fieldset>
                        <footer>
                            <button name="submit" id="submit" type="submit" class="btn btn-lg btn-primary">
                                Sign in <span class="fa fa-sign-in"></span>
                            </button>
                        </footer>
                    </form>

                </div>

                <h5 class="text-center"> - Social Media -</h5>

                <ul class="list-inline text-center">
                    <li>
                            <a href="<?php echo $this->config->item("social")["facebook"]; ?>" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("social")["twitter"]; ?>" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("social")["google"]; ?>" class="btn btn-danger btn-circle"><i class="fa fa-google-plus"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("social")["linkedin"]; ?>" class="btn btn-primary btn-circle"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>

            </div>
        </div>
        <hr>
        <div class="row">
            <footer>Copyright &copy; 2014-<?php echo date("Y"); ?> Created by <a href="http://hsali.me">Hafiz Shehbaz Ali</a></footer>
        </div>
    </div>

</div>

<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script src="<?php echo base_url("media/admin_panel"); ?>/js/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script> if (!window.jQuery) { document.write('<script src="<?php echo base_url("media/admin_panel"); ?>/js/libs/jquery-2.0.2.min.js"><\/script>');} </script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script> if (!window.jQuery.ui) { document.write('<script src="<?php echo base_url("media/admin_panel"); ?>/js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url("media/admin_panel"); ?>/js/bootstrap/bootstrap.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url("media/admin_panel"); ?>/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url("media/admin_panel"); ?>/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url("media/admin_panel"); ?>/js/app.min.js"></script>

<script type="text/javascript">
    runAllForms();

    $(function() {
        // Validation
        $("#login-form").validate({
            // Rules for form validation
            rules : {
                email : {
                    required : true
//                    email : true
                },
                password : {
                    required : true
//                    minlength : 3,
//                    maxlength : 20
                }
            },

            // Messages for form validation
            messages : {
                email : {
                    required : 'Please enter your email address'
//                    email : 'Please enter a VALID email address'
                },
                password : {
                    required : 'Please enter your password'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });
    });
</script>

</body>
</html>