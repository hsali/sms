<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>



            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-yellow" id="wid-id-3" data-widget-editbutton="false"
                         data-widget-colorbutton="false" data-widget-collapsed="false" data-widget-deletebutton="false"
                         data-widget-togglebutton="false">
                        <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-paper-plane-o"></i> </span>

                            <h2><?php if(isset($widget_title)) echo $widget_title; else "Fee Receipt"; ?></h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body padding-4">
                                </br>

                                <div class="row">
                                    <div class=" col-sm-12">
                                        <h1 style="display:inline">Receipt ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $view_data["receipt_id"]; ?></small>
                                        </h1>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <h1 style="display:inline">Student ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $view_data["student_id"]; ?></small>
                                        </h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class=" col-sm-12">
                                        <h1 style="display:inline">Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $view_data["first_name"]." ".$view_data["last_name"]; ?></small>
                                        </h1>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <h1 style="display:inline">Class &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $view_data["class_name"]; ?></small>
                                        </h1>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>Fee Detail</h2>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Fee</th>
                                                <th>Month</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if (isset($received_fee)) {
                                                foreach ($received_fee as $row) {
                                                    /* */ ?>
                                                    <tr id="<?php echo $row["rf_id"]; ?>">
                                                        <td><?php echo $row["fee_category"]; ?></td>
                                                        <td><?php echo $row["fee"]; ?></td>
                                                        <td><?php echo $row["period"]; ?>

                                                        </td>
                                                    </tr>
                                                    <?php /* */

                                                } /* */
                                            }  /* */?>
                                            </tbody>
                                            <tfoot>
                                            <th>Total</th>
                                            <th><?php echo $view_data["received_payment"]; ?></th>
                                            <th></th>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <a href="<?php echo site_url("admin/fee/receipt/print/" . $view_data["receipt_id"]); ?>"
                                           class="btn btn-success btn-lg"><span class="fa fa-print">&nbsp;&nbsp;Print</span></a>


                                    </div>
                                </div>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>
                <!-- WIDGET END -->

            </div>

            <!-- end row -->

            <!-- end row -->

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
