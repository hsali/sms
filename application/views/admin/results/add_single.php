<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>


            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-0"
                         data-widget-editbutton="false"
                         role="widget" data-widget-colorbutton="false" data-widget-collopse="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!--                                <a href="javascript:void(0);"-->
                                <!--                                                                           class="button-icon jarviswidget-toggle-btn"-->
                                <!--                                                                           rel="tooltip" title=""-->
                                <!--                                                                           data-placement="bottom"-->
                                <!--                                                                           data-original-title="Collapse"><i-->
                                <!--                                        class="fa fa-minus "></i></a> -->
                                <a href="javascript:void(0);"
                                   class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom"
                                   data-original-title="Fullscreen"><i
                                        class="fa fa-expand "></i></a>
                                <!--                                <a href="javascript:void(0);"-->
                                <!--                                                                          class="button-icon jarviswidget-delete-btn"-->
                                <!--                                                                          rel="tooltip" title="" data-placement="bottom"-->
                                <!--                                                                          data-original-title="Delete"><i-->
                                <!--                                        class="fa fa-times"></i></a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>

                            <h2><?php if(isset($widget_title)) echo $widget_title; else echo "Examination Management"; ?></h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                                <div class="alert alert-warning fade in">
                                    <button class="close" data-dismiss="alert">
                                        ×
                                    </button>
                                    <i class="fa-fw fa fa-2x fa-times"></i>
                                    <strong>Warning!</strong> Be careful to add the data. Once enter. You cannot modify it. After enter this data, you will go to receipt after saving data.
                                </div>

                                <form class="form-horizontal" method="post" enctype="multipart/form-data">

                                    <fieldset>
                                        <legend>Add Result</legend>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div
                                                    class="form-group  <?php if (form_error("student_id")) echo "has-error"; elseif (!empty($student_id)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Student ID*</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" required placeholder="Choose Student" id="student_id" name="student_id" list="staff_names"  type="text" value="<?php if(isset($student_id)) echo $student_id; ?>" autocomplete="off">
                                                        <datalist id="staff_names">
                                                            <?php if(isset($a_students)){
                                                                foreach($a_students as $rows){
                                                                    ?>
                                                                    <option value="<?php echo $rows["student_id"]; ?>" ><?php echo $rows["first_name"]." ".$rows["last_name"]; ?></option>

                                                                    <?php    /* */
                                                                }
                                                            } /*  */?>
                                                        </datalist>
                                                        <span class="help-Block"><?php echo form_error("student_id");?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <!--Date -->
                                                <div class="form-group  <?php if (form_error("exam_date")) echo "has-error"; elseif (!empty($exam_date)) echo "has-success"?>">
                                                    <label class="col-md-4 control-label">Exam Date*</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                            <input class="form-control" id="exam_date" name="exam_date" type="text" placeholder="Choose Date" autocomplete="off" value="<?php if (isset($exam_date)) echo $exam_date;?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                            <span class="help-Block"><?php echo form_error("exam_date");?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("result_category")) echo "has-error";
                                                elseif (!empty($result_category)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Result Category*</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control" required id="result_category" name="result_category">
                                                            <option value="" selected=""  disabled="">
                                                                Select Result Category
                                                            </option>
                                                            <?php
                                                            foreach ($this->m_common->select_variable("result_category") as $rows) {
                                                                $key = $rows["var_vid"];
                                                                $value = $rows["var_value"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($result_category) && $result_category == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("result_category");
                                                            ?></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <hr>
                                        <div class="multi-field-wrapper">
                                            <div class="multi-fields">
                                                <div class="multi-field">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group <?php
                                                            if (form_error("subject_id"))
                                                                echo "has-error";
                                                            elseif (!empty($subject_id))
                                                                echo
                                                                "has-success"
                                                            ?>">

                                                                <div class="col-md-12">

                                                                    <select class="form-control" id="subject_id"
                                                                            name="subject_id[]">
                                                                        <option value="" selected="" disabled="">
                                                                            Select Subject
                                                                        </option>
                                                                        <?php
                                                                        foreach ($this->m_common->select_variable('a_subjects') as $rows) {
                                                                            $key = $rows["var_vid"];
                                                                            $value = $rows["var_value"];
                                                                            echo "<option label='{$value}' value='{$key}'";
                                                                            if (isset($subject_id) && $subject_id == $key) {
                                                                                echo 'selected="selected"';
                                                                            }
                                                                            echo ">" . $value . "</option>";
                                                                        }
                                                                        ?>
                                                                    </select>
                                                        <span class="help-Block"><?php echo form_error("subject_id");
                                                            ?></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-4">
                                                            <input class="form-control" id="obtained_marks"
                                                                   name="obtained_marks[]"
                                                                   placeholder="Enter Obtained Marks" type="text"
                                                                   value="<?php if (isset($obtained_marks))
                                                                       echo $obtained_marks;
                                                                   ?>">
                                                            <span class="help-Block"><?php echo form_error("obtained_marks"); ?>
                                                            Obtained Marks</span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input class="form-control" id="total_marks"
                                                                   name="total_marks[]"
                                                                   placeholder="Enter Total Marks" type="text"
                                                                   value="<?php if (isset($total_marks))
                                                                       echo $total_marks;
                                                                   ?>">
                                                            <span class="help-Block"><?php echo form_error("total_marks"); ?>
                                                                Total Marks:</span>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="remove-field btn btn-danger "><span class="fa fa-minus-circle"></span> Remove
                                                    </button>
<hr>
                                                </div>
                                                <br/>
                                            </div>
                                            <br/>
                                            <button type="button" class="add-field btn btn-success"> <span class="fa fa-plus-circle"></span> Add field</button>
                                        </div>


                                    </fieldset>


                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset
                                                </button>
                                                <button type="submit" name="submit" id="submit" class="btn
                                              btn-success btn-lg">Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


//$this->load->view("admin/footers/footer_datatables");



// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
