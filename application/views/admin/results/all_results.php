<?php $this->load->view("admin/header"); ?>

    <!-- Left panel : Navigation area -->
    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
    <!-- END NAVIGATION -->

    <!-- MAIN PANEL -->
    <div id="main" role="main">

        <!-- RIBBON -->
        <?php $this->load->view("admin/headers/ribbon"); ?>
        <!-- END RIBBON -->

        <!-- MAIN CONTENT -->
        <div id="content">

            <?php
            // ribbon 2 contain the summary of page.
            $this->load->view("admin/headers/ribbon2"); ?>

            <!-- widget grid -->
            <section id="widget-grid" class="">

                <?php $this->load->view("admin/message_box"); ?>

                <!-- row -->
                <div class="row">

                    <!-- NEW WIDGET START -->
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget jarviswidget-color-greenLight" id="wid-id-3" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-collapsed="false" data-widget-deletebutton="false" data-widget-togglebutton="false">
                            <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"
                                    data-widget-sortable="false"

                                    -->
                            <header>
                                <span class="widget-icon"> <i class="fa fa-book"></i> </span>
                                <h2><?php if(isset($widget_title)) echo $widget_title; else echo "Fee Management"; ?></h2>

                            </header>

                            <!-- widget div-->
                            <div>

                                <!-- widget edit box -->
                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->

                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding">

                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th data-hide="phone" title="Result ID">RID</th>
                                            <th data-class="expand" title="Student ID">SID</th>
                                            <th data-class="phone,tablet" title="Student Name">Name</th>
                                            <th data-class="phone,tablet" title="Class Name">Class</th>
                                            <th data-class="phone,tablet" title="Result Category">Result</th>
                                            <th data-class="phone,tablet" title=" Name">Subject </th>
                                            <th data-hide="phone">Date</th>
                                            <th data-hide="phone">Marks</th>
                                            <th data-hide="phone">%age</th>
                                        </tr>
                                        </thead>
                                       <tbody>
                                       <?php foreach($view_data as $rows): ?>
                                           <tr id="result_id_<?php  echo $rows["result_id"]; ?>">
                                               <td><a href="#"><?php echo $rows["result_id"]; ?></a></td>
                                               <td><?php  echo $rows["student_id"]; ?></td>
                                               <td><?php  echo $rows["first_name"]." ".$rows["last_name"];?></td>
                                               <td><?php  echo $rows["class_name"];?></td>
                                               <td><?php  echo $this->m_common->select_value_of_variable($rows["result_category_id"]);?></td>
                                               <td><?php  echo $rows["subject_name"];?></td>
                                               <td><?php  echo $rows["exam_date"];?></td>
                                               <td><?php  echo $rows["obtained_marks"]."/".$rows["total_marks"];?></td>
                                               <td><?php  echo $rows["percentage"];?></td>
                                           </tr>
                                       <?php endforeach; ?>
                                       </tbody>
                                    </table>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end widget div -->

                        </div>
                        <!-- end widget -->

                    </article>
                    <!-- WIDGET END -->

                </div>

                <!-- end row -->

                <!-- end row -->

            </section>
            <!-- end widget grid -->

        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->


    <!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
