<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main" xmlns="http://www.w3.org/1999/html">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

  <?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-0"  data-widget-editbutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div>
                            <span class="widget-icon"> <i class="fa fa-gear"></i> </span>
                            <h2>Branches Management</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal"  >

                                    <fieldset>
                                        <legend>Branch Profile</legend>
                                        <?php
                                        if(isset($view_data)) {
                                            foreach ($view_data as $row) {

                                                $cr_branch_name = $row["branch_name"];
                                                $branch_id = $row["branch_id"];
                                                $cr_branch_phone = $row["branch_phone"];
                                                $cr_branch_cell = $row["branch_cell"];
                                                $cr_branch_email = $row["branch_email"];
                                                $cr_branch_fax = $row["branch_fax"];
                                                $cr_branch_address = $row["branch_address"];
                                                $cr_branch_description = $row["branch_description"];

                                            }
                                        }

                                        ?>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-responsive " >
                            <tbody>
                            <tr>
                                <td><h2>Name</h2> </td>
                                <td><h3><?php echo $cr_branch_name; ?></h3></td>
                            </tr>
                            <tr>
                                <td><h2>Phone No:</h2> </td>
                                <td><h3><?php echo $cr_branch_phone; ?></h3></td>
                                <td><h2>Cell No:</h2> </td>
                                <td><h3><?php echo $cr_branch_cell; ?></h3></td>


                            </tr>
                            <tr>
                                <td><h2>Fax No:</h2> </td>
                                <td><h3><?php echo $cr_branch_fax; ?></h3></td>
                                <td><h2>Email:</h2> </td>
                                <td><h3><?php echo $cr_branch_email; ?></h3></td>

                            </tr>
                            <tr>
                                <td><h2>Address</h2></td>
                                <td><h3><?php
                                        echo $cr_branch_address;
                                        ?></h3></td>
                            </tr>
                            <tr>
                                <td><h2>Description</h2></td>
                                <td><h3><?php echo $cr_branch_description; ?></h3></td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                </div>
 
                                        


                                    </fieldset>





                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <a href="<?php echo site_url("admin/branches/del/".$branch_id); ?>"
                                                   class="btn
                                                btn-danger btn-lg"> Delete</a>
                                                <a href="<?php echo site_url("admin/branches/edit/".$branch_id); ?>"
                                                   class="btn
                                                btn-warning btn-lg"> Edit</a>

                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>




                <!-- WIDGET END -->

            </div>











        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->





</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
