<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>



            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-0"  data-widget-editbutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div>
                            <span class="widget-icon"> <i class="fa fa-gear "></i> </span>
                            <h2>Branches Management</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal"  method="post">

                                    <?php
                                    if(isset($edit_data)) {
                                        foreach ($edit_data as $row) {

                                            $cr_branch_name = $row["branch_name"];
                                            $cr_branch_phone = $row["branch_phone"];
                                            $cr_branch_cell = $row["branch_cell"];
                                            $cr_branch_email = $row["branch_email"];
                                            $cr_branch_fax = $row["branch_fax"];
                                            $cr_branch_address = $row["branch_address"];
                                            $cr_branch_description = $row["branch_description"];

                                        }
                                    }

                                    ?>

                                    <fieldset>
                                        <legend>Edit Branch </legend>

                                        <!--Branch name -->
                                        <div class="form-group <?php if(form_error("branch_name")) echo "has-error";
                                        elseif(! empty($branch_name)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Branch Name</label>
                                            <div class="col-md-10">
                                                <input required="" class="form-control" id="branch_name" name="branch_name"
                                                       placeholder="Type Branch Name" type="text" value="<?php
                                                if(isset($branch_name)) echo $branch_name;
                                                elseif(isset($cr_branch_name)) echo $cr_branch_name;
                                                ?>">
                                                <span class="help-Block"><?php echo form_error("branch_name");
                                                    ?></span>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if(form_error("branch_phone")) echo
                                                "has-error";
                                                elseif(! empty($branch_phone)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Phone No</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="branch_phone"
                                                               name="branch_phone" placeholder="Type Phone No"
                                                               type="text" value="<?php
                                                        if(isset($branch_phone)) echo $branch_phone;
                                                        elseif(isset($cr_branch_phone)) echo $cr_branch_phone;
                                                        ?>">
                                                  <span class="help-Block"><?php echo form_error("branch_phone");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Cell No-->
                                                <div class="form-group <?php if(form_error("branch_cell")) echo
                                                "has-error";
                                                elseif(! empty($branch_cell)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Cell No</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="branch_cell"
                                                               name="branch_cell" placeholder="Type Cell No"
                                                               type="text" value="<?php
                                                        if(isset($branch_cell)) echo $branch_cell;
                                                        elseif(isset($cr_branch_cell)) echo $cr_branch_cell;
                                                        ?>">
                                               <span class="help-Block"><?php echo form_error("branch_cell");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!--                        branch email-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if(form_error("branch_email")) echo
                                                "has-error";
                                                elseif(! empty($branch_email)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Email </label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="branch_email"
                                                               name="branch_email" placeholder="Type Email "
                                                               type="email" value="<?php
                                                        if(isset($branch_email)) echo $branch_email;
                                                        elseif(isset($cr_branch_email)) echo $cr_branch_email;
                                                        ?>">
                                                  <span class="help-Block"><?php echo form_error("branch_email");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Cell No-->
                                                <div class="form-group <?php if(form_error("branch_fax")) echo
                                                "has-error";
                                                elseif(! empty($branch_fax)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Fax No</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="branch_fax"
                                                               name="branch_fax" placeholder="Type Fax No"
                                                               type="text" value="<?php
                                                        if(isset($branch_fax)) echo $branch_fax;
                                                        elseif(isset($cr_branch_fax)) echo $cr_branch_fax;
                                                        ?>">
                                               <span class="help-Block"><?php echo form_error("branch_fax");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!--                                       Address -->
                                        <div class="form-group <?php if(form_error("branch_address")) echo "has-error";
                                        elseif(! empty($branch_address)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Address</label>
                                            <div class="col-md-10">
                                                <textarea id="branch_address" name="branch_address" class="form-control"
                                                          placeholder="Type Address House No# , Street # , Town,City, Postal Code, Country"
                                                          rows="4"><?php
                                                    if(isset($branch_address)) echo $branch_address;
                                                    elseif(isset($cr_branch_address)) echo $cr_branch_address;
                                                    ?></textarea>
                                                  <span class="help-Block"><?php echo form_error("branch_address");
                                                      ?></span>
                                            </div>
                                        </div>
                                        <!--Description about branch-->
                                        <div class="form-group <?php if(form_error("branch_description")) echo
                                        "has-error";
                                        elseif(! empty($branch_description)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Description</label>
                                            <div class="col-md-10">
                                                <textarea id="branch_description" name="branch_description" class="form-control"
                                                          placeholder="Type something about branch"
                                                          rows="4"><?php
                                                    if(isset($branch_description)) echo $branch_description;
                                                    elseif(isset($cr_branch_description)) echo $cr_branch_description;
                                                    ?></textarea>
                                                  <span class="help-Block"><?php echo form_error("branch_description");
                                                      ?></span>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset</button>
                                                <button type="submit" name="submit" id="submit" class="btn
                                              btn-success btn-lg">Save</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>




                <!-- WIDGET END -->

            </div>




        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
