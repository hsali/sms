<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-0"  data-widget-editbutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading"><div class="jarviswidget-ctrls" role="menu">
                                <!--<a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a>
                            <!--    <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>
                            <h2>User Registration Form</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal" method="post" enctype="multipart/form-data" >
<!--upload picture -->
                                    <fieldset>
                                        <legend>Profile Picture</legend>

<?php if(file_exists("pdb/users/user_id_".$user_pic_id.".jpg") || file_exists("pdb/users/user_id_".$user_pic_id.".jpeg") || file_exists("pdb/users/user_id_".$user_pic_id.".png") || file_exists("pdb/users/user_id_".$user_pic_id.".gif")){ ?>
<div class="row">
    <div class="col-md-9 col-lg-offset-3">
        <img width="150" height="300" src="<?php echo base_url("pdb/users/user_id_").$user_pic_id;
        if(file_exists("pdb/users/user_id_".$user_pic_id.".jpg"))
        echo ".jpg";
        elseif(file_exists("pdb/users/user_id_".$user_pic_id.".jpeg"))
        echo ".jpeg";
        elseif(file_exists("pdb/users/user_id_".$user_pic_id.".png"))
        echo ".png";
        elseif(file_exists("pdb/users/user_id_".$user_pic_id.".gif"))
        echo ".gif";
         ?>"
             class="img-responsive" alt="<?php echo $cr_user_name; ?>">

    </div>
</div>
</br>
    <div class="form-group">
        <label class="col-md-2 control-label">Upload Picture</label>
        <div class="col-md-10">
            <input class="btn btn-default" id="profile_picture" name="profile_picture" type="file">
            <p class="help-block">
                images support jpeg,jpg, gif,png . 200 X 200 px size recommended.
            </p>
        </div>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-success" type="submit" name="upload_picture"
                        id="upload_picture">
                    <i class="fa fa-save"></i>
                    Change Picture
                </button>
            </div>
        </div>
    </div>
<?php }
else { ?>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Upload Picture</label>
                                            <div class="col-md-10">
                                                <input class="btn btn-default" id="profile_picture" name="profile_picture" type="file">
                                                <p class="help-block">
                                                         images support jpeg,jpg, gif,png . 200 X 200 px size recommended.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-success" type="submit" name="upload_picture"
                                                            id="upload_picture">
                                                        <i class="fa fa-save"></i>
                                                       Upload Picture
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  } ?>
                                    </fieldset>

                                    </br>
<!--profile update-->
                                    <fieldset>

                                        <legend>User Information</legend>
                                        <?php
                                        if(isset($edit_user)) {
                                            foreach ($edit_user as $row) {
                                                $cr_branch_id 	=$row["branch_id"];
                                                $cr_user_name = $row["user_name"];
                                                $cr_first_name = $row["first_name"];
                                                $cr_last_name = $row["last_name"];
                                                $cr_email = $row["email"];
                                                $cr_about_me = $row["about_me"];

                                            }
                                        }

                                        ?>


                                        <!--      first name-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if(form_error("first_name")) echo "has-error";
                                                elseif(! empty($first_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">First Name</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="first_name" name="first_name" placeholder="Type First Name" type="text" value="<?php
                                                        if(isset($first_name)) echo $first_name;
                                                        elseif(isset($cr_first_name)) echo $cr_first_name;
                                                        ?>">
                                                  <span class="help-Block"><?php echo form_error("first_name");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Last Name-->
                                                <div class="form-group <?php if(form_error("last_name")) echo "has-error";
                                                elseif(! empty($last_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Last Name</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="last_name" name="last_name" placeholder="Type Last Name" type="text" value="<?php
                                                        if(isset($last_name)) echo $last_name;
                                                        elseif(isset($cr_last_name)) echo $cr_last_name;
                                                        ?>">
                                               <span class="help-Block"><?php echo form_error("last_name");
                                                   ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group <?php if (form_error("branch_id"))
                                                    echo "has-error"; elseif (!empty($branch_id)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Branch</label>

                                                    <div class="col-md-8">

                                                        <select class="form-control" id="branch_id" name="branch_id">
                                                            <option value="" selected="" disabled="">
                                                                Select Branch
                                                            </option>
                                                            <?php foreach ($a_branches as $rows) {
                                                                $key=$rows["branch_id"];
                                                                $value=$rows["branch_name"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($branch_id) && $branch_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                elseif (isset($cr_branch_id) && $cr_branch_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                  <span class="help-Block"><?php echo form_error("branch_id");
                                                      ?></span>
                                                    </div>
                                                </div>
                                                </div>


                                            <div class="col-md-6">

                                                <!--User Name-->
                                                <div class="form-group <?php if(form_error("user_name")) echo "has-error";
                                                elseif(! empty($user_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">User Name</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" readonly id="user_name"
                                                               name="user_name" placeholder="Type User Name" type="text" value="<?php if(isset($cr_user_name)) echo $cr_user_name; ?>">
                                                <span class="help-Block"><?php echo form_error("user_name");
                                                    ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            </div>
                                        <!--                                        Email -->
                                        <div class="form-group  <?php if(form_error("email")) echo "has-error";
                                        elseif(! empty($email)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Email</label>
                                            <div class="col-md-10">
                                                <input class="form-control" id="email" name="email" placeholder="Type email" type="email" value="<?php
                                                if(isset($email)) echo $email;
                                                elseif(isset($cr_email)) echo $cr_email;

                                                ?>">
                                                 <span class="help-Block"><?php echo form_error("email");
                                                     ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group <?php if(form_error("about_me")) echo "has-error";
                                        elseif(! empty($about_me)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">About me</label>
                                            <div class="col-md-10">
                                                <textarea id="about_me" name="about_me" class="form-control"
                                                          placeholder="Type something about yourself"
                                                          rows="4"><?php
                                                    if(isset($about_me)) echo $about_me;
                                                    elseif(isset($cr_about_me)) echo $cr_about_me;
                                                    ?></textarea>
                                                  <span class="help-Block"><?php echo form_error("about_me");
                                                      ?></span>
                                            </div>
                                        </div>

<!--update profile information-->
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-success" type="submit" name="update_profile"
                                                            id="update_profile">
                                                        <i class="fa fa-save"></i>
                                                       Update Profile
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                    </br>
<!--password change -->
                                    <fieldset>
     <legend>Security</legend>
     <!--                                        Password-->
     <div class="form-group <?php if(form_error("current_password")) echo "has-error";
     elseif(! empty($current_password)) echo "has-success" ?>">
         <label class="col-md-2 control-label">Current Password</label>
         <div class="col-md-10">
             <input class="form-control" id="current_password" name="current_password" placeholder="Type current Password " type="password"  value="<?php if(isset($current_password)) echo $current_password; ?>">
                                                 <span class="help-Block"><?php echo form_error("current_password");
                                                     ?></span>
         </div>
     </div>
     <!--                                        Password-->
     <div class="form-group <?php if(form_error("password")) echo "has-error";
     elseif(! empty($password)) echo "has-success" ?>">
         <label class="col-md-2 control-label">Password</label>
         <div class="col-md-10">
             <input class="form-control" id="password" name="password" placeholder="Type Password " type="password"  value="<?php if(isset($password)) echo $password; ?>">
                                                 <span class="help-Block"><?php echo form_error("password");
                                                     ?></span>
         </div>
     </div>

     <!--     again        Password-->
     <div class="form-group <?php if(form_error("re_password")) echo "has-error";
     elseif(! empty($re_password)) echo "has-success" ?>">
         <label class="col-md-2 control-label">Confirm Password</label>
         <div class="col-md-10">
             <input class="form-control" id="re_password" name="re_password" placeholder="Type Password again " type="password"  value="<?php if(isset($re_password)) echo $re_password; ?>">
                                                 <span class="help-Block"><?php echo form_error("re_password");
                                                     ?></span>
         </div>
     </div>


     <div class="form-actions">
         <div class="row">
             <div class="col-md-12">
                 <button class="btn btn-danger" type="reset" id="reset" name="reset">
                     Reset
                 </button>
                 <button class="btn btn-success" type="submit" name="change_password" id="change_password">
                     <i class="fa fa-save"></i>
                    Change Password
                 </button>
             </div>
         </div>
     </div>
 </fieldset>

</form>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>




                <!-- WIDGET END -->

            </div>

            <!-- row -->
            <!--          --><?php //$this->load->view("admin/b_bootstrap_form"); ?>

            <!-- end row -->










        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->





</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
