<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main" xmlns="http://www.w3.org/1999/html">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

  <?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-0"  data-widget-editbutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading"><div class="jarviswidget-ctrls" role="menu">
<!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a>
                                <!--<a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>
                            <h2><?php echo isset($widget_title)?  $widget_title: "User Profile";  ?></h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal"  >

                                    <fieldset>
                                        <legend>Profile</legend>
                                        <?php
                                        if(isset($edit_user)) {
                                            foreach ($edit_user as $row) {

                                                $cr_user_name = $row["user_name"];
                                                $user_id = $row["user_id"];
                                                $cr_first_name = $row["first_name"];
                                                $cr_last_name = $row["last_name"];
                                                $cr_email = $row["email"];
                                                $cr_about_me = $row["about_me"];
                                                $cr_joined_date = $row["created_date_time"];
                                                $cr_user_type = $row["user_type"];
                                                $cr_user_status = $row["status"];
                                                $cr_editable = $row["editable"];
                                                $rows["branch_id"] = $row["branch_id"];

                                            }
                                        }

                                        ?>

                <div class="row">
                    <div class="col-md-4">
                        <img width="250" height="260" class="img-responsive img-rounded" src="<?php if(file_exists("pdb/users/user_id_".$user_id.".jpg") || file_exists("pdb/users/user_id_".$user_id.".jpeg") || file_exists("pdb/users/user_id_".$user_id.".png") || file_exists("pdb/users/user_id_".$user_id.".gif")){
                            echo base_url("pdb/users/user_id_".$user_id);
                            if(file_exists("pdb/users/user_id_".$user_id.".jpg"))
                                echo ".jpg";
                            elseif(file_exists("pdb/users/user_id_".$user_id.".jpeg"))
                                echo ".jpeg";
                            elseif(file_exists("pdb/users/user_id_".$user_id.".png"))
                                echo ".png";
                            elseif(file_exists("pdb/users/user_id_".$user_id.".gif"))
                                echo ".gif";

                        }
                        else {
                            echo base_url("pdb/users/demo.jpg");
                        }?>">
                        <p><strong style="color:lightslategray" >Status :</strong> <?php
                            echo $this->m_functions->user_status($cr_user_status);
                            ?>
                       </p>

                        </div>
                    <br>
                    <div class="col-md-8">
                        <table class="table table-responsive " >
                            <tr>
                                <td><h2><strong style="color:lightslategray">Name</strong></h2> </td>
                                <td><h2><?php echo $cr_first_name." ".$cr_last_name ?></h2></td>
                            </tr>
                            <tr>
                                <td><h2><strong style="color:lightslategray">User Name</strong></h2> </td>
                                <td><h2><?php echo $cr_user_name; ?></h2></td>
                            </tr>
                            <tr>
                                <td><h2><strong style="color:lightslategray">Email</strong></h2></td>
                                <td><h2><a href="mailto: <?php echo $cr_email; ?>"> <?php echo $cr_email; ?>
                                        </a></h2></td>
                            </tr>
                            <tr>
                                <td><h2><strong style="color:lightslategray">Joined Date<strong</h2></td>
                                <td><h2><?php echo $cr_joined_date; ?></h2></td>
                            </tr>
                        </table>
                        </div>
                 </div>
                <div class="row">
                    <div class="col-md-12">
                    <table class="table table-responsive">
                        <tr>
                            <td><h2><strong style="color:lightslategray">User Type</strong></h2></td>
                            <td><h2><?php
                                    echo $this->m_functions->user_type($cr_user_type)
                                    ?></h2></td>
                            <td><h2><strong style="color:lightslategray">Branch </strong></h2></td>
                            <td><h2><?php
                                    echo ($rows["branch_id"]=="-1")? "All" :$this->m_common->select_signle_field_and_row("branches", array("branch_id"=>$rows["branch_id"]),"branch_name");
                                    ?></h2></td>
                        </tr>
                    </table>
                    </div>
                </div>
                    <hr>
                    <h3 class="text-align-center text-justify text-capitalize"><strong style="color:lightslategray">About me</strong></h3>
                    <div class="row">
                    <div class="col-md-10 col-md-offset-2">

                      <blockquote>
                           <?php echo $cr_about_me; ?>
                       </blockquote>
                    </div>
                    </div>

 
                                        


                                    </fieldset>





                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <a href="<?php echo site_url("super/user/del/".$user_id); ?>"
                                                   class="btn
                                                btn-danger btn-lg <?php echo ($cr_editable==0)? "disabled":""; ?>"> Delete</a>
                                                <a href="<?php echo site_url("super/user/edit/".$user_id); ?>"
                                                   class="btn
                                                btn-warning btn-lg <?php echo ($cr_editable==0)? "disabled":""; ?>"> Edit</a>

                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>




                <!-- WIDGET END -->

            </div>











        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->





</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
