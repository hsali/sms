<?php $this->load->view("admin/header"); ?>

    <!-- Left panel : Navigation area -->
    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
    <!-- END NAVIGATION -->

    <!-- MAIN PANEL -->
    <div id="main" role="main">

        <!-- RIBBON -->
        <?php $this->load->view("admin/headers/ribbon"); ?>
        <!-- END RIBBON -->

        <!-- MAIN CONTENT -->
        <div id="content">

            <?php
            // ribbon 2 contain the summary of page.
            $this->load->view("admin/headers/ribbon2"); ?>

            <!-- widget grid -->
            <section id="widget-grid" class="">

                <?php $this->load->view("admin/message_box"); ?>

                <!-- row -->
                <div class="row">

                    <!-- NEW WIDGET START -->
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget jarviswidget-color-yellow" id="wid-id-3" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false" data-widget-collapse="false">
                            <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"
                                    data-widget-sortable="false"

                                    -->
                            <header>
                                <span class="widget-icon"> <i class="fa fa-users"></i> </span>
                                <h2><?php if(isset($widget_title)) echo $widget_title;
                                          else echo "User Management";
                                    ?></h2>

                            </header>

                            <!-- widget div-->
                            <div>

                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->

                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding">

                                    <table id="datatable_tabletools" class="table  table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th data-hide="phone">User ID</th>
                                            <th data-class="expand">User Name</th>
                                            <th data-hide="phone">Name</th>
                                            <th data-hide="phone">Branch</th>
                                            <th data-hide="phone,tablet">Status</th>
                                            <th data-hide="phone,tablet">User type</th>
                                            <th data-hide="phone,tablet">Email</th>
                                            <th data-hide="phone,tablet">Joined Dated</th>
                                        </tr>
                                        </thead>
                                       <tbody>
                                       <?php foreach($view_users as $rows): ?>
                                           <tr id="user_id_<?php  echo $rows["user_id"]; ?>">
                                               <td><?php  echo $rows["user_id"]; ?> </td>
                                               <td><a  href="<?php echo site_url("super/user/profile/"
                                                       .$rows["user_id"]);
                                                   ?>"><?php  echo $rows["user_name"];
                                                       ?></a></td>
                                               <td><?php  echo $rows["first_name"]." ".$rows["last_name"]; ?></td>
                                               <td><?php  echo ($rows["branch_id"]=="-1")? "All" :$this->m_common->select_signle_field_and_row("branches", array("branch_id"=>$rows["branch_id"]),"branch_name"); ?></td>
                                               <td><?php  echo $this->m_functions->user_status($rows["status"]); ?></td>
                                               <td><?php  echo $this->m_functions->user_type($rows["user_type"]); ?></td>
                                               <td><?php  echo $rows["email"]; ?></td>
                                               <td><?php  echo $rows["created_date_time"]; ?></td>
                                           </tr>
                                       <?php endforeach; ?>
                                       </tbody>
                                    </table>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end widget div -->

                        </div>
                        <!-- end widget -->

                    </article>
                    <!-- WIDGET END -->

                </div>

                <!-- end row -->

                <!-- end row -->

            </section>
            <!-- end widget grid -->

        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->


    <!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
