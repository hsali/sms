<?php $this->load->view("admin/header"); ?>

    <!-- Left panel : Navigation area -->
    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
    <!-- END NAVIGATION -->

    <!-- MAIN PANEL -->
    <div id="main" role="main">

        <!-- RIBBON -->
        <?php $this->load->view("admin/headers/ribbon"); ?>
        <!-- END RIBBON -->

        <!-- MAIN CONTENT -->
        <div id="content">

            <?php
            // ribbon 2 contain the summary of page.
            $this->load->view("admin/headers/ribbon2"); ?>

            <!-- widget grid -->
            <section id="widget-grid" class="">

                <?php $this->load->view("admin/message_box"); ?>

                <!-- row -->
                <div class="row">

                    <!-- NEW WIDGET START -->
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget jarviswidget-color-greenLight" id="wid-id-3" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-collapsed="false" data-widget-deletebutton="false" data-widget-togglebutton="false">
                            <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"
                                    data-widget-sortable="false"

                                    -->
                            <header>
                                <span class="widget-icon"> <i class="fa fa-book"></i> </span>
                                <h2><?php if(isset($widget_title)) echo $widget_title; else echo "Fee Management"; ?></h2>

                            </header>

                            <!-- widget div-->
                            <div>

                                <!-- widget edit box -->
                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->

                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding">

                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th data-hide="phone" title="Receipt ID">ID</th>
                                            <th data-class="expand" title="Staff ID">SID</th>
                                            <th data-class="phone,tablet" title="Staff Name">Name</th>
                                            <th data-class="phone,tablet" title="Type">Type</th>
                                            <th data-hide="phone">Value</th>
                                            <th data-hide="phone">Action</th>
                                        </tr>
                                        </thead>
                                       <tbody>
                                       <?php foreach($view_data as $rows): ?>
                                           <tr id="ss_id_<?php  echo $rows["ss_id"]; ?>">
                                               <td><?php  echo $rows["ss_id"]; ?></td>
                                               <td><?php  echo $rows["staff_id"]; ?></td>
                                               <td><?php  echo $rows["name"];?></td>
                                               <td><?php  echo $rows["var_value"];?></td>
                                               <td><?php  echo $rows["salary_type_value"];?></td>
                                               <td >
                                               <a href="<?php echo site_url("admin/settings/salary/staff/edit")
                                                   ."/".$rows["ss_id"]; ?>" class="<?php if($rows["editable"]=="1") echo "";else echo "disabled"; ?> btn btn-warning"><span class="fa fa-edit"></span> Edit</a>
                                               <a class="<?php if($rows["editable"]=="1") echo "";else echo "disabled"; ?> btn btn-danger" href="<?php echo site_url
                                                   ("admin/settings/salary/staff/delete") ."/".$rows["ss_id"];
                                               ?>"><span class="fa fa-times-circle"></span> Delete</a>
                                               </td>
                                           </tr>
                                       <?php endforeach; ?>
                                       </tbody>
                                    </table>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end widget div -->

                        </div>
                        <!-- end widget -->

                    </article>
                    <!-- WIDGET END -->

                </div>

                <!-- end row -->

                <!-- end row -->

            </section>
            <!-- end widget grid -->

        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->


    <!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
