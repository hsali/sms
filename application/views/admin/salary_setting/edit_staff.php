<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>


            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-0"
                         data-widget-editbutton="false"
                         role="widget" data-widget-colorbutton="false" data-widget-collopse="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!--                                <a href="javascript:void(0);"-->
                                <!--                                                                           class="button-icon jarviswidget-toggle-btn"-->
                                <!--                                                                           rel="tooltip" title=""-->
                                <!--                                                                           data-placement="bottom"-->
                                <!--                                                                           data-original-title="Collapse"><i-->
                                <!--                                        class="fa fa-minus "></i></a> -->
                                <a href="javascript:void(0);"
                                   class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom"
                                   data-original-title="Fullscreen"><i
                                        class="fa fa-expand "></i></a>
                                <!--                                <a href="javascript:void(0);"-->
                                <!--                                                                          class="button-icon jarviswidget-delete-btn"-->
                                <!--                                                                          rel="tooltip" title="" data-placement="bottom"-->
                                <!--                                                                          data-original-title="Delete"><i-->
                                <!--                                        class="fa fa-times"></i></a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-money "></i> </span>

                            <h2><?php echo isset($widget_title)? $widget_title: "Salary Management"; ?></h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">


                                <form class="form-horizontal" method="post" enctype="multipart/form-data">

                                    <fieldset>
                                        <legend><?php echo isset($form_legend_title)? $form_legend_title: "Staff Salary Setting"; ?></legend>
                                        <?php

                                        if(!empty($edit_data)){
                                            foreach($edit_data as $rows){
                                                $cr_staff_id=$rows["staff_and_designations"];
                                                $cr_salary_type=$rows["salary_type"];
                                                $cr_salary_type_value=$rows["salary_type_value"];
                                            }
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="col-md-5   ">
                                                <div
                                                    class="form-group form-group-lg  <?php if (form_error("staff_id")) echo "has-error"; elseif (!empty($staff_id)) echo "has-success" ?>">
                                                    <label class="col-md-3 control-label">Staff ID*</label>

                                                    <div class="col-md-9">
                                                        <input class="form-control" required placeholder="Choose Staff" id="staff_id" name="staff_id" list="staff_names"  type="text" value="<?php
                                                        if(isset($staff_id)) echo $staff_id;
                                                        elseif(isset($cr_staff_id)) echo $cr_staff_id;
                                                        ?>" autocomplete="off">
                                                        <?php
                                                        $a_staffs=$this->m_common->select_staffs($this->session->userdata("branch_id"));
                                                        ?>
                                                        <datalist id="staff_names">
                                                            <?php if(isset($a_staffs)){
                                                                foreach($a_staffs as $rows){
                                                                    ?>
                                                                    <option value="<?php echo $rows["staff_id"]; ?>" ><?php echo $rows["first_name"]." ".$rows["last_name"]; ?></option>

                                                                    <?php    /* */
                                                                }
                                                            } /*  */?>
                                                        </datalist>
                                                        <span class="help-Block"><?php echo form_error("staff_id");?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group-lg col-md-5">
                                                <div class="form-group <?php
                                                if (form_error("salary_type"))
                                                    echo "has-error";
                                                elseif (!empty($salary_type))
                                                    echo
                                                    "has-success"
                                                ?>">

                                                    <div class="col-md-12">

                                                        <select class="form-control" id="salary_type"
                                                                name="salary_type">
                                                            <option value="" selected="" disabled="">
                                                                Select Fee Category
                                                            </option>
                                                            <?php
                                                            foreach ($a_salary_type as $rows) {
                                                                $key = $rows["var_vid"];
                                                                $value = $rows["var_value"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($salary_type) && $salary_type == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                elseif (isset($cr_salary_type) && $cr_salary_type == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("salary_type");
                                                            ?></span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class=" form-group-lg col-md-5">
                                                <input class="form-control" id="salary_type_value"
                                                       name="salary_type_value"
                                                       placeholder="Type Value" type="number"
                                                       value="<?php
                                                       if (isset($salary_type_value)) echo $salary_type_value;
                                                       elseif (isset($cr_salary_type_value)) echo $cr_salary_type_value;
                                                       ?>">
                                                            <span class="help-Block"><?php echo form_error("salary_type_value"); ?>
                                                                type +ve value for allowance and negative value for deductions  </span>
                                            </div>

                                        </div>

                                    </fieldset>


                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset
                                                </button>
                                                <button type="submit" name="submit" id="submit" class="btn
                                              btn-success btn-lg">Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


//$this->load->view("admin/footers/footer_datatables");



// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
