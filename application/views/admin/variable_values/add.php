<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
                $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-0"  data-widget-editbutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>
                            <h2>Entities Management</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal"  method="post">

                                    <fieldset>
                                        <legend>Add Value to Any Entity</legend>

                                        <!--     Variable Name -->
                                        <div class="form-group form-group-lg <?php if (form_error("var_id"))
                                            echo "has-error"; elseif (!empty($var_id)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Variable Name</label>

                                            <div class="col-md-4">

                                                <select class="form-control" id="var_id" name="var_id">
                                                    <option value="" selected="" disabled="">
                                                        Select Variable
                                                    </option>
                                                    <?php foreach ($a_var_name as $rows) {
                                                        $key=$rows["var_id"];
                                                        $value=$rows["var_name"];
                                                        echo "<option label='{$value}' value='{$key}'";
                                                        if (isset($var_id) && $var_id == $key) {
                                                            echo 'selected="selected"';
                                                        }
                                                        echo ">" . $value . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                                  <span class="help-Block"><?php echo form_error("var_id");
                                                      ?></span>
                                            </div>
                                        </div>
<!-- vartiable title  -->
                                        <div class="form-group <?php if(form_error("var_value"))
                                            echo "has-error";
                                        elseif(! empty($var_value)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Variable Value*</label>
                                            <div class="col-md-10">
                                                <input class="form-control" id="var_value" name="var_value"
                                                       placeholder="Type value of variable" required=""
                                                       type="text"
                                                       value="<?php if(isset($var_value)) echo $var_value; ?>">
                                                 <span class="help-Block"><?php echo form_error("var_value");
                                                     ?></span>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group <?php if(form_error("variable_value"))
                                            echo "has-error";
                                        elseif(! empty($variable_value)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Variable Value</label>
                                            <div class="col-md-10">
                                                <input class="form-control" id="variable_value" name="variable_value"
                                                       placeholder="Type value of variable"
                                                       type="text"
                                                       value="<?php if(isset($variable_value)) echo $variable_value; ?>">
                                                 <span class="help-Block">Optional: It is for those variables who has sub variables and values. <a href="javascript:void(0);" class="label label-info" rel="popover" data-placement="top" data-original-title="Help" data-content="For example. Variable: Allowances, Sub-Variable:Food Allowance,Value:1000Rs etc."><i class="fa fa-question-circle"></i> <strong>more</strong></a> <?php echo form_error("variable_value");
                                                     ?></span>
                                            </div>
                                        </div>
<div class="row">



</div>

                                    </fieldset>





                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-danger" type="reset" id="reset" name="reset">
                                                    Reset
                                                </button>
                                                <button class="btn btn-success" type="submit" name="submit" id="submit">
                                                    <i class="fa fa-save"></i>
                                                    Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>




                <!-- WIDGET END -->

            </div>

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->





    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
