<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false"
                         role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i>-->
                                <!--                                </a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i
                                        class="fa fa-expand "></i></a>
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-bank "></i> </span>

                            <h2><?php echo isset($widget_title) ? $widget_title : "Program Fee management" ?></h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal" method="post">

                                    <fieldset>
                                        <legend><?php echo isset($form_legend_title) ? $form_legend_title : "Add Program Fee" ?></legend>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group  <?php
                                                if (form_error("fc_id"))
                                                    echo "has-error";
                                                elseif (!empty($fc_id))
                                                    echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Fee Category*</label>

                                                    <div class="col-md-8">
                                                        <select required="" class="form-control" id="fc_id"
                                                                name="fc_id">
                                                            <option value="" selected="" disabled="">
                                                                Select Fee Category
                                                            </option>
                                                            <?php foreach ($a_fee_category as $rows) {
                                                                $key = $rows["var_vid"];
                                                                $value = $rows["var_value"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($fc_id) && $fc_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                  <span class="help-Block"><?php echo form_error("fc_id");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group <?php
                                                if (form_error("fee"))
                                                    echo "has-error";
                                                elseif (!empty($fee))
                                                    echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Fee*</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="fee" name="fee"
                                                               placeholder="Type Fee" required="" type="text"
                                                               value="<?php if (isset($fee)) echo $fee; ?>">
                                                        <span class="help-Block"><?php echo form_error("fee"); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group  <?php
                                                if (form_error("class_id"))
                                                    echo "has-error";
                                                elseif (!empty($class_id))
                                                    echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Class*</label>

                                                    <div class="col-md-8">
                                                        <select required="" class="form-control" id="class_id"
                                                                name="class_id">
                                                            <option value="" selected="" disabled="">
                                                                Select Class
                                                            </option>
                                                            <?php foreach ($a_classes as $rows) {
                                                                $key = $rows["class_id"];
                                                                $value = $rows["class_name"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($class_id) && $class_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                  <span class="help-Block"><?php echo form_error("class_id");
                                                      ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group <?php
                                                if (form_error("period"))
                                                    echo "has-error";
                                                elseif (!empty($period))
                                                    echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Period*</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="period" name="period"
                                                               placeholder="Type Number of month " required=""
                                                               type="number"
                                                               value="<?php if (isset($period)) echo $period; ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("period"); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </fieldset>


                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-danger" type="reset" id="reset" name="reset">
                                                    Reset
                                                </button>
                                                <button class="btn btn-success" type="submit" name="submit" id="submit">
                                                    <i class="fa fa-save"></i>
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
