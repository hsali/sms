<html>
<head>
<title>EMS: Print the Admission Form</title>
<style>
.formp-print {
	/*size: 7in 9.25in;*/
	width: 7in;
	height: 9.25in;
	margin: 0.125in;
	padding: 0.125in;
	border: 0.010in black solid;
	background-color: #e5e5e5;
}

.formp-header {
	width: 7in;
	height: 0.5in;
	border-radius: 10px;
	background-color: #00ff00;
}

.formp-footer {
	display: block;
	background-color: #008DC4;
	width: 7in;
	height: 1.5in;
	border-width: 2px;
	border-style: solid;
	border-color: gray;
	border-radius: 5px;
	position: absolute;
	top: 7.75in;
}

p.field {
	display: inline;
	font-size: 14px;
	margin-left: 3%;
	margin-right: 3%;
}

p.value {
	display: inline;
	font-size: 13px;
	text-decoration-line: underline;
	margin-left: 3%;
	margin-right: 3%;
}

p.field+p.value {
	margin-left: 5px;
}

.profile-pic {
	widht: 150px;
	height: 150px;
	border-style: solid;
	border-color: gainsboro;
	display: inline;
	border-radius: 100%;
}

.img-circle {
	border-radius: 100%;
}

.row {
	display: block;
	widht: 100%;
}

.col-4 {
	display: inline;
	width: 2.25in;
	height: auto;
}

div {
	display: inherit;
}

.col-8 {
	display: inline;
	width: 4.5in;
}

.row-inline {
	display: inline;
}

.row-block {
	display: block;
}

.col-6 {
	widht: 50%;
	display: inline;
}

.col-right-fields {
	width: 2.25in;
	display: inline;
}

div .up-pos {
	position: relative;
	top: 5px;
}

th {
	width: 15%;
	border: 2px #AAAAAA solid;
	border-collapse: collapse;
}
</style>
</head>

<body>
            <?php
												$a_domicile = $this->config->item ( 'domicile' );
												$a_gender = $this->config->item ( 'gender' );
												$a_religion = $this->config->item ( 'religion' );
												$a_country = $this->config->item ( 'country' );
												$a_city = $this->config->item ( 'city' );
												$a_guardian_type = $this->config->item ( 'guardian_type' );
												$a_guardian_status = $this->config->item ( 'guardian_status' );
												$a_degree_types = $a_guardian_education = $this->config->item ( 'degree_type' );
												$a_enrollment_status = $this->config->item ( 'enrollment_status' );
												?>
                                    <?php
																																				if (isset ( $data_pi )) {
																																					foreach ( $data_pi as $row ) {
																																						$student_id = $row->student_id;
																																						$cr_first_name = $row->first_name;
																																						$cr_last_name = $row->last_name;
																																						$cr_gender = $row->gender;
																																						$cr_dob = $row->dob;
																																						$cr_cnic = $row->cnic;
																																						$cr_religion = $row->religion;
																																						$cr_domicile = $row->domicile;
																																						$cr_caste = $row->caste;
																																						$cr_tnbs = $row->tnbs;
																																						$cr_pbse = $row->pbse;
																																						$cr_email = $row->email;
																																						$cr_cell_no = $row->cell_no;
																																						$cr_country = $row->country;
																																						$cr_city = $row->city;
																																						$cr_other_city = $row->other_city;
																																						$cr_town = $row->town;
																																						$cr_permanent_address = $row->permanent_address;
																																						$cr_present_address = $row->present_address;
																																					}
																																				}
																																				if (isset ( $data_gi )) {
																																					foreach ( $data_gi as $row ) {
																																						$cr_guardian_type = $row->guardian_type;
																																						$cr_guardian_name = $row->guardian_name;
																																						$cr_guardian_cnic = $row->guardian_cnic;
																																						$cr_guardian_occupation = $row->guardian_occupation;
																																						$cr_guardian_status = $row->guardian_status;
																																						$cr_guardian_education = $row->guardian_education;
																																						$cr_guardian_phone = $row->guardian_phone;
																																						$cr_guardian_email = $row->guardian_email;
																																						$cr_guardian_cellno1 = $row->guardian_cellno1;
																																						$cr_guardian_cellno2 = $row->guardian_cellno2;
																																					}
																																				}
																																				if (isset ( $data_ai )) {
																																					foreach ( $data_ai as $row ) {
																																						$cr_a_degree_type = $row->a_degree_type;
																																						$cr_a_class_name = $row->a_class_name;
																																						$cr_a_institute = $row->a_institute;
																																						$cr_a_obtained_marks = $row->a_obtained_marks;
																																						$cr_a_total_marks = $row->a_total_marks;
																																						$cr_a_starting_date = $row->a_starting_date;
																																						$cr_a_ending_date = $row->a_ending_date;
																																					}
																																				}
																																				if (isset ( $data_ei )) {
																																					foreach ( $data_ei as $row ) {
																																						$cr_branch_id = $row->branch_id;
																																						$cr_dor = $row->dor;
																																						$cr_class_id = $row->class_id;
																																						$cr_section_id = $row->section_id;
																																						$cr_fee = $row->fee;
																																						$cr_enrollment_status = $row->enrollment_status;
																																					}
																																				}
																																				?>
        <div class="formp-print">
		<div class="formp-header"></div>
		<div class="formp-body">
			<div class="formp-box">

				<div class="row" style="height: 180px;">
					<div class="col-4">
						<img class="profile-pic img-circle"
							src="<?php echo base_url("pdb/students/student_id_8.jpg") ?>">
					</div>
					<div class="col-8"
						style="position: relative; top: -130px; left: 170px">
						<div class=" row-inline row-block ">
							<div class="col-right-fields">
								<p class="field">Name</p>
								<p class="value"><?php echo $cr_first_name." ".$cr_last_name; ?></p>
							</div>
							<div class="col-right-fields">
								<p class="field">Caste</p>
								<p class="value"><?php echo $cr_caste; ?></p>
							</div>
						</div>
						<div class=" row-inline row-block ">
							<div class="col-6">
								<p class="field">Gender</p>
								<p class="value"><?php
								foreach ( $a_gender as $key => $value ) {
									if (! empty ( $cr_gender ) && $cr_gender == $key)
										echo $value;
								}
								?></p>
							</div>
							<div class="col-6">
								<p class="field">DOB</p>
								<p class="value"><?php echo $cr_dob; ?></p>
							</div>
						</div>
						<div class=" row-inline row-block ">
							<div class="col-6">
								<p class="field">CNIC #</p>
								<p class="value"><?php echo $cr_cnic; ?></p>
							</div>
							<div class="col-6">
								<p class="field">Domicile</p>
								<p class="value"><?php
								foreach ( $a_domicile as $key => $value ) {
									if (! empty ( $cr_domicile ) && $cr_domicile == $key)
										echo $value;
								}
								?></p>
							</div>
						</div>
						<div class=" row-inline row-block ">
							<div class="col-6">
								<p class="field">TNBS</p>
								<p class="value"><?php echo $cr_tnbs; ?></p>
							</div>
							<div class="col-6">
								<p class="field">PBS</p>
								<p class="value"><?php echo $cr_pbse; ?></p>
							</div>
						</div>
						<div class=" row-inline row-block ">
							<div class="col-6">
								<p class="field">Cell No</p>
								<p class="value"><?php echo $cr_cell_no; ?></p>
							</div>
							<div class="col-6">
								<p class="field">Email</p>
								<p class="value"><?php echo $cr_email; ?></p>
							</div>
						</div>
						<div class=" row-inline row-block ">
							<div class="col-6">
								<p class="field">Religion</p>
								<p class="value"><?php
								foreach ( $a_religion as $key => $value ) {
									if (! empty ( $cr_religion ) && $cr_religion == $key)
										echo $value;
								}
								?></p>
							</div>
							<div class="col-6">
								<p class="field">Town</p>
								<p class="value"><?php echo $cr_town; ?></p>
							</div>
						</div>
						<div class=" row-inline row-block ">
                            <?php
																												if (empty ( $cr_other_city )) { //
																													?>
                            <div class="col-6">
								<p class="field">City</p>
								<p class="value"><?php
																													foreach ( $a_city as $key => $value ) {
																														if (! empty ( $cr_city ) && $cr_city == $key)
																															echo $value;
																													}
																													?></p>
							</div>
                                <?php     /* */    }else{    // ?>
                                <div class="col-6">
								<p class="field">City</p>
								<p class="value"><?php
																													echo $cr_other_city;
																													?></p>
							</div>

                                <?php     /* */ }    // ?>

                            <div class="col-6">
								<p class="field">Country</p>
								<p class="value"><?php
								foreach ( $a_country as $key => $value ) {
									if (! empty ( $cr_country ) && $cr_country == $key)
										echo $value;
								}
								?></p>
							</div>

						</div>

					</div>
				</div>
				<div class="row">
					<p class="field">Present Address:</p>
					<p class="value"><?php  echo $cr_present_address; ?></p>
				</div>
				<div class="row">
					<p class="field">Permanent Address:</p>
					<p class="value"><?php  echo $cr_present_address;  ?></p>
				</div>

			</div>
			<div class="formp-box">
				<h3>Guardian Information</h3>
				<table>
					<thead>
						<th>Guardian</th>
						<th>Name</th>
						<th>Occupation</th>
						<th>Phone</th>
						<th>Cell</th>
						<th>Email</th>
					</thead>
					<tbody>
						<tr>
							<td><?php
							foreach ( $a_guardian_type as $key => $value ) {
								if (! empty ( $cr_guardian_type ) && $cr_guardian_type == $key)
									echo $value;
							}
							?></td>
							<td><?php echo $cr_guardian_name; ?></td>
							<td><?php echo $cr_guardian_occupation; ?></td>
							<td><?php echo $cr_guardian_phone; ?></td>
							<td><?php echo $cr_guardian_cellno1; ?></td>
							<td><?php echo $cr_guardian_email; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="formp-box">
				<h3>Academic Information</h3>
				<table>
					<thead>
						<th>Institute</th>
						<th>Marks</th>
						<th>Starting Date</th>
						<th>Ending Date</th>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $cr_a_institute; ?></td>
							<td><?php echo (($cr_a_obtained_marks/$cr_a_total_marks)*100)."%"; ?></td>
							<td><?php echo $cr_a_starting_date; ?></td>
							<td><?php echo $cr_a_ending_date; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="formp-box">
				<h3>Enrollment Information</h3>
				<div class=" row row-block ">
					<div class="col-4">
						<p class="field">Class</p>
						<p class="value"><?php
						foreach ( $a_classes as $rows ) {
							$key = $rows ["class_id"];
							$value = $rows ["class_name"];
							if (! empty ( $cr_class_id ) && $cr_class_id == $key)
								echo $value;
						}
						?></p>
					</div>
					<div class="col-4">
						<p class="field">Section</p>
						<p class="value"><?php
						foreach ( $a_sections as $rows ) {
							$key = $rows ["section_id"];
							$value = $rows ["section_name"];
							if (! empty ( $cr_section_id ) && $cr_section_id == $key)
								echo $value;
						}
						?></p>
					</div>
					<div class="col-4">
						<p class="field">DOR</p>
						<p class="value"><?php
						echo $cr_dor;
						?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="formp-footer"></div>
	</div>
</body>
</html>