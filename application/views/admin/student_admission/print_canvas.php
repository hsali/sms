<html>
<head>
    <title>Print form</title>

    <link rel="stylesheet" type="text/css"  href="<?php echo base_url("media/admin_panel/css/bootstrap.min.css"); ?>">
    <script src="<?php echo base_url(); ?>media/html2canvas/html2canvas.min.js"></script>
    <script src="<?php echo base_url(); ?>media/html2canvas/html2canvas.svg.min.js"></script>


</head>
<body id="page">
<h1>Hello Body </h1>
<div class="container " id="printing">
    <div class="row ">
        <ol class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li class="active">Link</li>
        </ol>

    </div>
    <span class="label label-danger">Label</span>


    <h1>h1. SmartAdmin heading <small>Secondary text</small></h1>

    <span>Hello</span>
    <h2>h2. SmartAdmin heading <small>Secondary text</small></h2>
    <h3>h3. SmartAdmin heading <small>Secondary text</small></h3>
    <h4>h4. SmartAdmin heading <small>Secondary text</small></h4>
    <h5>h5. SmartAdmin heading <small>Secondary text</small></h5>
    <h6>h6. SmartAdmin heading <small>Secondary text</small></h6>


</div>

<script src="<?php echo base_url(); ?>media/admin_panel/js/libs/jquery-2.0.2.min.js"></script>
<script src="<?php echo base_url(); ?>media/admin_panel/js/bootstrap/bootstrap.min.js"></script>
<script>
    html2canvas(document.body, {
        onrendered: function(canvas) {
//            $("#page").hide();
            document.body.appendChild(canvas);
            window.print();
//            $('canvas').remove();
//            $("#page").show();
        }
    });
</script>

</body>
</html>