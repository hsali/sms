<?php
//$a_branch=$this->config->item('branches');
$a_domicile = $this->config->item('domicile');
$a_gender = $this->config->item('gender');
$a_religion = $this->config->item('religion');
$a_country = $this->config->item('country');
$a_city = $this->config->item('city');
$a_guardian_type = $this->config->item('guardian_type');
$a_guardian_status = $this->config->item('guardian_status');
$a_degree_types = $a_guardian_education = $this->config->item('degree_type');
//$a_sections=$this->config->item('sections');
//$a_classes=$this->config->item('classes');
$a_enrollment_status = $this->config->item('enrollment_status');
?>
<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2");
        ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>


            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-0"
                         data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false"
                         data-widget-togglebutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse">-->
                                <!--                                    <i class="fa fa-angle-down "></i>-->
                                <!--                                </a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                    <i class="fa fa-expand "></i>
                                </a>
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete">-->
                                <!--                                    <i class="fa fa-times"></i>-->
                                <!--                                </a>-->
                            </div>

                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>

                            <h2>Students Management</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal" method="post" enctype="multipart/form-data">

                                    <fieldset>
                                        <legend>Personal Information</legend>
                                        </br>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("first_name"))
                                                        echo "has-error";
                                                    elseif (!empty($first_name))
                                                        echo "has-success"
                                                    ?>">
                                                    <label class="col-md-4 control-label">First Name</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="first_name" name="first_name"
                                                               placeholder="Type First Name" type="text"
                                                               value="<?php if (isset($first_name)) echo $first_name; ?>">
                                                        <span class="help-Block"><?php echo form_error("first_name");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Last Name-->
                                                <div
                                                    class="form-group <?php if (form_error("last_name"))
                                                        echo "has-error";
                                                    elseif (!empty($last_name))
                                                        echo "has-success"
                                                    ?>">
                                                    <label class="col-md-4 control-label">Last Name</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="last_name" name="last_name"
                                                               placeholder="Type Last Name" type="text"
                                                               value="<?php if (isset($last_name)) echo $last_name; ?>">
                                                        <span class="help-Block"><?php echo form_error("last_name");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("cnic"))
                                                    echo "has-error";
                                                elseif (!empty($cnic))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">CNIC No</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="cnic" name="cnic"
                                                               placeholder="Type CNIC Number" type="text"
                                                               data-mask="99999-9999999-9" data-placeholder="X"
                                                               value="<?php if (isset($cnic)) echo $cnic; ?>">
                                                        <span class="help-Block"><?php echo form_error("cnic");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Last Name-->
                                                <div class="form-group <?php
                                                if (form_error("domicile"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($domicile))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Domicile</label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="domicile" name="domicile">
                                                            <option value="" selected="" disabled="">Select
                                                                Domicile
                                                            </option>
                                                            <?php
                                                            foreach ($a_domicile as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($domicile) && $domicile == $key) {
                                                                    echo
                                                                    'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("domicile");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("gender"))
                                                    echo "has-error";
                                                elseif (!empty($gender))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Gender</label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="gender" name="gender">
                                                            <option value="" selected="" disabled="">Select Gender
                                                            </option>
                                                            <?php
                                                            foreach ($a_gender as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($gender) && $gender == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("gender");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("dob")) echo "has-error"; elseif (!empty($dob)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Date of Birth</label>

                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                            <input class="form-control"
                                                                   placeholder="Choose Date of Birth" id="dob"
                                                                   data-mask="9999-99-99" data-placeholder="*"
                                                                   name="dob" type="text"
                                                                   value="<?php if (isset($dob)) echo $dob; ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <span class="help-Block"><?php echo form_error("dob");
                                                            ?></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("caste"))
                                                    echo "has-error";
                                                elseif (!empty($caste))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Caste</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="caste" name="caste" type="text"
                                                               placeholder="Type Caste"
                                                               value="<?php if (isset($caste)) echo $caste; ?>">
                                                        <span class="help-Block"><?php echo form_error("caste");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("religion"))
                                                        echo "has-error";
                                                    elseif (!empty($religion))
                                                        echo "has-success"
                                                    ?>">
                                                    <label class="col-md-4 control-label">Religion</label>

                                                    <div class="col-md-8">

                                                        <select class="form-control" id="religion" name="religion">
                                                            <option value="" selected="" disabled="">
                                                                Select Religion
                                                            </option>
                                                            <?php
                                                            foreach ($a_religion as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($religion) && $religion == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("religion");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("tnbs"))
                                                    echo "has-error";
                                                elseif (!empty($tnbs))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Total Family Members</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="tnbs" name="tnbs" type="text"
                                                               placeholder="Type Total No brothers and sisters"
                                                               value="<?php if (isset($tnbs)) echo $tnbs; ?>">
                                                        <span class="help-Block"><?php echo form_error("tnbs");
                                                            ?>Note: Total Brothers and sisters</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("pbse"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($pbse))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Position in Family</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="pbse" name="pbse" type="text"
                                                               placeholder="Type your position in brothers and sisters"
                                                               value="<?php if (isset($pbse)) echo $pbse; ?>">
                                                        <span class="help-Block"><?php echo form_error("pbse");
                                                            ?>Note: Position in brothers and sisters</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--email and cell no -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("cell_no"))
                                                        echo "has-error";
                                                    elseif (!empty($cell_no))
                                                        echo "has-success"
                                                    ?>">
                                                    <label class="col-md-4 control-label">Cell No</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="cell_no" name="cell_no"
                                                               placeholder="Type Cell No"
                                                               type="text"
                                                               value="<?php if (isset($cell_no)) echo $cell_no; ?>">
                                                        <span class="help-Block"><?php echo form_error("cell_no");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("email"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($email))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Email</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="email" name="email" type="text"
                                                               placeholder="Type Email"
                                                               value="<?php if (isset($email)) echo $email; ?>">
                                                        <span class="help-Block"><?php echo form_error("email");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--country and city -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("country"))
                                                        echo "has-error";
                                                    elseif (!empty($country))
                                                        echo "has-success"
                                                    ?>">
                                                    <label class="col-md-4 control-label">Country </label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="country" name="country">
                                                            <option value="" selected="" disabled="">Select Country
                                                            </option>
                                                            <?php
                                                            foreach ($a_country as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($country) && $country == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("country");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("city"))
                                                    echo "has-error";
                                                elseif (!empty($city))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">City </label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="city" name="city">
                                                            <option value="" selected="" disabled="">
                                                                Select City
                                                            </option>
                                                            <?php
                                                            foreach ($a_city as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($city) && $city == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("city");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("other_city"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($other_city))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Other City </label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="other_city" name="other_city"
                                                               placeholder="Type City Name"
                                                               type="text" value="<?php
                                                        if (isset($other_city))
                                                            echo $other_city;
                                                        ?>">
                                                        <span class="help-Block"><?php echo form_error("other_city");
                                                            ?>Note: If city selected is "Other".</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("town"))
                                                    echo "has-error";
                                                elseif (!empty($town))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Town </label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="town" name="town" type="text"
                                                               placeholder="Type Town name"
                                                               value="<?php if (isset($town)) echo $town; ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("town"); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                        permanent address -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group <?php
                                                if (form_error("permanent_address"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($permanent_address))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-2 control-label">Permanent Address</label>

                                                    <div class="col-md-10">
                                                        <textarea id="permanent_address" name="permanent_address"
                                                                  class="form-control"
                                                                  placeholder="Type Address House No# , Street # , Town,City, Postal Code, Country"
                                                                  rows="3"><?php if (isset($permanent_address))
                                                                echo
                                                                $permanent_address;
                                                            ?></textarea>
                                                        <span
                                                            class="help-Block"><?php echo form_error("permanent_address");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--                                        present address -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group <?php
                                                if (form_error("present_address"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($present_address))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-2 control-label">Present Address</label>

                                                    <div class="col-md-10">
                                                        <textarea id="present_address" name="present_address"
                                                                  class="form-control"
                                                                  placeholder="Type Address House No# , Street # , Town,City, Postal Code, Country"
                                                                  rows="3"><?php if (isset($present_address))
                                                                echo
                                                                $present_address;
                                                            ?></textarea>
                                                        <span
                                                            class="help-Block"><?php echo form_error("present_address");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>


                                    <fieldset id="guardian_information">
                                        <legend>Guardian Information</legend>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("guardian_type"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($guardian_type))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Guardian</label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="guardian_type"
                                                                name="guardian_type">
                                                            <option value="" selected="" disabled="">
                                                                Select Guardian
                                                            </option>
                                                            <?php
                                                            foreach ($a_guardian_type as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($guardian_type) && $guardian_type == $key) {
                                                                    echo
                                                                    'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span
                                                            class="help-Block"><?php echo form_error("guardian_type"); ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("guardian_name")) echo "has-error";
                                                    elseif (!empty($guardian_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Guardian Name </label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="guardian_name"
                                                               name="guardian_name" type="text"
                                                               value="<?php if (isset($guardian_name))
                                                                   echo $guardian_name;
                                                               ?>">
                                                        <span class="help-Block"><?php echo form_error("guardian_name");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("guardian_status"))
                                                    echo "has-error";
                                                elseif (!empty($guardian_status))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Guardian Status</label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="guardian_status"
                                                                name="guardian_status">
                                                            <option value="" selected="" disabled="">
                                                                Select Guardian Status
                                                            </option>
                                                            <?php
                                                            foreach ($a_guardian_status as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($guardian_status) && $guardian_status == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span
                                                            class="help-Block"><?php echo form_error("guardian_status"); ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("guardian_cnic")) echo "has-error";
                                                    elseif (!empty($guardian_cnic)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Guardian CNIC </label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="guardian_cnic"
                                                               name="guardian_cnic" type="text"
                                                               value="<?php if (isset($guardian_cnic))
                                                                   echo $guardian_cnic;
                                                               ?>">
                                                        <span class="help-Block"><?php echo form_error("guardian_cnic");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("guardian_education"))
                                                    echo "has-error";
                                                elseif (!empty($guardian_education))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Guardian Education</label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="guardian_education"
                                                                name="guardian_education">
                                                            <option value="" selected="" disabled="">
                                                                Select Guardian Education
                                                            </option>
                                                            <?php
                                                            foreach ($a_guardian_education as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($guardian_education) && $guardian_education == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span
                                                            class="help-Block"><?php echo form_error("guardian_education"); ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("guardian_occupation"))
                                                    echo "has-error";
                                                elseif (!empty($guardian_occupation))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Guardian Occupation </label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="guardian_occupation"
                                                               name="guardian_occupation" type="text"
                                                               value="<?php if (isset($guardian_occupation))
                                                                   echo $guardian_occupation;
                                                               ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("guardian_occupation");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("guardian_phone")) echo "has-error";
                                                    elseif (!empty($guardian_phone)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Phone Number</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="guardian_phone"
                                                               name="guardian_phone" placeholder="Type Phone Number"
                                                               type="text" value="<?php if (isset($guardian_phone))
                                                            echo $guardian_phone;
                                                        ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("guardian_phone");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Cell No -->
                                                <div
                                                    class="form-group <?php if (form_error("guardian_email")) echo "has-error";
                                                    elseif (!empty($last_name)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Email </label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="guardian_email"
                                                               name="guardian_email"
                                                               placeholder="Type Email" type="text"
                                                               value="<?php if (isset($guardian_email)) echo $guardian_email; ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("guardian_email");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("guardian_cellno1")) echo "has-error";
                                                    elseif (!empty($guardian_cellno1)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Cell Number</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="guardian_cellno1"
                                                               name="guardian_cellno1" placeholder="Type Cell Number"
                                                               type="text" value="<?php if (isset($guardian_cellno1))
                                                            echo $guardian_cellno1;
                                                        ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("guardian_cellno1");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Alternative Cell No-->
                                                <div
                                                    class="form-group <?php if (form_error("guardian_cellno2")) echo "has-error";
                                                    elseif (!empty($guardian_cellno2)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Alternative Cell no </label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="guardian_cellno2"
                                                               name="guardian_cellno2"
                                                               placeholder="Type Alternative Cell No" type="text"
                                                               value="<?php if (isset($guardian_cellno2)) echo $guardian_cellno2; ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("guardian_cellno2");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>

                                    <fieldset id="academic_informatin">
                                        <legend>Academic Information</legend>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group <?php
                                                if (form_error("a_institute"))
                                                    echo "has-error";
                                                elseif (!empty($a_institute))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-2 control-label">Institute Name
                                                    </label>

                                                    <div class="col-md-10">
                                                        <input class="form-control" id="a_institute"
                                                               name="a_institute"
                                                               placeholder="Type Institute Name" type="text"
                                                               value="<?php if (isset($a_institute)) echo $a_institute; ?>">
                                                        <span class="help-Block"><?php echo form_error("a_institute");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("a_degree_type"))
                                                    echo "has-error";
                                                elseif (!empty($a_degree_type))
                                                    echo
                                                    "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Degree Type </label>

                                                    <div class="col-md-8">
                                                        <select class="form-control" id="a_degree_type"
                                                                name="a_degree_type">
                                                            <option value="" selected="" disabled="">
                                                                Select Degree
                                                            </option>
                                                            <?php
                                                            foreach ($a_degree_types as $key => $value) {
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($a_degree_type) && $a_degree_type == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("a_degree_type");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Class Name-->
                                                <div class="form-group <?php if (form_error("a_class_name"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($a_class_name))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Class Name</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="a_class_name"
                                                               name="a_class_name"
                                                               placeholder="Type Class Name" type="text"
                                                               value="<?php if (isset($a_class_name))
                                                                   echo $a_class_name;
                                                               ?>">
                                                        <span class="help-Block"><?php echo form_error("a_class_name");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php if (form_error("a_obtained_marks"))
                                                    echo "has-error";
                                                elseif (!empty($a_obtained_marks))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Obtained Marks</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="a_obtained_marks"
                                                               name="a_obtained_marks"
                                                               placeholder="Type Obtained Marks" type="text"
                                                               value="<?php if (isset($a_obtained_marks))
                                                                   echo
                                                                   $a_obtained_marks;
                                                               ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("a_obtained_marks");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Class Name-->
                                                <div class="form-group <?php if (form_error("a_total_marks"))
                                                    echo
                                                    "has-error";
                                                elseif (!empty($a_total_marks))
                                                    echo "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Total Marks</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" id="a_total_marks"
                                                               name="a_total_marks"
                                                               placeholder="Type Total Marks" type="text"
                                                               value="<?php if (isset($a_total_marks))
                                                                   echo
                                                                   $a_total_marks;
                                                               ?>">
                                                        <span class="help-Block"><?php echo form_error("a_total_marks");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group <?php if (form_error("a_starting_date")) echo "has-error"; elseif (!empty($a_starting_date)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Starting Date</label>

                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                            <input class="form-control" id="a_starting_date"
                                                                   name="a_starting_date" type="text"
                                                                   placeholder="Choose Starting Date"
                                                                   value="<?php if (isset($a_starting_date)) echo $a_starting_date; ?>">
                                                             <span class="input-group-addon">
                                                                 <span class="glyphicon glyphicon-calendar"></span>
                                                             </span>
                                                            <span
                                                                class="help-Block"><?php echo form_error("a_starting_date");
                                                                ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <!--Class Name-->
                                                <div
                                                    class="form-group <?php if (form_error("a_ending_date")) echo "has-error"; elseif (!empty($a_ending_date)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Ending Date</label>

                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                            <input class="form-control" id="a_ending_date"
                                                                   name="a_ending_date" type="text"
                                                                   placeholder="Choose Ending Date"
                                                                   value="<?php if (isset($a_ending_date)) echo $a_ending_date; ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                            <span
                                                                class="help-Block"><?php echo form_error("a_ending_date");?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                    </fieldset>

                                    <fieldset id="enrollment_info">
                                        <legend>Enrollment Information</legend>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-6">

                                                <!--Date of Registration-->
                                                <div
                                                    class="form-group <?php if (form_error("dor")) echo "has-error"; elseif (!empty($dor)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Date Of Registration</label>

                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                            <input class="form-control" id="dor" name="dor" type="text"
                                                                   placeholder="Choose Registration Date"
                                                                   value="<?php if (isset($dor)) echo $dor; ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                            <span
                                                                class="help-Block"><?php echo form_error("dor");?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php   if ($this->m_functions->user_access($this->session->userdata('user_type')) == 'super') {  //   ?>
                                                <div class="col-md-6">
                                                    <div
                                                        class="form-group <?php if (form_error("branch_id")) echo "has-error"; elseif (!empty($branch_id)) echo "has-success"; ?>">
                                                        <label class="col-md-4 control-label">Branch</label>

                                                        <div class="col-md-8">
                                                            <select class="form-control" id="branch_id"
                                                                    name="branch_id">
                                                                <option value="" selected="" disabled="">
                                                                    Select Branch
                                                                </option>
                                                                <?php
                                                                foreach ($a_branches as $rows) {
                                                                    $key = $rows["branch_id"];
                                                                    $value = $rows["branch_name"];
                                                                    echo "<option label='{$value}' value='{$key}'";
                                                                    if (isset($branch_id) && $branch_id == $key) {
                                                                        echo 'selected="selected"';
                                                                    }
                                                                    echo ">" . $value . "</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                            <span
                                                                class="help-Block"><?php echo form_error("branch_id"); ?></span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <?php /* end of user access  */
                                            }   // ?>


                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("class_id"))
                                                    echo "has-error";
                                                elseif (!empty($class_id))
                                                    echo
                                                    "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Class</label>

                                                    <div class="col-md-8">

                                                        <select class="form-control" id="class_id" name="class_id">
                                                            <option value="" selected="" disabled="">
                                                                Select Class
                                                            </option>
                                                            <?php
                                                            foreach ($a_classes as $rows) {
                                                                $key = $rows["class_id"];
                                                                $value = $rows["class_name"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($class_id) && $class_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("class_id");
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("section_id"))
                                                    echo "has-error";
                                                elseif (!empty($section_id))
                                                    echo
                                                    "has-success"
                                                ?>">
                                                    <label class="col-md-4 control-label">Section</label>

                                                    <div class="col-md-8">

                                                        <select class="form-control" id="section_id" name="section_id">
                                                            <option value="" selected="" disabled="">
                                                                Select Section
                                                            </option>
                                                            <?php
                                                            foreach ($a_sections as $rows) {
                                                                $key = $rows["section_id"];
                                                                $value = $rows["section_name"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($section_id) && $section_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("section_id");
                                                            ?></span>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                    </fieldset>

                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset
                                                </button>
                                                <button type="submit" name="submit" id="submit" class="btn
                                                        btn-success btn-lg">Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php
// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
