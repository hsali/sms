<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main" xmlns="http://www.w3.org/1999/html">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2");
        ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

<?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-0"
                         data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false"
                         data-widget-togglebutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse">-->
                                <!--                                    <i class="fa fa-angle-down "></i>-->
                                <!--                                </a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                    <i class="fa fa-expand "></i>
                                </a>
                                <!--                                <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete">-->
                                <!--                                    <i class="fa fa-times"></i>-->
                                <!--                                </a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>
                            <h2>Student Management</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <fom class="form-horizontal">
                                    <?php
                                    $a_domicile = $this->config->item('domicile');
                                    $a_gender = $this->config->item('gender');
                                    $a_religion = $this->config->item('religion');
                                    $a_country = $this->config->item('country');
                                    $a_city = $this->config->item('city');
                                    $a_guardian_type = $this->config->item('guardian_type');
                                    $a_guardian_status = $this->config->item('guardian_status');
                                    $a_degree_types = $a_guardian_education = $this->config->item('degree_type');
                                    $a_enrollment_status = $this->config->item('enrollment_status');
                                    ?>
                                    <?php
                                    if (isset($data_pi)) {
                                        foreach ($data_pi as $row) {
                                            $cr_student_id = $row->student_id;
                                            $student_id = $row->student_id;
                                            $cr_first_name = $row->first_name;
                                            $cr_last_name = $row->last_name;
                                            $cr_gender = $row->gender;
                                            $cr_dob = $row->dob;
                                            $cr_cnic = $row->cnic;
                                            $cr_religion = $row->religion;
                                            $cr_domicile = $row->domicile;
                                            $cr_caste = $row->caste;
                                            $cr_tnbs = $row->tnbs;
                                            $cr_pbse = $row->pbse;
                                            $cr_email = $row->email;
                                            $cr_cell_no = $row->cell_no;
                                            $cr_country = $row->country;
                                            $cr_city = $row->city;
                                            $cr_other_city = $row->other_city;
                                            $cr_town = $row->town;
                                            $cr_permanent_address = $row->permanent_address;
                                            $cr_present_address = $row->present_address;
                                        }
                                    }
                                    if (isset($data_gi)) {
                                        foreach ($data_gi as $row) {
                                            $cr_guardian_type = $row->guardian_type;
                                            $cr_guardian_name = $row->guardian_name;
                                            $cr_guardian_cnic = $row->guardian_cnic;
                                            $cr_guardian_occupation = $row->guardian_occupation;
                                            $cr_guardian_status = $row->guardian_status;
                                            $cr_guardian_education = $row->guardian_education;
                                            $cr_guardian_phone = $row->guardian_phone;
                                            $cr_guardian_email = $row->guardian_email;
                                            $cr_guardian_cellno1 = $row->guardian_cellno1;
                                            $cr_guardian_cellno2 = $row->guardian_cellno2;
                                        }
                                    }
                                    if (isset($data_ai)) {
                                        foreach ($data_ai as $row) {
                                            $cr_a_degree_type = $row->a_degree_type;
                                            $cr_a_class_name = $row->a_class_name;
                                            $cr_a_institute = $row->a_institute;
                                            $cr_a_obtained_marks = $row->a_obtained_marks;
                                            $cr_a_total_marks = $row->a_total_marks;
                                            $cr_a_starting_date = $row->a_starting_date;
                                            $cr_a_ending_date = $row->a_ending_date;
                                        }
                                    }
                                    if (isset($data_ei)) {
                                        foreach ($data_ei as $row) {
                                            $cr_branch_id = $row->branch_id;
                                            $cr_dor = $row->dor;
                                            $cr_class_id = $row->class_id;
                                            $cr_section_id = $row->section_id;
                                            $cr_fee = $row->fee;
                                            $cr_enrollment_status = $row->enrollment_status;
                                        }
                                    }
                                    ?>

                                    <fieldset>
                                        <legend>Personal Information</legend>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <?php if(file_exists("pdb/students/student_id_".$cr_student_id.".jpg") || file_exists("pdb/students/student_id_".$cr_student_id.".jpeg") || file_exists("pdb/students/student_id_".$cr_student_id.".png") || file_exists("pdb/students/student_id_".$cr_student_id.".gif")) { ?>

                                                    <img width="200" height="200" src="<?php echo base_url("pdb/students/student_id_").$cr_student_id;
                                                    if(file_exists("pdb/students/student_id_".$cr_student_id.".jpg"))
                                                        echo ".jpg";
                                                    elseif(file_exists("pdb/students/student_id_".$cr_student_id.".jpeg"))
                                                        echo ".jpeg";
                                                    elseif(file_exists("pdb/students/student_id_".$cr_student_id.".png"))
                                                        echo ".png";
                                                    elseif(file_exists("pdb/students/student_id_".$cr_student_id.".gif"))
                                                        echo ".gif";
                                                    ?>"
                                                         class="img-responsive img-rounded"
                                                         alt=" Profile Picture of Student_id: <?php echo $cr_student_id; ?>">
                                                    <?php   /**/  } /**/ ?>

                                            </div>
                                            <br>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <p class="font-lg">Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_first_name . " " . $cr_last_name; ?></ins> </p>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <p class="font-lg">Caste &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <ins><?php echo $cr_caste; ?></ins></p> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <p class="font-lg">Gender &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php 
                                                foreach ($a_gender as $key=>$value){
                                                    if( !empty($cr_gender) && $cr_gender==$key )
                                                            echo $value;
                                                }
                                                            ?></ins> </p>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <p class="font-lg"><span title="Date Of Birth">DOB</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_dob; ?></ins> </p>
                                                        </div>
                                                    </div>  
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <p class="font-lg">CNIC # &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_cnic; ?></ins> </p>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <p class="font-lg">Domicile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php 
                                                            foreach($a_domicile as $key=>$value){
                                                                if( !empty($cr_domicile) && $cr_domicile==$key)
                                                                    echo $value;
                                                            }
                                                             ?></ins> </p>
                                                        </div>
                                                    </div> 
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <p class="font-lg"><span title="Total Number of Brother and Sister">TNBS #</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_tnbs; ?></ins> </p>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <p class="font-lg"><span title="Position in Brothers and Sisters">PBS #</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_pbse; ?></ins> </p>
                                                        </div>
                                                    </div> 
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <p class="font-lg">Cell No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_cell_no; ?></ins> </p>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <p class="font-lg">Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_email; ?></ins> </p> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <p class="font-lg">Religion &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php 
                                                            foreach($a_religion as $key=>$value){
                                                                if( !empty($cr_religion) && $cr_religion==$key)
                                                                    echo $value;
                                                            }
                                                             ?></ins> </p>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <p class="font-lg">Town &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_town; ?></ins> </p> 
                                                        </div>
                                                    </div>     
                                                </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p class="font-lg">Country &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php 
                                                            foreach($a_country as $key=>$value){
                                                                if( !empty($cr_country) && $cr_country==$key)
                                                                    echo $value;
                                                            }
                                                             ?></ins> </p>
                                            </div>    
                                            <div class="col-md-4">
                                                <p class="font-lg">City &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php 
                                                            foreach($a_city as $key=>$value){
                                                                if( !empty($cr_city) && $cr_city==$key)
                                                                    echo $value;
                                                            }
                                                             ?></ins> </p>
                                            </div>
                                            <div class="col-md-4 ">
                                                <p class="font-lg">Other City &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_other_city; ?></ins> </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="font-lg">Present Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_present_address; ?></ins> </p>
                                            </div>    
                                        </div>    
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="font-lg">Permanent Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_permanent_address; ?></ins> </p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <hr>
                                        <fieldset>
                                            <legend>Guardian Information</legend>
                                            <table  class="table table-striped table-hover" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th data-hide="phone">Guardian</th>
                                                        <th data-class="expand">Name</th>
                                                        <th data-class="expand">CNIC</th>
                                                        <th data-hide="phone">Occupation</th>
                                                        <th data-hide="phone">Phone </th>
                                                        <th data-hide="phone,tablet">Cell</th>
                                                        <th data-hide="phone,tablet">Email</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr id="">
                                                        <td>
                                                        <?php 
                                                            foreach($a_guardian_type as $key=>$value){
                                                                if( !empty($cr_guardian_type) && $cr_guardian_type==$key)
                                                                    echo $value;
                                                            }
                                                             ?>
                                                        </td>
                                                        <td><?php echo $cr_guardian_name; ?></td>
                                                        <td><?php echo $cr_guardian_cnic; ?></td>
                                                        <td><?php echo $cr_guardian_occupation; ?></td>
                                                        <td><?php echo $cr_guardian_phone; ?></td>
                                                        <td><?php echo $cr_guardian_cellno1; ?></td>
                                                        <td><?php echo $cr_guardian_email; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </fieldset>
                                        <hr>
                                            <fieldset>
                                                <legend>Academic Information</legend>
                                                <table  class="table table-striped table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th data-hide="phone">Institute</th>
                                                            <th data-hide="phone">Type</th>
                                                            <th data-class="expand">Class</th>
                                                            <th data-hide="phone">Obtained Marks</th>
                                                            <th data-hide="phone">Total Marks</th>
                                                            <th data-hide="phone">Starting Date</th>
                                                            <th data-hide="phone,tablet">Ending Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr id="">
                                                            <td><?php echo $cr_a_institute; ?> </td>
                                                            <td>
                                                                <?php 
                                                            foreach($a_degree_types as $key=>$value){
                                                                if( !empty($cr_a_degree_type) && $cr_a_degree_type==$key)
                                                                    echo $value;
                                                            }
                                                             ?>
                                                            </td>
                                                            <td><?php echo $cr_a_class_name; ?></td>
                                                            <td><?php echo $cr_a_obtained_marks; ?></td>
                                                            <td><?php echo $cr_a_total_marks; ?></td>
                                                            <td><?php echo $cr_a_starting_date; ?></td>
                                                            <td><?php echo $cr_a_ending_date; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                            <fieldset>
                                                <legend>Enrollment Information</legend>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="font-lg">Date of Registration &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php echo $cr_dor; ?></ins> </p>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <p class="font-lg"><span title="">Enrollment Status</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php 
                                                            foreach($a_enrollment_status as $key=>$value){
                                                                if( !empty($cr_enrollment_status) && $cr_enrollment_status==$key)
                                                                    echo $value;
                                                            }
                                                             ?></ins> </p>
                                                    </div>
                                                </div> 
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="font-lg">Class &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php 
                                                            foreach($a_classes as $rows){
                                                                 $key = $rows["class_id"];
    $value = $rows["class_name"];
                                                                if( !empty($cr_class_id) && $cr_class_id==$key)
                                                                    echo $value;
                                                            }
                                                             ?></ins> </p>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <p class="font-lg"><span title="">Section</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php 
                                                            foreach($a_sections as $rows){
                                                                 $key = $rows["section_id"];
    $value = $rows["section_name"];
                                                                if( !empty($cr_section_id) && $cr_section_id==$key)
                                                                    echo $value;
                                                            }
                                                             ?></ins> </p>
                                                    </div>
                                                </div> 
<?php if ($this->m_functions->user_access($this->session->userdata('user_type')) == 'super') {  //    ?>
                                                    <div class="row">

                                                        <div class="col-md-6 ">
                                                            <p class="font-lg"><span title="">Branch</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins><?php 
                                                            foreach($a_branches as $rows){
                                                                 $key = $rows["branch_id"];
    $value = $rows["branch_name"];
                                                                if( !empty($cr_branch_id) && $cr_branch_id==$key)
                                                                    echo $value;
                                                            }
                                                             ?></ins> </p>
                                                        </div>
                                                    </div> 
<?php /* */
} /* */ ?>
                                            </fieldset>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <a href="<?php echo site_url("admin/students/del/" . $student_id); ?>"
                                                           class="btn
                                                           btn-danger btn-lg"> Delete</a>
                                                        <a href="<?php echo site_url("admin/students/edit/" . $student_id); ?>" class="btn btn-warning btn-lg"> Edit</a>
                                                        <a href="<?php echo site_url("admin/students/print/" . $student_id); ?>" class="btn btn-success btn-lg"> Print</a>

                                                    </div>
                                                </div>
                                            </div>
                                            </fom>


                                            </div>
                                            <!-- end widget content -->

                                            </div>
                                            <!-- end widget div -->

                                            </div>
                                            <!-- end widget -->
                                            </article>




                                            <!-- WIDGET END -->

                                            </div>

                                            </section>
                                            <!-- end widget grid -->

                                            </div>
                                            <!-- END MAIN CONTENT -->





                                            </div>
                                            <!-- END MAIN CONTENT -->

                                            </div>
                                            <!-- END MAIN PANEL -->


                                            <!--================================================== -->



                                            <?php
// copyrights and info for website
                                            $this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
                                            $this->load->view("admin/footers/shortcuts");

// general scripts references
                                            $this->load->view("admin/footers/footer_general_scripts_ref");


                                            $this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
                                            $this->load->view("admin/footers/footer1");
                                            ?>
