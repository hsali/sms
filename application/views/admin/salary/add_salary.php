<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2");
        ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>


            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-123"
                         data-widget-editbutton="false"
                         role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!-- <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse">
                             <i class="fa fa-minus "></i>
                         </a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                    <i class="fa fa-expand "></i>
                                </a>
                                <!--    <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete">
                                        <i class="fa fa-times"></i>
                                    </a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>

                            <h2>Attendance</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                                <?php

                                $cr_staff_id = $edit_data["staff_id"];
                                $cr_select_month = $edit_data["year"] . "-" . $edit_data["month"];
                                $cr_payable = $edit_data["payable"];
                                $cr_paid = $edit_data["paid"];

                                ?>

                                <form class="form-horizontal" method="post" enctype="multipart/form-data">

                                    <fieldset id="">
                                        <legend><?php if (isset($widget_title)) echo $widget_title; else echo "Salary Management" ?></legend>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg <?php
                                                if (form_error("staff_id")) echo "has-error";
                                                elseif (!empty($staff_id)) echo "has-success"; ?>">
                                                    <label class="col-md-2 control-label">Staff*</label>

                                                    <div class="col-md-10">
                                                        <select style="width:100%"
                                                                class="form-group select2-active js-example-basic-single"
                                                                required id="staff_id" name="staff_id">

                                                            <?php
                                                            foreach ($a_staffs as $rows) {
                                                                $key = $rows["staff_id"];
                                                                $value = $rows["b_staff_id"] . " - " . $rows["first_name"] . " " . $rows["last_name"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($staff_id) && $staff_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                if (isset($cr_staff_id) && $cr_staff_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("staff_id");
                                                            ?></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">

                                                <!--Date -->
                                                <div
                                                    class="form-group  <?php if (form_error("select_month")) echo "has-error"; elseif (!empty($select_month)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Month*</label>

                                                    <div class="col-md-8">
                                                        <div class="input-group date date-month-year-picker">
                                                            <input class="form-control" id="select_month"
                                                                   name="select_month" type="text"
                                                                   placeholder="Choose Month" value="<?php
                                                            if (isset($select_month))
                                                                echo $select_month;
                                                            elseif (isset($cr_select_month))
                                                                echo $cr_select_month;
                                                            ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                            <span
                                                                class="help-Block"><?php echo form_error("select_month"); ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group  <?php if (form_error("payable")) echo "has-error"; elseif (!empty($payable)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Payable</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" readonly id="payable" name="payable"
                                                               type="text" value="<?php
                                                        if (isset($payable)) echo $payable;
                                                        elseif (isset($cr_payable)) echo $cr_payable;
                                                        ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("payable"); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group  <?php if (form_error("paid")) echo "has-error"; elseif (!empty($paid)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Paying</label>

                                                    <div class="col-md-8">
                                                        <input class="form-control" required id="paid" name="paid"
                                                               type="text" placeholder="" value="<?php
                                                        if (isset($paid)) echo $paid;
                                                        elseif (isset($cr_paid)) echo $cr_paid;
                                                        ?>">
                                                        <span
                                                            class="help-Block"><?php echo form_error("paid"); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset
                                                </button>
                                                <button type="submit" name="submit" id="submit"
                                                        class="btn btn-success btn-lg">Change <span
                                                        class="fa fa-arrow-right"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php
// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
