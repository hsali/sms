<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-greenLight" id="wid-id-3" data-widget-editbutton="false"
                         data-widget-colorbutton="false" data-widget-collapsed="false" data-widget-deletebutton="false"
                         data-widget-togglebutton="false">
                        <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-book"></i> </span>

                            <h2><?php if (isset($widget_title)) echo $widget_title; else echo "Salary Management"; ?></h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <table id="datatable_tabletools" class="table table-striped table-bordered table-hover"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th data-hide="phone" title="Unique salary id">ID</th>
                                        <th data-class="expand" title="Staff ID ">Staff ID</th>
                                        <th data-class="expand" title="Staff Name">Staff</th>
                                        <th data-class="phone,tablet">Month</th>
                                        <th data-class="phone,tablet">Attendance</th>
                                        <th data-hide="phone">Payable/Paid</th>
                                        <th data-hide="phone" title="Allowances and Deductions">Allowance/Deduction</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($view_data as $rows): ?>
                                        <tr id="salary_id_<?php echo $rows["salary_id"]; ?>">
                                            <td>
                                               <?php echo $rows["salary_id"]; ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo site_url("admin/salary/staff/view") . '/' . $rows["staff_id"]; ?>"> <?php echo $rows["staff_id"]; ?></a>
                                            </td>
                                            <td><?php echo $rows["name"]; ?></td>
                                            <td><?php echo $rows["month"] . '-' . $rows["year"]; ?></td>
                                            <td><span class="badge inbox-badge bg-color-green"
                                                      title="Present"><?php echo $rows["total_presents"]; ?></span><span
                                                    class="badge inbox-badge bg-color-red"
                                                    title="Absent"><?php echo $rows["total_absents"]; ?></span><span
                                                    class="badge inbox-badge bg-color-blue"
                                                    title="Leave"><?php echo $rows["total_leaves"]; ?></span><span
                                                    class="badge inbox-badge"
                                                    title="Late"><?php echo $rows["total_late_presents"]; ?></span></td>
                                            <td title="Payable / Paid"><span
                                                    class="badge inbox-badge bg-color-greenDark"> <?php echo $rows["payable"]; ?> </span>
                                                <span
                                                    class="badge inbox-badge bg-color-greenLight"> <?php echo $rows["paid"]; ?></span>
                                            </td>

                                            <td title="Allowances / Deductions"><span
                                                    class="badge inbox-badge bg-color-blueDark"> <?php echo $rows["allowances"]; ?> </span>
                                                <span
                                                    class="badge inbox-badge bg-color-blue"> <?php echo '-' . $rows["deductions"]; ?></span>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>
                <!-- WIDGET END -->

            </div>

            <!-- end row -->

            <!-- end row -->

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
