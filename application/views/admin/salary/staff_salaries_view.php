<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>


            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-yellow" id="wid-id-3" data-widget-editbutton="false"
                         data-widget-colorbutton="false" data-widget-collapsed="false" data-widget-deletebutton="false"
                         data-widget-togglebutton="false">
                        <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-money"></i> </span>

                            <h2><?php if (isset($widget_title)) echo $widget_title; else "Salary Management"; ?></h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body padding-4">
                                </br>

                                <div class="row">
                                    <div class=" col-sm-12">
                                        <h1 style="display:inline">Staff ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $staff_job_data["b_staff_id"]; ?></small>
                                        </h1>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <h1 style="display:inline">Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $staff_name["first_name"] . " " . $staff_name["last_name"]; ?></small>
                                        </h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class=" col-sm-12">
                                        <h1 style="display:inline">Designation &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $this->m_common->select_value_of_variable($staff_job_data["staff_designation"]); ?></small>
                                        </h1>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <h1 style="display:inline">Job Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $this->m_common->select_value_of_variable($staff_job_data["job_type"]); ?></small>
                                        </h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class=" col-sm-12">
                                        <h1 style="display:inline">Basic Salary &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $staff_job_data["basic_salary"]; ?></small>
                                        </h1>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <h1 style="display:inline">Appointed &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $staff_job_data["doa"]; ?></small>
                                        </h1>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-9">
                                        <h2>Salaries Detail</h2>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Month</th>
                                                <th>Attendance</th>
                                                <th>Payable/ Paid</th>
                                                <th>Allowances/Deductions</th>
                                                <th>Arrears Or Advance</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if (isset($salaries_data)) {
                                                foreach ($salaries_data as $rows) {
                                                    /* */ ?>
                                                    <tr id="<?php echo $rows["salary_id"]; ?>">
                                                        <td><a class="btn btn-small btn-info"
                                                               href="<?php echo site_url("admin/salary/edit") . "/" . $rows["salary_id"]; ?>"><?php echo $rows["salary_id"]; ?></a>
                                                        </td>
                                                        <td><?php echo $rows["month"] . '-' . $rows["year"]; ?></td>
                                                        <td><span class="badge inbox-badge bg-color-green"
                                                                  title="Present"><?php echo $rows["total_presents"]; ?></span><span
                                                                class="badge inbox-badge bg-color-red"
                                                                title="Absent"><?php echo $rows["total_absents"]; ?></span><span
                                                                class="badge inbox-badge bg-color-blue"
                                                                title="Leave"><?php echo $rows["total_leaves"]; ?></span><span
                                                                class="badge inbox-badge"
                                                                title="Late"><?php echo $rows["total_late_presents"]; ?></span>
                                                        </td>
                                                        <td title="Payable / Paid"><span
                                                                class="badge inbox-badge bg-color-greenDark"> <?php echo $rows["payable"]; ?> </span>
                                                            <span
                                                                class="badge inbox-badge bg-color-greenLight"> <?php echo $rows["paid"]; ?></span>
                                                        </td>

                                                        <td title="Allowances / Deductions"><span
                                                                class="badge inbox-badge bg-color-blueDark"> <?php echo $rows["allowances"]; ?> </span>
                                                            <span
                                                                class="badge inbox-badge bg-color-blue"> <?php echo '-' . $rows["deductions"]; ?></span>
                                                        </td>
                                                        </td>
                                                        <td><?php echo $rows["arrears_and_advance"]; ?></td>
                                                    </tr>
                                                    <?php /* */

                                                } /* */
                                            }  /* */ ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <a href="<?php echo site_url("admin/salary/staff/print/" . $staff_name["staff_id"]); ?>"
                                           class="btn btn-success btn-lg"><span
                                                class="fa fa-print">&nbsp;&nbsp;Print</span></a>


                                    </div>
                                </div>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>
                <!-- WIDGET END -->

            </div>

            <!-- end row -->

            <!-- end row -->

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
