<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>


            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-0"
                         data-widget-editbutton="false"
                         role="widget" data-widget-colorbutton="false" data-widget-collopse="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!--                                <a href="javascript:void(0);"-->
                                <!--                                                                           class="button-icon jarviswidget-toggle-btn"-->
                                <!--                                                                           rel="tooltip" title=""-->
                                <!--                                                                           data-placement="bottom"-->
                                <!--                                                                           data-original-title="Collapse"><i-->
                                <!--                                        class="fa fa-minus "></i></a> -->
                                <a href="javascript:void(0);"
                                   class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom"
                                   data-original-title="Fullscreen"><i
                                        class="fa fa-expand "></i></a>
                                <!--                                <a href="javascript:void(0);"-->
                                <!--                                                                          class="button-icon jarviswidget-delete-btn"-->
                                <!--                                                                          rel="tooltip" title="" data-placement="bottom"-->
                                <!--                                                                          data-original-title="Delete"><i-->
                                <!--                                        class="fa fa-times"></i></a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>

                            <h2><?php if (isset($widget_title)) echo $widget_title; else echo "Examination Management"; ?></h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                                <div class="alert alert-info fade in">
                                    <button class="close" data-dismiss="alert">
                                        ×
                                    </button>
                                    <i class="fa-fw fa fa-2x fa-times"></i>
                                    <strong>Info!</strong> Select The staff and date to generate the salary
                                </div>

                                <form class="form-horizontal" method="post" enctype="multipart/form-data">

                                    <fieldset>
                                        <legend>Select staff and date</legend>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group form-group-lg <?php
                                                if (form_error("staff_id")) echo "has-error";
                                                elseif (!empty($staff_id)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Staff*</label>

                                                    <div class="col-md-8">
                                                        <select style="width:100%"
                                                                class="form-group select2-active js-example-basic-single"
                                                                required id="staff_id" name="staff_id">

                                                            <?php
                                                            foreach ($a_staffs as $rows) {
                                                                $key = $rows["staff_id"];
                                                                $value = $rows["b_staff_id"] . " - " . $rows["first_name"] . " " . $rows["last_name"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($staff_id) && $staff_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                if (isset($cr_staff_id) && $cr_staff_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("staff_id");
                                                            ?></span>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-8">

                                                <!--Date -->
                                                <div
                                                    class="form-group form-group-lg  <?php if (form_error("select_date")) echo "has-error"; elseif (!empty($select_date)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Month</label>

                                                    <div class="col-md-8">
                                                        <div class="input-group date  date-month-year-picker">
                                                            <input class="form-control" id="select_date"
                                                                   name="select_date" type="text"
                                                                   placeholder="Choose month" autocomplete="off"
                                                                   value="<?php if (isset($select_date)) echo $select_date; ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                            <span
                                                                class="help-Block"><?php echo form_error("select_date"); ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>


                                    </fieldset>


                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset
                                                </button>
                                                <button type="submit" name="generate" id="generate" class="btn
                                              btn-success btn-lg fa fa-caret-up"> Generate
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


//$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
