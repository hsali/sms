<?php $user_type = $this->session->userdata('user_type'); ?>

<!--// super user-->
<?php if ($this->m_functions->user_access($user_type) == 'super') { ?>
    <aside id="left-panel">

        <!-- User info -->
        <div class="login-info">
            <span> <!-- User image size is adjusted inside CSS, it should stay as it -->

                <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                    <img src="<?php echo base_url("media/admin_panel/img/avatars/sunny.png"); ?>" alt="me"
                         class="online"/>
                    <span>
                        <?php echo $this->session->userdata("first_name") . " " . $this->session->userdata("last_name"); ?>
                    </span>
                    <i class="fa fa-angle-down"></i>
                </a>

            </span>
        </div>
        <!-- end user info -->

        <!-- NAVIGATION : This navigation is also responsive

        To make this navigation dynamic please make sure to link the node
        (the reference to the nav > ul) after page load. Or the navigation
        will not initialize.
        -->
        <nav>
            <!-- NOTE: Notice the gaps after each icon usage <i></i>..
            Please note that these links work a bit different than
            traditional href="" links. See documentation for details.
            -->

            <ul>
                <li <?php if (current_url() == site_url("home")) echo 'class="active"'; ?>>
                    <a href="<?php echo site_url("home"); ?>" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i>
                        <span
                            class="menu-item-parent">Dashboard</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Users</span> &nbsp; &nbsp;<span class="badge inbox-badge bg-color-greenLight"><?php  echo $this->config->item("total_users"); ?></span></a>
                    <ul>
                        <li <?php if (current_url() == site_url("super/users")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("super/users"); ?>"><i class="fa fa-lg fa-fw fa-database"></i>
                                Directory </a>
                        </li>
                        <li <?php if (current_url() == site_url("super/user/add")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("super/user/add"); ?>"><i class="fa fa-lg fa-plus-circle"></i>User</a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-cogs"></i> <span
                            class="menu-item-parent">Settings</span></a>
                    <ul>
                        <li>
                            <a href="#">Branch &nbsp; &nbsp;<span
                                    class="badge inbox-badge bg-color-greenLight"><?php echo $this->config->item("total_branches"); ?></span></a>
                            <ul>
                                <li <?php if (current_url() == site_url("admin/branches")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/branches"); ?>"><i class="fa fa-database"></i>Directory</a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/branches/add")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/branches/add"); ?>"><i
                                            class="fa fa-plus-circle"></i> Branch</a>
                                </li>
                            </ul>
                        </li>


                        <li>
                            <a href="#"
                               title="Create, Edit, Delete and view all variables. These variables are used in other modules.">Variables</a>
                            <ul>
                                <li <?php if (current_url() == site_url("admin/settings/variables")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/settings/variables"); ?>"><i
                                            class="fa fa-database" title="View all the variables of system"></i>
                                        Directory</a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/settings/variables/add")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/settings/variables/add"); ?>"><i
                                            class="fa fa-plus-circle"></i> Variable</a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/settings/variable/values/add")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/settings/variable/values/add"); ?>"
                                       title="Add values in the variable"><i class="fa fa-plus-circle"></i> Value</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" title="Add, Edit, Delete values in Fee category">Fee Category</a>
                            <ul>
                                <li <?php if (current_url() == site_url("admin/settings/variables/view/4")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/settings/variables/view/4"); ?>"><i
                                            class="fa fa-database"></i>Directory</a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/settings/variable/values/add/4")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/settings/variable/values/add/4"); ?>"><i
                                            class="fa fa-plus-circle"></i> Fee Category </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>


                    </ul>
                </li>

            </ul>
        </nav>
        <span class="minifyme" data-action="minifyMenu">
            <i class="fa fa-arrow-circle-left hit"></i>
        </span>
    </aside>
<?php } //end of super ?>


<?php if ($this->m_functions->user_access($user_type) == 'admin') { ?>
    <aside id="left-panel">

        <!-- User info -->
        <div class="login-info">
            <span> <!-- User image size is adjusted inside CSS, it should stay as it -->
                <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                    <img src="<?php echo base_url("media/admin_panel/img/avatars/sunny.png"); ?>" alt="me"
                         class="online"/>
                    <span>
                        <?php echo $this->session->userdata("first_name") . " " . $this->session->userdata("last_name"); ?>
                    </span>
                    <i class="fa fa-angle-down"></i>
                </a>

            </span>
        </div>
        <!-- end user info -->

        <!-- NAVIGATION : This navigation is also responsive

        To make this navigation dynamic please make sure to link the node
        (the reference to the nav > ul) after page load. Or the navigation
        will not initialize.
        -->
        <nav>
            <!-- NOTE: Notice the gaps after each icon usage <i></i>..
            Please note that these links work a bit different than
            traditional href="" links. See documentation for details.
            -->

            <ul>
                <li <?php if (current_url() == site_url("home")) echo 'class="active"'; ?>>
                    <a href="<?php echo site_url("home"); ?>" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i>
                        <span
                            class="menu-item-parent">Dashboard</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-cogs"></i> <span
                            class="menu-item-parent">Settings</span></a>
                    <ul>
                        <li>
                            <a href="#">Class &nbsp;&nbsp; <span
                                    class="badge inbox-badge bg-color-greenLight"><?php echo $this->config->item("total_classes") ?></span></a>
                            <ul>
                                <li <?php if (current_url() == site_url("admin/classes")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/classes"); ?>"><i class="fa fa-database"></i>
                                        Directory</a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/classes/add")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/classes/add"); ?>"><i
                                            class="fa fa-plus-circle"></i> Class</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Section &nbsp; &nbsp;<span
                                    class="badge inbox-badge bg-color-greenLight"><?php echo $this->config->item("total_sections") ?></span></a>
                            <ul>
                                <li <?php if (current_url() == site_url("admin/sections")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/sections"); ?>"><i class="fa fa-database"></i>
                                        Directory</a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/sections/add")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/sections/add"); ?>"><i
                                            class="fa fa-plus-circle"></i> Section</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" title="Add fee for different fee categories">Program Fee</a>
                            <ul>
                                <li <?php if (current_url() == site_url("admin/settings/program/fee")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/settings/program/fee"); ?>"><i
                                            class="fa fa-database"></i> Directory</a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/settings/program/fee/add")) echo 'class="active"'; ?> >
                                    <a href="<?php echo site_url("admin/settings/program/fee/add"); ?>"><i
                                            class="fa fa-plus-circle"></i> Program Fee </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" title="Add salary and view salary">Salary Setting</a>
                            <ul>
                                <li <?php if (current_url() == site_url("admin/settings/salary/staff")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/settings/salary/staff"); ?>"><i
                                            class="fa fa-database"></i>Staff Directory</a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/settings/salary/group")) echo 'class="active"'; ?>>
                                    <a href="<?php echo site_url("admin/settings/salary/group"); ?>"><i
                                            class="fa fa-database"></i>Group Directory</a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/settings/salary/staff/add")) echo 'class="active"'; ?> >
                                    <a href="<?php echo site_url("admin/settings/salary/staff/add"); ?>"><i
                                            class="fa fa-plus-circle"></i> Staff </a>
                                </li>
                                <li <?php if (current_url() == site_url("admin/settings/salary/group/add")) echo 'class="active"'; ?> >
                                    <a href="<?php echo site_url("admin/settings/salary/group/add"); ?>"><i
                                            class="fa fa-plus-circle"></i> Group </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">Students</span>
                        &nbsp; &nbsp;<span
                            class="badge inbox-badge bg-color-greenLight"><?php echo $this->config->item("total_students") ?></span></a>
                    <ul>
                        <li <?php if (current_url() == site_url("admin/students")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/students"); ?>"><span class="fa fa-database"></span> Directory</a>
                        </li>
                        <li <?php if (current_url() == site_url("admin/students/add")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/students/add"); ?>"><span class="fa fa-plus-circle"></span> Student </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">Staff</span>  &nbsp; &nbsp;<span class="badge inbox-badge bg-color-greenLight"><?php echo $this->config->item("total_staffs"); ?></span></a>
                    <ul>
                        <li <?php if (current_url() == site_url("admin/staff")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/staff"); ?>"><span class="fa fa-database"></span> Directory</a>
                        </li>
                        <li <?php if (current_url() == site_url("admin/staff/add")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/staff/add"); ?>"><span class="fa fa-plus-circle"></span> Staff</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-pencil"></i><span class="menu-item-parent">Attendance</span> &nbsp;&nbsp;<span class="badge inbox-badge bg-color-grayDark">New</span></a>
                    <ul>
                        <li <?php if (current_url() == site_url("admin/attendance/section/add")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/attendance/section/add"); ?>"><span class="fa fa-plus-circle"></span> Section</a>
                        </li>
                        <li <?php if (current_url() == site_url("admin/attendance/sections")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/attendance/sections"); ?>" title="view all the attendance of all sections"><span class="fa fa-database"></span> Sections Attendance</a>
                        </li>
                        <hr>
                        <li <?php if (current_url() == site_url("admin/attendance/staff/add")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/attendance/staff/add") ?>" title="Add the individual Attendance for each staff"><span class="fa fa-plus-circle"></span> Staff</a>
                        </li>
                        <li <?php if (current_url() == site_url("admin/attendance/staff")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/attendance/staff") ?>" title="View all the attendance of all staff"><span class="fa fa-database"></span> Staff</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-bank"></i><span class="menu-item-parent"> Fee</span> &nbsp; &nbsp; <span class="badge inbox-badge bg-color-greenLight">New</span> </a>
                    <ul>
                        <li <?php if (current_url() == site_url("admin/fee/admission/add")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/fee/admission/add"); ?>"><span class="fa fa-plus-circle"></span> Admission Fee</a>
                        </li>
                        <li <?php if (current_url() == site_url("admin/fee/receipts")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/fee/receipts"); ?>"><span class="fa fa-database"></span> Receipts</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-graduation-cap"></i><span class="menu-item-parent"> Results</span> &nbsp; &nbsp; <span class="badge inbox-badge bg-color-greenLight">New</span> </a>
                    <ul>
                        <li <?php if (current_url() == site_url("admin/result/add")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/result/add"); ?>"><span class="fa fa-plus-circle"></span> Student </a>
                        </li>
                        <li <?php if (current_url() == site_url("admin/result/all")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/result/all"); ?>"><span class="fa fa-database"></span> Results</a>
                        </li>
                        <li <?php if (current_url() == site_url("admin/result/select")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/result/select"); ?>"><span class="fa fa-filter"></span> Filter</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-money"></i><span class="menu-item-parent"> Salary</span> &nbsp; &nbsp; <span class="badge inbox-badge bg-color-greenLight">New</span> </a>
                    <ul>
                        <li <?php if (current_url() == site_url("admin/salary/select")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/salary/select"); ?>"><span
                                    class="fa fa-plus-circle"></span> Salary </a>
                        </li>
                        <li <?php if (current_url() == site_url("admin/salary/all")) echo 'class="active"'; ?>>
                            <a href="<?php echo site_url("admin/salary/all"); ?>"><span class="fa fa-database"></span>
                                Directory</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <span class="minifyme" data-action="minifyMenu">
            <i class="fa fa-arrow-circle-left hit"></i>
        </span>

    </aside>
<?php } // end admin sdiebar ?>
