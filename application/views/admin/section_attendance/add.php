
<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2");
        ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>



            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-21"
                         data-widget-editbutton="false" jarviswidget-togglebutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!-- <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse">
                                     <i class="fa fa-minus "></i>
                                 </a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                    <i class="fa fa-expand "></i>
                                </a>
                                <!--    <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete">
                                        <i class="fa fa-times"></i>
                                    </a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>

                            <h2>Attendance</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal" method="post" enctype="multipart/form-data" >

                                    <fieldset id="enrollment_info">
                                        <legend>Enrollment Information</legend>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("class_id")) echo "has-error";
                                                elseif (!empty($class_id)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Class*</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control" required id="class_id" name="class_id">
                                                            <option value="" selected=""  disabled="">
                                                                Select Classs
                                                            </option>
                                                            <?php
                                                            foreach ($a_classes as $rows) {
                                                                $key = $rows["class_id"];
                                                                $value = $rows["class_name"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($class_id) && $class_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("class_id");
                                                            ?></span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group <?php
                                                if (form_error("section_id")) echo "has-error";
                                                elseif (!empty($section_id)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Section*</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control" required id="section_id" name="section_id">
                                                            <option value="" selected=""  disabled="">
                                                                Select Section
                                                            </option>
                                                            <?php
                                                            foreach ($a_sections as $rows) {
                                                                $key = $rows["section_id"];
                                                                $value = $rows["section_name"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($section_id) && $section_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("section_id");
                                                            ?></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">

                                                <!--Date -->
                                                <div class="form-group  <?php if (form_error("attendance_date")) echo "has-error"; elseif (!empty($attendance_date)) echo "has-success"?>">
                                                    <label class="col-md-4 control-label">Attendance Date*</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date date-picker">
                                                            <input class="form-control" id="attendance_date" name="attendance_date" type="text" placeholder="Choose Attendance Date" value="<?php if (isset($attendance_date)) echo $attendance_date;?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                            <span class="help-Block"><?php echo form_error("attendance_date");?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group  <?php
                                                if (form_error("subject_id")) echo "has-error";
                                                elseif (!empty($subject_id)) echo "has-success"; ?>">
                                                    <label class="col-md-4 control-label">Subject*</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control" required id="subject_id" name="subject_id">
                                                            <option value="" selected="" disabled="">
                                                                Select Subject
                                                            </option>
                                                            <?php
                                                            foreach ($a_subjects as $rows) {
                                                                $key = $rows["var_vid"];
                                                                $value = $rows["var_value"];
                                                                echo "<option label='{$value}' value='{$key}'";
                                                                if (isset($subject_id) && $subject_id == $key) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                echo ">" . $value . "</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-Block"><?php echo form_error("subject_id");
                                                            ?></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group  <?php if (form_error("total_present")) echo "has-error"; elseif (!empty($total_present)) echo "has-success"?>">
                                                    <label class="col-md-4 control-label">Total Present*</label>
                                                    <div class="col-md-8">
                                                            <input class="form-control" required id="total_present" name="total_present" type="text" placeholder="Total Present Students" value="<?php if (isset($total_present)) echo $total_present;?>">
                                                            <span class="help-Block"><?php echo form_error("total_present");?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group  <?php if (form_error("total_absent")) echo "has-error"; elseif (!empty($total_absent)) echo "has-success"?>">
                                                    <label class="col-md-4 control-label">Total Absent</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="total_absent" name="total_absent" type="text" placeholder="Total Absent Students" value="<?php if (isset($total_absent)) echo $total_absent;?>">
                                                        <span class="help-Block"><?php echo form_error("total_absent");?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group  <?php if (form_error("total_leave")) echo "has-error"; elseif (!empty($total_leave)) echo "has-success"?>">
                                                    <label class="col-md-4 control-label">Total on Leave</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="total_leave" name="total_leave" type="text" placeholder="Total Students on leave" value="<?php if (isset($total_leave)) echo $total_leave;?>">
                                                        <span class="help-Block"><?php echo form_error("total_leave");?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group  <?php if (form_error("total_late")) echo "has-error"; elseif (!empty($total_late)) echo "has-success"?>">
                                                    <label class="col-md-4 control-label">Total Late</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" id="total_late" name="total_late" type="text" placeholder="Total Students are late" value="<?php if (isset($total_late)) echo $total_late;?>">
                                                        <span class="help-Block"><?php echo form_error("total_late");?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   </fieldset>

                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset
                                                </button>
                                                <button type="submit" name="submit" id="submit" class="btn
                                                        btn-success btn-lg">Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->



<?php
// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
