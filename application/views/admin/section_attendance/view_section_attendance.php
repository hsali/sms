<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-yellow" id="wid-id-3" data-widget-editbutton="false"
                         data-widget-colorbutton="false" data-widget-collapsed="false" data-widget-deletebutton="false"
                         data-widget-togglebutton="false">
                        <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-gear"></i> </span>

                            <h2>Attendance</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body padding-4">
                                </br>
                                <?php
                                if (!empty($view_data)) {
                                    foreach ($view_data as $rows) {
                                        $sa_id = $rows["sa_id"];
                                        $class_name = $rows["class_name"];
                                        $section_name = $rows["section_name"];
                                        $subject_name = $rows["subject_name"];
                                        $total_present = $rows["total_present"];
                                        $total_absent = $rows["total_absent"];
                                        $total_leave = $rows["total_leave"];
                                        $total_late = $rows["total_late"];
                                        $attendance_date = $rows["attendance_date"];
                                    }
                                } else {
                                    $sa_id = $id;
                                    $class_name = "------";
                                    $section_name = "-----";
                                    $subject_name = "------";
                                    $total_present = "1";
                                    $total_absent = "1";
                                    $total_leave = "1";
                                    $total_late = "1";
                                    $attendance_date = "NILL";
                                }
                                ?>

                                <div class="row">
                                    <div class=" col-sm-12">
                                        <h1 style="display:inline">ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $sa_id; ?></small>
                                        </h1>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <h1 style="display:inline">Class &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $class_name; ?></small>
                                        </h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class=" col-sm-12">
                                        <h1 style="display:inline">Section &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $section_name; ?></small>
                                        </h1>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <h1 style="display:inline">Subject &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <small><?php echo $subject_name; ?></small>
                                        </h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2 class="txt-color-blue">Attendance <span class="semi-bold">Chart</span></h2>
                                        <br>

                                        <div class="text-align-center">
                                            <div class="sparkline txt-color-red display-inline"
                                                 data-sparkline-type="pie" data-sparkline-offset="100"
                                                 data-sparkline-piesize="200px"><?php echo $total_present; ?>
                                                ,<?php echo $total_absent; ?>,<?php echo $total_leave; ?>
                                                ,<?php echo $total_late; ?>,
                                            </div>
                                            <br/>
                                            <br/>
                                            <span
                                                class="font-sm ">This chart is showing the percentage of attendance.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2>Full Detail of attendance</h2>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Present</th>
                                                <th>Absent</th>
                                                <th>Leave</th>
                                                <th>Late</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td id="<?php echo $sa_id ?>"><?php echo $sa_id; ?></td>
                                                <td><?php echo $attendance_date; ?></td>
                                                <td><?php echo $total_present; ?></td>
                                                <td><?php echo $total_absent; ?></td>
                                                <td><?php echo $total_leave; ?></td>
                                                <td><?php echo $total_late; ?></td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td>Total</td>
                                                <td>--------</td>
                                                <td><?php echo round($total_present / ($total_present + $total_absent + $total_late + $total_leave) * 100, 1) . " %"; ?></td>
                                                <td><?php echo round($total_absent / ($total_present + $total_absent + $total_late + $total_leave) * 100, 1) . " %"; ?></td>
                                                <td><?php echo round($total_leave / ($total_present + $total_absent + $total_late + $total_leave) * 100, 1) . " %"; ?></td>
                                                <td><?php echo round($total_late / ($total_present + $total_absent + $total_late + $total_leave) * 100, 1) . " %"; ?></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <a href="<?php echo site_url("admin/attendance/section/del/" . $sa_id); ?>"
                                   class="btn btn-danger btn-lg"> Delete</a>
                                <a href="<?php echo site_url("admin/attendance/section/edit/" . $sa_id); ?>"
                                   class="btn btn-warning btn-lg"> Edit</a>

                            </div>
                        </div>

                    </div>
                    <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

</div>

<!-- end row -->

<!-- end row -->

</section>
<!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
