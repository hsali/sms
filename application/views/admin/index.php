<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php
// login info . profile and avatar
$this->load->view("admin/sidebar");
?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon");
    ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <?php

        $this->load->view("admin/headers/ribbon2");

        ?>
        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-green" id="wid-id-1" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>

                            <h2>Students</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <div id="student-summary" class="chart no-padding"></div>

                                <div class="well well-sm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="padding-5">
                                                <span class="font-lg"> Total Students </span><span
                                                    class="badge bg-color-green txt-color-white font-lg"><?php echo $summary["total_students"]; ?></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-green" id="wid-id-11" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>

                            <h2>Students</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">


                                <div id="gender-student-summary" class="chart no-padding"></div>
                                <div class="well well-sm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="padding-5">
                                                <span class="font-lg"> Male </span><span
                                                    class="badge bg-color-green txt-color-white font-lg"><?php echo $summary["total_student_males"]; ?></span></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="padding-5">
                                                <span class="font-lg"> Female </span><span
                                                    class="badge bg-color-green txt-color-white font-lg"><?php echo $summary["total_student_females"]; ?></span></div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-greenLight" id="wid-id-3"
                         data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                            <h2>Student Summary</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">


                                <div class="table-responsive">

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><i class="fa fa-edit"></i> Name</th>
                                            <th><i class="fa fa-male"></i> Males</th>
                                            <th><i class="fa fa-female"></i> Females</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i=1;
                                        foreach ($student_detail as $row): ?>
                                            <tr class="<?php
                                            if ($i % 4 == "0")
                                                echo "success";
                                            elseif ($i % 4 == "1")
                                                echo "danger";
                                            elseif ($i % 4 == "2")
                                                echo "warning";
                                            elseif ($i % 4 == "3")
                                                echo "info";
                                            ?>">
                                                <td><?php echo $i++ ?></td>
                                                <td><?php echo $row["name"] ?></td>
                                                <td><?php echo $row["males"] ?></td>
                                                <td><?php echo $row["females"] ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->


                </article>
                <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-yellow" id="wid-id-2" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>

                            <h2>Staff</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <div id="staff-summary" class="chart no-padding"></div>

                                <div class="well well-sm">
                                    <div class="row">
                                        <div class="col-md-6">

                                        </div>
                                    </div>
                                    <div class="padding-5">
                                        <span class="font-lg"> Total Staffs </span><span
                                            class="badge bg-color-green txt-color-white font-lg"><?php echo $summary["total_staffs"]; ?></span></div>
                                </div>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-yellow" id="wid-id-21" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>

                            <h2>Staff</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <div id="gender-staff-summary" class="chart no-padding"></div>


                                <div class="well well-sm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="padding-5">
                                                <span class="font-lg"> Male </span><span
                                                    class="badge bg-color-green txt-color-white font-lg"><?php echo $summary["total_staff_males"]; ?></span></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="padding-5">
                                                <span class="font-lg"> Female </span><span
                                                    class="badge bg-color-green txt-color-white font-lg"><?php echo $summary["total_staff_females"]; ?></span></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-greenLight" id="wid-id-4"
                         data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                            <h2>Staff Summary</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">


                                <div class="table-responsive">

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><i class="fa fa-edit"></i> Name</th>
                                            <th><i class="fa fa-male"></i> Males</th>
                                            <th><i class="fa fa-female"></i> Females</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php   $i=1;
                                        foreach ($staff_detail as $row): ?>
                                            <tr class="<?php
                                            if ($i % 4 == "0")
                                                echo "success";
                                            elseif ($i % 4 == "1")
                                                echo "danger";
                                            elseif ($i % 4 == "2")
                                                echo "warning";
                                            elseif ($i % 4 == "3")
                                                echo "info";
                                            ?>">
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $row["name"] ?></td>
                                                <td><?php echo $row["males"] ?></td>
                                                <td><?php echo $row["females"] ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-yellow" id="wid-id-222" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>

                            <h2>Fee & Salary</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <div id="fee-and-salary-summary" class="chart no-padding"></div>


                                <div class="well well-sm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="padding-5">
                                                <span class="font-lg"> Fee </span><span
                                                    class="badge bg-color-green txt-color-white font-lg"><?php echo $summary["total_fee"]; ?></span></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="padding-5">
                                                <span class="font-lg"> Salary </span><span
                                                    class="badge bg-color-green txt-color-white font-lg"><?php echo $summary["total_salary"]; ?></span></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>
                <!-- WIDGET END -->

            </div>

            <!-- end row -->

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->
<?php


// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");

// . scripts or code which required on the home page
$this->load->view("admin/footers/footer_home_scripts");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
