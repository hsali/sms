<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>


            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-0"
                         data-widget-editbutton="false"
                         role="widget" data-widget-colorbutton="false" data-widget-collopse="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!--                                <a href="javascript:void(0);"-->
                                <!--                                                                           class="button-icon jarviswidget-toggle-btn"-->
                                <!--                                                                           rel="tooltip" title=""-->
                                <!--                                                                           data-placement="bottom"-->
                                <!--                                                                           data-original-title="Collapse"><i-->
                                <!--                                        class="fa fa-minus "></i></a> -->
                                <a href="javascript:void(0);"
                                   class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom"
                                   data-original-title="Fullscreen"><i
                                        class="fa fa-expand "></i></a>
                                <!--                                <a href="javascript:void(0);"-->
                                <!--                                                                          class="button-icon jarviswidget-delete-btn"-->
                                <!--                                                                          rel="tooltip" title="" data-placement="bottom"-->
                                <!--                                                                          data-original-title="Delete"><i-->
                                <!--                                        class="fa fa-times"></i></a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>

                            <h2>Fee Management</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                                <div class="alert alert-warning fade in">
                                    <button class="close" data-dismiss="alert">
                                        ×
                                    </button>
                                    <i class="fa-fw fa fa-2x fa-times"></i>
                                    <strong>Warning!</strong> Be careful to add the data. Once enter. You cannot modify it. After enter this data, you will go to receipt after saving data.
                                </div>

                                <form class="form-horizontal" method="post" enctype="multipart/form-data">

                                    <fieldset>
                                        <legend>Add Admission Fee</legend>
                                        <div class="row">
                                            <div class="col-md-6  pull-left ">
                                                <div
                                                    class="form-group form-group-lg  <?php if (form_error("student_id")) echo "has-error"; elseif (!empty($student_id)) echo "has-success" ?>">
                                                    <label class="col-md-2 control-label">Student ID*</label>

                                                    <div class="col-md-10">
                                                        <input class="form-control" required placeholder="Choose Student" id="student_id" name="student_id" list="staff_names"  type="text" value="<?php if(isset($student_id)) echo $student_id; ?>" autocomplete="off">
                                                        <datalist id="staff_names">
                                                            <?php if(isset($a_students)){
                                                                foreach($a_students as $rows){
                                                                    ?>
                                                                    <option value="<?php echo $rows["student_id"]; ?>" ><?php echo $rows["first_name"]." ".$rows["last_name"]; ?></option>

                                                                    <?php    /* */
                                                                }
                                                            } /*  */?>
                                                        </datalist>
                                                        <span class="help-Block"><?php echo form_error("student_id");?></span>
                                                    </div>
                                                </div>
                                            </div>
<div class="col-md-6"></div>
                                        </div>


                                        <div class="multi-field-wrapper">
                                            <div class="multi-fields">
                                                <div class="multi-field">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group <?php
                                                            if (form_error("fee_category"))
                                                                echo "has-error";
                                                            elseif (!empty($fee_category))
                                                                echo
                                                                "has-success"
                                                            ?>">

                                                                <div class="col-md-12">

                                                                    <select class="form-control" id="fee_category"
                                                                            name="fee_category[]">
                                                                        <option value="" selected="" disabled="">
                                                                            Select Fee Category
                                                                        </option>
                                                                        <?php
                                                                        foreach ($this->m_common->select_variable('fee_category') as $rows) {
                                                                            $key = $rows["var_vid"];
                                                                            $value = $rows["var_value"];
                                                                            echo "<option label='{$value}' value='{$key}'";
                                                                            if (isset($fee_category) && $fee_category == $key) {
                                                                                echo 'selected="selected"';
                                                                            }
                                                                            echo ">" . $value . "</option>";
                                                                        }
                                                                        ?>
                                                                    </select>
                                                        <span class="help-Block"><?php echo form_error("fee_category");
                                                            ?></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-4">
                                                            <input class="form-control" id="fee"
                                                                   name="fee[]"
                                                                   placeholder="Type Fee" type="text"
                                                                   value="<?php if (isset($fee))
                                                                       echo $fee;
                                                                   ?>">
                                                            <span class="help-Block"><?php echo form_error("fee"); ?>
                                                                Fee: Don't Enter the fee if you want default</span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input class="form-control" id="period"
                                                                   name="period[]"
                                                                   placeholder="Type Period" type="text"
                                                                   value="<?php if (isset($period))
                                                                       echo $period;
                                                                   ?>">
                                                            <span class="help-Block"><?php echo form_error("period"); ?>
                                                                Period: Don't Enter period if you want default</span>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="remove-field btn btn-danger ">Remove
                                                    </button>
<hr>
                                                </div>
                                                <br/>
                                            </div>
                                            <br/>
                                            <button type="button" class="add-field btn btn-success">Add field</button>
                                        </div>


                                    </fieldset>


                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset
                                                </button>
                                                <button type="submit" name="submit" id="submit" class="btn
                                              btn-success btn-lg">Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


//$this->load->view("admin/footers/footer_datatables");



// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
