<?php $this->load->view("admin/header"); ?>

    <!-- Left panel : Navigation area -->
    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
    <!-- END NAVIGATION -->

    <!-- MAIN PANEL -->
    <div id="main" role="main">

        <!-- RIBBON -->
        <?php $this->load->view("admin/headers/ribbon"); ?>
        <!-- END RIBBON -->

        <!-- MAIN CONTENT -->
        <div id="content">

            <?php
            // ribbon 2 contain the summary of page.
            $this->load->view("admin/headers/ribbon2"); ?>

            <!-- widget grid -->
            <section id="widget-grid" class="">

                <?php $this->load->view("admin/message_box"); ?>

                <!-- row -->
                <div class="row">

                    <!-- NEW WIDGET START -->
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
                            <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"
                                    data-widget-sortable="false"

                                    -->
                            <header>
                                <span class="widget-icon"> <i class="fa fa-users"></i> </span>
                                <h2>Entities Managements</h2>

                            </header>

                            <!-- widget div-->
                            <div>

                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->

                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding">

                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th data-hide="phone">Var ID</th>
                                            <th data-class="expand">Var Title</th>
                                            <th data-hide="phone">Var Description</th>
                                            <th data-hide="phone">Created Date</th>
                                            <th data-hide="phone,tablet" >Action</th>
                                        </tr>
                                        </thead>
                                       <tbody>
                                       <?php foreach($view_data as $rows): ?>
                                           <tr id="var_id_<?php  echo $rows["var_id"]; ?>">
                                               <td><?php  echo $rows["var_id"]; ?> </td>
                                               <td><a  href="<?php echo site_url("admin/settings/variables/view/"
                                                       .$rows["var_id"]);
                                                   ?>"><?php  echo $rows["var_title"];
                                                       ?></a></td>
                                               <td><?php  echo $rows["var_description"]; ?></td>
                                               <td><?php  echo $rows["created_date_time"]; ?></td>
                                               <td >
                                                   <a href="<?php echo site_url("admin/settings/variable/values/add")
                                                       ."/".$rows["var_id"]; ?>" class="btn btn-info">Add</a>
                                                   <?php if($rows["editable"]=="1"): ?>
                                                   <a href="<?php echo site_url("admin/settings/variables/edit")
                                                       ."/".$rows["var_id"]; ?>" class="btn btn-warning">Edit</a>
                                                   <a class="btn btn-danger" href="<?php echo site_url
                                                       ("admin/settings/variables/del") ."/".$rows["var_id"];
                                                   ?>">Delete</a>
                                               <?php endif; ?>
                                               </td>
                                           </tr>
                                       <?php endforeach; ?>
                                       </tbody>
                                    </table>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end widget div -->

                        </div>
                        <!-- end widget -->

                    </article>
                    <!-- WIDGET END -->

                </div>

                <!-- end row -->

                <!-- end row -->

            </section>
            <!-- end widget grid -->

        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->


    <!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
