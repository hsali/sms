<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2"); ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-0"  data-widget-editbutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>
                            <h2>User Registration Form</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                                <?php
                                if(isset($edit_data)){
                                    foreach($edit_data as $row){
                                        $cr_var_name=$row["var_name"];
                                        $cr_var_title=$row["var_title"];
                                        $cr_var_description=$row["var_description"];
                                    }
                                }
                                ?>

                                <form class="form-horizontal" method="post" enctype="multipart/form-data" >
                                    <fieldset>
                                        <legend>Edit Entity</legend>

                                        <!--     Variable Name -->
                                        <div class="form-group <?php if(form_error("var_name"))
                                            echo "has-error";
                                        elseif(! empty($var_name)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Variable Name*</label>
                                            <div class="col-md-10">
                                                <input class="form-control" id="var_name" name="var_name"
                                                       placeholder="Type Variable Name " required="" type="text"
                                                       value="<?php
                                                       if(isset($var_name)) echo $var_name;
                                                       elseif(isset($cr_var_name)) echo $cr_var_name;
                                                       ?>">
                                                 <span class="help-Block"><?php echo form_error("var_name");
                                                     ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group <?php if(form_error("var_title"))
                                            echo "has-error";
                                        elseif(! empty($var_title)) echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Variable Title*</label>
                                            <div class="col-md-10">
                                                <input class="form-control" id="var_title" name="var_title"
                                                       placeholder="Type Title of variable" required=""
                                                       type="text"
                                                       value="<?php
                                                       if(isset($var_title)) echo $var_title;
                                                       elseif(isset($cr_var_title)) echo $cr_var_title;
                                                       ?>">
                                                 <span class="help-Block"><?php echo form_error("var_title");
                                                     ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group <?php
                                        if(form_error("var_description"))
                                            echo "has-error";
                                        elseif(! empty($var_description))
                                            echo "has-success" ?>">
                                            <label class="col-md-2 control-label">Variable Description</label>
                                            <div class="col-md-10">
                                                <textarea id="var_description" name="var_description" class="form-control"
                                                          placeholder="Type something about variable"
                                                          rows="4"><?php
                                                    if(isset($var_description)) echo $var_description;
                                                    elseif(isset($cr_var_description)) echo $cr_var_description;
                                                    ?></textarea>
                                                  <span class="help-Block"><?php echo form_error("var_description");
                                                      ?></span>
                                            </div>
                                        </div>

                                    </fieldset>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-danger" type="reset" id="reset" name="reset">
                                                    Reset
                                                </button>
                                                <button class="btn btn-success" type="submit" name="submit" id="submit">
                                                    <i class="fa fa-save"></i>
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>




                <!-- WIDGET END -->

            </div>

            <!-- row -->
            <!--          --><?php //$this->load->view("admin/b_bootstrap_form"); ?>

            <!-- end row -->










        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->





</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!--================================================== -->



<?php

// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
