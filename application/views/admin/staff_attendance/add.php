<?php
//$a_branch=$this->config->item('branches');
$a_domicile = $this->config->item('domicile');
$a_gender = $this->config->item('gender');
$a_religion = $this->config->item('religion');
$a_country = $this->config->item('country');
$a_city = $this->config->item('city');
$a_guardian_type = $this->config->item('guardian_type');
$a_guardian_status = $this->config->item('guardian_status');
$a_degree_types = $a_guardian_education = $this->config->item('degree_type');
//$a_sections=$this->config->item('sections');
//$a_classes=$this->config->item('classes');
$a_enrollment_status = $this->config->item('enrollment_status');
?>
<?php $this->load->view("admin/header"); ?>

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<?php $this->load->view("admin/sidebar"); ?>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <?php $this->load->view("admin/headers/ribbon"); ?>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <!-- MAIN CONTENT -->
    <div id="content">

        <?php
        // ribbon 2 contain the summary of page.
        $this->load->view("admin/headers/ribbon2");
        ?>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <?php $this->load->view("admin/message_box"); ?>


            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable jarviswidget-color-yellow" id="wid-id-21"
                         data-widget-editbutton="false" jarviswidget-togglebutton="false" role="widget">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu">
                                <!-- <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse">
                                     <i class="fa fa-minus "></i>
                                 </a>-->
                                <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn"
                                   rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                    <i class="fa fa-expand "></i>
                                </a>
                                <!--    <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete">
                                        <i class="fa fa-times"></i>
                                    </a>-->
                            </div>
                            <span class="widget-icon"> <i class="fa fa-th-list "></i> </span>

                            <h2>Attendance</h2>

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <form class="form-horizontal" method="post" enctype="multipart/form-data">

                                    <fieldset id="enrollment_info">
                                        <legend>Attendance</legend>
                                        </br>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group  <?php if (form_error("staff_id")) echo "has-error"; elseif (!empty($staff_id)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Staff ID*</label>

                                                    <div class="col-md-8">
                                                  <input class="form-control" required placeholder="Choose Staff" id="staff_id" name="staff_id" list="staff_names"  type="text" value="<?php if(isset($staff_id)) echo $staff_id; ?>" autocomplete="off">
                                                        <datalist id="staff_names">
                                                            <?php if(isset($a_staffs)){
                                                                foreach($a_staffs as $rows){
                                                            ?>
                                                            <option value="<?php echo $rows["staff_id"]; ?>" ><?php echo $rows["first_name"]." ".$rows["last_name"]; ?></option>

                                                                    <?php    /* */
                                                                }
                                                            } /*  */?>
                                                        </datalist>
                                                        <span class="help-Block"><?php echo form_error("staff_id");?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <!--Date -->
                                                <div
                                                    class="form-group  <?php if (form_error("attendance_time")) echo "has-error"; elseif (!empty($attendance_time)) echo "has-success" ?>">
                                                    <label class="col-md-4 control-label">Attendance Time*</label>

                                                    <div class="col-md-8">
                                                        <div class="input-group date date-time-picker">
                                                            <input required class="form-control" id="attendance_time"
                                                                   name="attendance_time" type="text"
                                                                   placeholder="Choose Attendance Time"
                                                                   value="<?php if (isset($attendance_time)) echo $attendance_time; ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                            <span class="help-Block"><?php echo form_error("attendance_time");?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Attendance*</label>

                                                    <div class="col-md-10">
                                                        <div class="btn-group btn-group-sm btn-group-justified"
                                                             data-toggle="buttons">
                                                            <label class="btn btn-default <?php
                                                            if(isset($attendance) && $attendance=="1")
                                                                echo 'active';
                                                            ?> ">
                                                                <input type="radio" required name="attendance" id="a1" value="1"
                                                                    <?php if(isset($attendance) && $attendance=="1")
                                                                    echo 'checked=""';
                                                                    ?> >
                                                                <i class="text-success">Present</i>
                                                            </label>
                                                            <label class="btn btn-default <?php
                                                            if(isset($attendance) && $attendance=="2")
                                                                echo 'active';
                                                            ?>">
                                                                <input type="radio" name="attendance" id="a2" value="2" <?php
                                                                    if(isset($attendance) && $attendance=="2")
                                                                    echo 'checked=""';
                                                                    ?>>
                                                                <i class="text-danger">Absent</i> </label>
                                                            <label class="btn btn-default <?php
                                                            if(isset($attendance) && $attendance=="3")
                                                                echo 'active';
                                                            ?>">
                                                                <input type="radio" name="attendance" id="a3" value="3" <?php
                                                                    if(isset($attendance) && $attendance=="3")
                                                                        echo 'checked=""';
                                                                    ?> >
                                                                <i class="text-warning">Leave</i> </label>
                                                            <label class="btn btn-default <?php
                                                            if(isset($attendance) && $attendance=="4")
                                                                echo 'active';
                                                            ?>">
                                                                <input type="radio" name="attendance" id="a4" value="4" <?php
                                                                    if(isset($attendance) && $attendance=="4")
                                                                        echo 'checked=""';
                                                                    ?>>
                                                                <i class="text-info">Late</i> </label>

                                                        </div>
                                                        <span class="help-Block"><?php echo form_error("attendance");?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </fieldset>

                                    <!--                                        submit button -->
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-warning btn-lg" id="reset" name="reset"
                                                        type="reset">Reset
                                                </button>
                                                <button type="submit" name="submit" id="submit" class="btn
                                                        btn-success btn-lg">Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>


                <!-- WIDGET END -->

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->


</div>
<!-- END MAIN PANEL -->


<!--================================================== -->


<?php
// copyrights and info for website
$this->load->view("admin/footers/footer4_copyright");

// shortcut. these are icon which pop up when click on the user profile avatar.
$this->load->view("admin/footers/shortcuts");

// general scripts references
$this->load->view("admin/footers/footer_general_scripts_ref");


$this->load->view("admin/footers/footer_datatables");


// last footer. footer counting from the bottom. bottom footer is called footer1
$this->load->view("admin/footers/footer1");
?>
