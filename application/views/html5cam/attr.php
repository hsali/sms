<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
     <link rel="stylesheet" type="text/css"  href="<?php echo base_url("media/admin_panel/css/bootstrap.min.css"); ?>">
    <title>Photo Capturing</title>
</head>
<body>
    <h1>Capture picture and upload</h1>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <video class="responsive" id="video" width="400" height="300" ></video>
            </div>
            <div class="col-md-6">
                <canvas style="display:none" id="canvas" width="400" height="300"></canvas>
    <img id="im1" src="<?php echo base_url("pdb/test/camera.jpg"); ?>" width="400" height="300" id="photo" alt="Kid">
            </div>
        </div>
        <div class="form-actions">
            <div class="btn-group-lg">
            <a href="#" id="capture" class="btn btn-large btn-success">Take Picture</a>    
            <a href="#" id="upload" class="btn btn-large btn-warning">Upload</a>    
            </div>
            
        </div>
    </div>
    <!-- BOOTSTRAP JS -->
<script src="<?php echo base_url("media/admin_panel/js/libs/jquery-2.0.2.min.js"); ?>"><\/script>
<script src="<?php echo base_url("media/admin_panel/js/bootstrap/bootstrap.min.js"); ?>"></script>

<script>
$(document).ready(function(){
    $("#upload").click(function(){
        alert($("#im1").attr("src"));
//            alert("htlm");
    });
});
</script>
</body>
</html>