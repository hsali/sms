<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
     <link rel="stylesheet" type="text/css"  href="<?php echo base_url("media/admin_panel/css/bootstrap.min.css"); ?>">
    <title>Photo Capturing</title>
</head>
<body>
    <h1>Capture picture and upload</h1>
    <hr>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <video class="responsive" id="video" width="400" height="300" ></video>
            </div>
            <div class="col-md-6">
                <canvas style="display:none" id="canvas" width="400" height="300"></canvas>
    <img src="<?php echo base_url("pdb/test/camera.jpg"); ?>" width="400" height="300" id="photo" alt="Kid">
            </div>
        </div>
        <div class="form-actions">
            <div class="btn-group-lg">
            <a href="javascript:void(0)" id="capture" class="btn btn-large btn-success">Take Picture</a>
            <a href="#" id="upload" class="btn btn-large btn-warning">Upload</a>
            </div>
            
        </div>
    </div>
    <div>
    </div>
    <script src="<?php echo base_url("media/admin_panel/js/libs/jquery-2.0.2.min.js"); ?>"></script>
    <script src="<?php echo base_url("media/admin_panel/js/bootstrap/bootstrap.min.js"); ?>"></script>

    <script type="text/javascript">
        (function(){

    navigator.getMedia=     navigator.getUserMedia ||
                            navigator.webkitGetUserMedia ||
                            navigator.mozGetUserMedia ||
                            navigator.msGetUserMedia;
    navigator.getMedia({
        video: true,
        audio: false
    }, function(stream) {
        video.src = vendorUrl.createObjectURL(stream);
        video.play();
    }, function(error){
        // an error occured
        // error.code
        alert("Browser do not support");
    });

            document.getElementById("capture").addEventListener('click', function(){
        context.drawImage(video,0,0,400,300);
        photo.setAttribute('src',canvas.toDataURL('image/png'));

    });
        })();
        var video=document.getElementById('video'),
        canvas=document.getElementById('canvas'),
        context=canvas.getContext('2d'),
        photo=document.getElementById('photo'),
        vendorUrl=window.URL || window.webkitURL;

        (function(){

            var video=document.getElementById('video'),
                canvas=document.getElementById('canvas'),
                context=canvas.getContext('2d'),
                photo=document.getElementById('photo'),
                vendorUrl=window.URL || window.webkitURL;
            navigator.getMedia=     navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia;

            navigator.getMedia({
                video: true,
                audio: false
            }, function(stream) {
                video.src = vendorUrl.createObjectURL(stream);
                video.play();
            }, function(error){
                // an error occured
                // error.code
                alert("Browser do not support");
            });
            document.getElementById("capture").addEventListener('click', function(){
                context.drawImage(video,0,0,400,300);
                photo.setAttribute('src',canvas.toDataURL('image/png'));

            });
        })();

    </script>


<script>
$(document).ready(function(){
    $("#upload").on('click',function(){
       var dd=$("img").attr("src");
//       alert(dd);
       $.ajax({
         url: "<?php echo site_url("upload/image"); ?>",
         type: 'POST',
         data: { img: dd,
                id:"<?php echo isset($id)? $id: ""; ?>",
                type:"<?php echo isset($type)? $type: ""; ?>",
         },
    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
    success: function(result){
        if(result=="1"){
//            alert("Message :"+ result);
            window.close();
        }
        else if(result=="-1")
        alert("Picture cannot upload successfully. Invalid validation");
        }
    });

});
});
</script>

</body>
</html>